﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.Generic;
using MedTouch.Core;
using MedTouch.Connector.Locations;

[assembly: MRegisterApiController(typeof(GetLocations))]

namespace MedTouch.Connector.Locations
{
    using global::MedTouch.Common.Items;
    using global::MedTouch.Common.WebApi.Base;
    using global::MedTouch.Locations.Helpers.Search;
    using global::MedTouch.Locations.Items;

    using MedTouch.Common.GeoLocations;
    using MedTouch.ContentSearch.Spatial;
    using MedTouch.Locations.Helpers;

    using Sitecore.Common;
    using Sitecore.ContentSearch;
    using Sitecore.Data.Items;
    using Sitecore.WordOCX.Extensions;

    /// <summary>
    ///     search by criteria
    ///     /mapi/public/getlocations.json?lastname=xxx
    /// </summary>
    public class GetLocations : MApiContextInfoAndSearchController<LocationsModuleSettings, SiteSettings>, IMApiGetAction
    {
        #region Public Methods and Operators

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="arguments">
        /// The arguments.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public virtual string Get(MApiArgs arguments)
        {
        var locationSearchParam = arguments.QueryString.DeserializeHtmlForm<LocationSearchParam>();

        locationSearchParam.StartPaths =
            this.ModuleSettings.LocationsFolder.StringConcatenate(x => x.ItemId.ToID().ToString() + "|")
                .TrimEnd('|');

        locationSearchParam.Templates = this.ModuleSettings.LocationTemplate.ItemId.ToID().ToString();
        locationSearchParam.Language = this.ContextInfo.Language;

        using (var searchContext = ContentSearchManager.GetIndex(this.SiteSettings.IndexName).CreateSearchContext())
        {
                var locationSearcher = new LocationSearcher<LocationSearchResultItem>(
                    searchContext,
                    this.ModuleSettings);

                var locationResultitems = locationSearcher.GetResultItems(locationSearchParam).ToArray();          
            return this.Json(locationResultitems.Select(x => this.getAPIFields(x.GetItem()).SerializeToJson()));
        }
    }

        public APIFields getAPIFields(Item locationItem)
        {
            APIFields flds = new APIFields();
            flds.ID = locationItem.ID.ToString();
            flds.Title = locationItem.DisplayName;
            flds.LatLon = locationItem.Fields["LatLon"].ToString();
            flds.Addr1 = locationItem.Fields["Address 1"].ToString();
            flds.Addr2 = locationItem.Fields["Address 2"].ToString();
            flds.City = locationItem.Fields["City"].ToString();
            flds.State = locationItem.Fields["State"].ToString();
            flds.Zip = locationItem.Fields["Zip"].ToString();
            flds.Phone = locationItem.Fields["Main Phone"].ToString();
            flds.Introduction = locationItem.Fields["Introduction"].ToString();
            flds.ContentCopy = locationItem.Fields["Content Copy"].ToString();
            flds.Services = locationItem.Fields["Services"].ToString();
            flds.Specialties = locationItem.Fields["Specialties"].ToString();
            flds.Photo = locationItem.Fields["Photo"].ToString();
            return flds;
        }
        #endregion
    }

    public class APIFields
    {
        public string ID { get; set; }
        public string Title { get; set; }
        public string Addr1 { get; set; }
        public string Addr2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }

        public string Introduction { get; set; }
        public string ContentCopy { get; set; }
        public string Services { get; set; }
        public string Specialties { get; set; }
        public string LatLon { get; set; }
        public string Photo { get; set; }
    }
}