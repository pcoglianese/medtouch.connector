﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;

using MedTouch.Connector.RestEndpoints.RequestHandlers;
using MedTouch.Connector.RestEndpoints.ResponseFormatters;


namespace MedTouch.Connector.RestEndpoints
{
    using MedTouch.Common;

    /// <summary>
    /// A REST endpoint request/response context
    /// </summary>
    public class RestContext
    {
        /// <summary>
        /// An ISO 8601 timestamp format
        /// </summary>
        private const string Iso8601TimestampFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fffzzz";

        /// <summary>
        /// Default constructor
        /// </summary>
        private RestContext() { }

        /// <summary>
        /// Executes the request for this context, then formats the response to output
        /// </summary>
        public void Execute() 
        {
            if (this.Request == null)
                throw new InvalidOperationException("No request to execute");
            else if (this.Response == null)
                throw new InvalidOperationException("No response with which to send output");

            RestRequestHandler.GetRequestHandler(this).Execute(this.Request);
            RestResponseFormatter.GetResponseFormatter(this).Format(this.Response, this.Response.Output);

            this.Response.Flush();
            this.Response.Close();
        }

        /// <summary>
        /// Constructs a new instance
        /// </summary>
        /// <param name="endpoint">The REST endpoint for this REST service context</param>
        /// <param name="request">The REST request for this REST service context</param>
        /// <param name="response">The REST response for this REST service context</param>
        /// <param name="sitecoreContext">The Sitecore context for this REST service context</param>
        public RestContext(RestEndpoint endpoint, HttpContext httpContext)
        {
            if (endpoint == null || httpContext == null) throw new ArgumentNullException();            

            Endpoint = endpoint;
            Request = new RestRequest(httpContext.Request, this);
            Response = new RestResponse(httpContext.Response, this);
            Item = endpoint.GetOriginalItem();
            Database = Item.Database;
        }

        /// <summary>
        /// The REST endpoint for this service context
        /// </summary>
        public RestEndpoint Endpoint { get; private set; }

        /// <summary>
        /// The request for this REST service context
        /// </summary>
        public RestRequest Request { get; private set; }

        /// <summary>
        /// The response for this REST service context
        /// </summary>
        public RestResponse Response { get; private set; }

        /// <summary>
        /// The Sitecore item for this REST endpoint service context
        /// </summary>
        public Item Item { get; private set; }

        /// <summary>
        /// The Sitecore database for this REST endpoint service context
        /// </summary>
        public Database Database { get; private set; }

        /// <summary>
        /// Gets the current time elapsed for this service request/response context, in milliseconds
        /// </summary>
        public int DurationInMilliseconds
        {
            get
            {
                return (int)((DateTime.Now.Ticks - Request.Timestamp.Ticks) / 10000L);
            }
        }

        /// <summary>
        /// The log for this context
        /// </summary>
        private List<string> log = new List<string>();

        /// <summary>
        /// Logs the specified message to output; may be useful in troubleshooting
        /// </summary>
        /// <param name="message">The message to log</param>
        public void Log(string message)
        {
            if (string.IsNullOrWhiteSpace(message)) return;
            message = DateTime.Now.ToString(Iso8601TimestampFormat) + " " + message.Trim();
            log.Add(message);
        }

        /// <summary>
        /// Logs the specified exception to output; may be useful in troubleshooting
        /// </summary>
        /// <param name="message">The exceptoin to log</param>
        /// <param name="full">If false, only the message will be logged for the exception</param>
        public void Log(Exception e)
        {
            if (e == null) return;
            string message = e.GetExtendedMessage();
            log.Add(message + " " + e.StackTrace);
            if (IsSystemLoggingEnabled)
                try { Sitecore.Diagnostics.Log.Error(message, e, this); } catch {}
        }

        /// <summary>
        /// Gets the log for this REST context
        /// </summary>
        /// <returns>The log for this instance</returns>
        public IList<string> GetLog()
        {
//            return log.ToList<string>();
            return log;
        }

        /// <summary>
        /// The name of the field containing the setting for logging enablement
        /// </summary>
        private const string IsLoggingEnabledFieldName = "Is Logging Enabled";

        /// <summary>
        /// The name of the field containing the setting for system logging enablement
        /// </summary>
        private const string IsSystemLoggingEnabledFieldName = "Is System Logging Enabled";

        /// <summary>
        /// The name of the field containing the setting for pretty-printing enablement
        /// </summary>
        private const string IsPrettyPrintingEnabledFieldName = "Is Pretty Printing Enabled";

        /// <summary>
        /// Gets the setting/parameter value for the specified name
        /// </summary>
        /// <param name="name">The name of the value to get/param>
        /// <returns></returns>
        private bool GetBooleanSettingOrParameter(string name, bool defaultValue = false)
        {
            if (string.IsNullOrWhiteSpace(name)) return defaultValue;

            string nameNoSpaces = name.Replace(" ", "");
            Item item = Endpoint.GetOriginalItem();

            if (item[name] == "1" || item[nameNoSpaces] == "1") return true;

            string value = "";
            string key = name;
            if (Request.Parameters.TryGetValue(key, out value) && !string.IsNullOrWhiteSpace(value))
            {
                value = value.Trim().ToLower();
                return (value == "1" || value == "true" || value == "yes");
            }

            key = nameNoSpaces;
            if (Request.Parameters.TryGetValue(key, out value) && !string.IsNullOrWhiteSpace(value))
            {
                value = value.Trim().ToLower();
                return (value == "1" || value == "true" || value == "yes");
            }

            return defaultValue;
        }

        /// <summary>
        /// Indicates whether logging is enabled for this configuration
        /// </summary>
        public bool IsLoggingEnabled
        {
            get
            {
                return GetBooleanSettingOrParameter(IsLoggingEnabledFieldName, false);
            }
        }

        /// <summary>
        /// Indicates whether system logging is enabled
        /// </summary>
        private bool? isSystemLoggingEnabled = null;

        /// <summary>
        /// Indicates whether logging is enabled for this configuration
        /// </summary>
        public bool IsSystemLoggingEnabled
        {
            get
            {
                if (isSystemLoggingEnabled == null)
                    isSystemLoggingEnabled = GetBooleanSettingOrParameter(IsSystemLoggingEnabledFieldName, false);
                return (bool)isSystemLoggingEnabled;
            }
        }

        /// <summary>
        /// Indicates whether logging is enabled for this configuration
        /// </summary>
        public bool IsPrettyPrintingEnabled
        {
            get
            {
                return GetBooleanSettingOrParameter(IsPrettyPrintingEnabledFieldName, false);
            }
        }

    }
}