﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedTouch.Connector.RestEndpoints
{
    /// <summary>
    /// Formats responses
    /// </summary>
    public interface IRestResponseFormatter
    {
        /// <summary>
        /// Formats the specified response
        /// </summary>
        /// <param name="response">The response to format</param>
        /// <returns>A string representation of the response</returns>
        string Format(RestResponse response);

        /// <summary>
        /// Formats the specified response
        /// </summary>
        /// <param name="response">The response to format</param>
        /// <param name="writer">The writer to which to write</param>
        void Format(RestResponse response, TextWriter writer);
    }
}
