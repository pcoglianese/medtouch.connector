// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IInternalDataSource.cs" company="MedTouch">
//   Copyright (c) MedTouch 2015. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//        Developer:    sitecore\admin
// Template Version:    3.0
//             Date:    07/23/2015
// --------------------------------------------------------------------------------------------------------------------
namespace MedTouch.Connector.RestEndpoints.DataSources
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    using Newtonsoft.Json;

    using Sitecore;
    using Sitecore.Data;
    using Sitecore.Data.Items;

    using MedTouch.Common;
    using MedTouch.Common.Versioning;
    

    /// <summary>
    /// The 'InternalDataSource' Interface
    /// </summary>
    public interface IInternalDataSource : IMItem, IDataSource
    {

    }

    /// <summary>
    /// The 'InternalDataSource' Data Template item
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    [Serializable, GuidAttribute("0df0df2a-0525-409f-b794-34e42a3d632c")]
    [MTemplateVersion("0df0df2a-0525-409f-b794-34e42a3d632c", "Internal Data Source")]
    public partial class InternalDataSource : MedTouch.Connector.RestEndpoints.DataSource, IInternalDataSource
    {
        /// <summary>
        /// Initializes a new instance of the InternalDataSource class.
        /// </summary>
        /// <param name="item">The Sitecore item.</param>
        internal InternalDataSource(Item item)
            : base(item)
        {
        }

        #region Template Properties

        #endregion

        #region IInternalDataSource Properties

        #endregion

        /// <summary>
        /// Performs an implicit conversion from <see cref="Sitecore.Data.Items.Item"/> to <see cref="InternalDataSource"/>.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>
        /// The result of the conversion.
        /// </returns>
        public static implicit operator InternalDataSource(Item item)
        {
            return item.InheritsTemplate(typeof(InternalDataSource)) ? new InternalDataSource(item) : null;
        }

        /// <summary>
        /// The Strongly typed field names for <see cref="InternalDataSource"/>.
        /// </summary>
        public static new class FieldNames
        {

        }

        #region Custom code

        /// <summary>
        /// Implements logic to get data from this data soruce
        /// </summary>
        /// <param name="request">The request for which to get the data</param>
        /// <param name="data">A list of data objects, to which to append</param>
        /// <param name="sources">The list of sources from which to get the data</param>
        protected override void GetData(RestRequest request, List<SortedDictionary<string, object>> data, List<string> sources)
        {
            if (request == null || data == null) throw new ArgumentNullException();
            else if (Source.Count == 0) return;

            using (new Sitecore.SecurityModel.SecurityDisabler())
            {
                foreach (string source in sources)
                {
#if (DEBUG)
                    request.Context.Log("Executing query " + source + " ...");
#endif
                    Item[] sitecoreItems = request.Context.Database.SelectItems(source);
                    request.Context.Log(sitecoreItems.Length + " items found");

                    SortedDictionary<string, object> item;
                    foreach (Item sitecoreItem in sitecoreItems)
                    {
                        item = new SortedDictionary<string, object>();
                        item.Load(sitecoreItem, this.ImageFieldRenderingHint, this.LinkFieldRenderingHint, this.TextFieldRenderingHint);
                        data.Add(item);
                    }
                }
            }
        }

        #endregion Custom code

    }
}