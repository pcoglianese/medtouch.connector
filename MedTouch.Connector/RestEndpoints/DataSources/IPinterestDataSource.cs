// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IPinterestDataSource.cs" company="MedTouch">
//   Copyright (c) MedTouch 2015. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//        Developer:    sitecore\admin
// Template Version:    3.0
//             Date:    07/23/2015
// --------------------------------------------------------------------------------------------------------------------
namespace MedTouch.Connector.RestEndpoints.DataSources
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Runtime.InteropServices;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    using Sitecore.Data;
    using Sitecore.Data.Items;

    using MedTouch.Common;
    using MedTouch.Common.Versioning;


    /// <summary>
    /// The 'PinterestDataSource' Interface
    /// </summary>
    public interface IPinterestDataSource : IExternalDataSource
    {

    }

    /// <summary>
    /// The 'PinterestDataSource' Data Template item
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    [Serializable, GuidAttribute("4f75344d-7e1c-42f2-96e4-0562a4a37958")]
    [MTemplateVersion("4f75344d-7e1c-42f2-96e4-0562a4a37958", "Pinterest Data Source")]
    public partial class PinterestDataSource :
        MedTouch.Connector.RestEndpoints.DataSources.ExternalDataSource, IPinterestDataSource
    {
        /// <summary>
        /// Initializes a new instance of the PinterestDataSource class.
        /// </summary>
        /// <param name="item">The Sitecore item.</param>
        internal PinterestDataSource(Item item)
            : base(item)
        {
        }

        #region Template Properties

        #endregion

        #region IPinterestDataSource Properties

        #endregion

        /// <summary>
        /// Performs an implicit conversion from <see cref="Sitecore.Data.Items.Item"/> to <see cref="PinterestDataSource"/>.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>
        /// The result of the conversion.
        /// </returns>
        public static implicit operator PinterestDataSource(Item item)
        {
            return item.InheritsTemplate(typeof (PinterestDataSource)) ? new PinterestDataSource(item) : null;
        }

        /// <summary>
        /// The Strongly typed field names for <see cref="PinterestDataSource"/>.
        /// </summary>
        public new static class FieldNames
        {

        }


        /// <summary>
        /// A date-time format used by the Newtonsoft JSON package
        /// </summary>
        private const string NewtonsoftTimestampFormat = "M/d/yyyy h:mm:ss tt";

        /// <summary>
        /// A format for constructing URLs to retrieve user data
        /// </summary>
        private const string AuthorUrlFormat =
            "https://api.pinterest.com/v1/me/?fields=id,image,username,first_name,last_name&access_token={0}";

        /// <summary>
        /// A format for constructing URLs to retrieve pins for a user
        /// </summary>
        private const string PinUrlFormat =
            "https://api.pinterest.com/v1/me/pins/?fields=id,creator,note,image,link,created_at,url&access_token={0}";

        //        private const string PinUrlFormat = "https://api.pinterest.com/v1/me/pins/?fields=id,note,creator,image,link,board,created_at,url&access_token={0}";

        /// <summary>
        /// A URL format for a public profile
        /// </summary>
        private const string AuthorProfileUrlFormat = "https://www.pinterest.com/{0}/";

        /// <summary>
        /// A result data field name
        /// </summary>
        private const string AuthorNameFieldName = "Author Name";

        /// <summary>
        /// A result data field name
        /// </summary>
        private const string AuthorAvatarUrlFieldName = "Author Avatar URL";

        /// <summary>
        /// A result data field name
        /// </summary>
        private const string AuthorProfileUrlFieldName = "Author Profile URL";

        /// <summary>
        /// A result data field name
        /// </summary>
        private const string LinkedUrlFieldName = "Linked URL";

        /// <summary>
        /// A result data field name
        /// </summary>
        private const string ContentFieldName = "Content";

        /// <summary>
        /// A result data field name
        /// </summary>
        private const string ImageUrlFieldName = "Image URL";

        public override bool IsValid
        {
            get { return !string.IsNullOrWhiteSpace(this.ExternalSystemAccessToken); }
        }

        public override string ValidationMessage
        {
            get
            {
                if (!IsValid)
                    return "External system access token not set";

                return "";
            }
        }

        /// <summary>
        /// The input stream buffer size
        /// </summary>
        private const int InputStreamBufferSize = 8192;

        /// <summary>
        /// Reads JSON from the remote service
        /// </summary>
        /// <param name="urlFormat">The format of a URL to use for the request</param>
        /// <returns>JSON if it was able to be read, otherwise an empty string</returns>
        private string GetJson(string urlFormat)
        {
            try
            {
                // Avoids errors from expired SSL certificates when requesting over HTTPS
                ServicePointManager.ServerCertificateValidationCallback = (s, cert, chain, ssl) => true;

                HttpWebRequest req =
                    (HttpWebRequest) WebRequest.Create(string.Format(urlFormat, this.ExternalSystemAccessToken));
                req.Method = "GET";
                req.ContentType = "application/x-www-form-urlencoded";
                WebResponse resp = (HttpWebResponse) req.GetResponse();

                string json;
                using (
                    StreamReader reader =
                        new StreamReader(new BufferedStream(resp.GetResponseStream(), InputStreamBufferSize)))
                    json = reader.ReadToEnd();

                return json;
            }
            catch
            {
            }

            return "";
        }

        /// <summary>
        /// Implements logic to get data from this data soruce
        /// </summary>
        /// <param name="request">The request for which to get the data</param>
        /// <param name="data">A list of data objects, to which to append</param>
        /// <param name="sources">The list of sources from which to get the data</param>
        protected override void GetData(RestRequest request, List<SortedDictionary<string, object>> data, List<string> sources)
        {
            if (request == null || data == null) throw new ArgumentNullException();
            else if (!IsValid) throw new InvalidOperationException(ValidationMessage);

            #region 1. Get user info 

            string json = GetJson(AuthorUrlFormat);
            if (string.IsNullOrWhiteSpace(json))
                return;

            /*
             {
                "data": {
                    "username": "jjflemegi", 
                    "bio": "", 
                    "first_name": "Jeff and Judy",
                    "last_name": "Flemegi", 
                    "image": {"60x60": {"url": "http://media-cache-ak0.pinimg.com/avatars/jjflemegi_1438890405_60.jpg", "width": 60, "height": 60}}, 
                    "id": "464222811498536701"
                }
             }
             */

            JObject author = JObject.Parse(JObject.Parse(json)["data"].ToString());
            string authorUserName = author["username"].ToString();
            string authorProfileUrl = string.Format(AuthorProfileUrlFormat, authorUserName);
            string authorName = authorUserName;
            try
            {
                authorName = (author["first_name"].ToString() + " " + author["last_name"].ToString()).Trim();
            }
            catch
            {
            }
            string authorAvatarUrl = "";
            try
            {
                authorAvatarUrl = author["image"]["60x60"]["url"].ToString().Replace("http://", "https://s-");
            }
            catch
            {
            }
            string authorID = author["id"].ToString();

            #endregion 1. Get user info

            #region 2. Get pins

            json = GetJson(PinUrlFormat);
            if (string.IsNullOrWhiteSpace(json))
                return;

            DateTime createdTime = DateTime.Now;
            JArray pins = JArray.Parse(JObject.Parse(json)["data"].ToString());
            for (int x = 0; x < pins.Count; x++)
            {
                /* Sample pin JSON: 
                 {
                    "creator": {"url": "https://www.pinterest.com/jjflemegi/", "first_name": "Jeff and Judy", "last_name": "Flemegi", "id": "464222811498536701"}, 
                    "created_at": "2015-08-17T09:53:25", 
                    "note": "The Rialto Bridge, Venice Giclee Print by Canaletto at AllPosters.com", 
                    "link": "https://www.pinterest.com/r/pin/464222674068261812/4779055074072594921/e6ed552f1e55bae195aa79dc7f34bcd6ff637ae9eadc86051623289cdf11c642", 
                    "board": {"url": "https://www.pinterest.com/jjflemegi/art/", "id": "464222742779336266", "name": "Art"}, 
                    "image": {"original": {"url": "http://media-cache-ec0.pinimg.com/originals/37/cc/97/37cc97f419c01ccad56d52c0b71c83a6.jpg", "width": 473, "height": 355}}, 
                    "id": "464222674068261812"
                 }
                 */

                try
                {
                    JToken pin = pins[x];
                    SortedDictionary<string, object> item = new SortedDictionary<string, object>();

                    string pinID = pin["id"].ToString();
                    string pinURL = pin["url"].ToString();

                    string pinTimestamp = pin["created_at"].ToString();
                    if (DateTime.TryParseExact(pinTimestamp, NewtonsoftTimestampFormat, null,
                        System.Globalization.DateTimeStyles.None, out createdTime))
                        item.SetTimestamp(createdTime);
                    else
                        item.SetTimestamp(pinTimestamp);


                    item.SetExternalID(pinID);
                    item.SetUrl(pinURL);

                    item[AuthorNameFieldName] = authorName;
                    item[AuthorAvatarUrlFieldName] = (authorAvatarUrl + "");
                    item[AuthorProfileUrlFieldName] = (authorProfileUrl + "");
                    item[ContentFieldName] = pin["note"].ToString();
                    item[LinkedUrlFieldName] = pin["link"].ToString();
                    item[ImageUrlFieldName] = pin["image"]["original"]["url"].ToString()
                        .Replace("http://", "https://s-");

                    data.Add(item);
                }
                catch
                {
                }

            }

            #endregion 2. Get pins
        }

    }


}
