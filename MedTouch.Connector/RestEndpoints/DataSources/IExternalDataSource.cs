// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IExternalDataSource.cs" company="MedTouch">
//   Copyright (c) MedTouch 2015. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//        Developer:    sitecore\admin
// Template Version:    3.0
//             Date:    07/23/2015
// --------------------------------------------------------------------------------------------------------------------
namespace MedTouch.Connector.RestEndpoints.DataSources
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    using MedTouch.Common;
    using MedTouch.Common.Versioning;

    using Newtonsoft.Json;

    using Sitecore.Data;
    using Sitecore.Data.Items;
    using System.Text.RegularExpressions;

    /// <summary>
    /// The 'ExternalDataSource' Interface
    /// </summary>
    public interface IExternalDataSource : IMItem, IDataSource
    {
        #region External authentication settings

        /// <summary>
        /// Gets the AccessToken
        /// </summary>
        string ExternalSystemAccessToken { get; }

        /// <summary>
        /// Gets whether the external system access token is temporary
        /// </summary>
        bool IsExternalSystemAccessTokenTemporary { get; }

        /// <summary>
        /// Gets the AccessTokenSecret
        /// </summary>
        string ExternalSystemAccessTokenSecret { get; }

        /// <summary>
        /// Gets the Password
        /// </summary>
        string ExternalSystemPassword { get; }

        /// <summary>
        /// Gets the RelyingPartyKey
        /// </summary>
        string RelyingPartyKey { get; }

        /// <summary>
        /// Gets the RelyingPartySecret
        /// </summary>
        string RelyingPartySecret { get; }

        /// <summary>
        /// Gets the UserID
        /// </summary>
        string ExternalSystemUserID { get; }

        #endregion External authentication settings

        #region Content replacement

        bool ConformUrlProtocolSecurityToRequest { get; }
        string EmbeddedUrlFormat { get; }
        string EmbeddedHashtagFormat { get; }

        #endregion Content replacement
    }

    /// <summary>
    /// The 'ExternalDataSource' Data Template item
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    [Serializable, GuidAttribute("8d54736d-6004-413c-9b6a-691191ac09ae")]
    [MTemplateVersion("8d54736d-6004-413c-9b6a-691191ac09ae", "External Data Source")]
    public partial class ExternalDataSource : MedTouch.Connector.RestEndpoints.DataSource, IExternalDataSource
    {
        /// <summary>
        /// Initializes a new instance of the ExternalDataSource class.
        /// </summary>
        /// <param name="item">The Sitecore item.</param>
        internal ExternalDataSource(Item item)
            : base(item)
        {
            this.ExternalSystemUserID = item[FieldNames.ExternalSystemUserID];
            this.ExternalSystemPassword = item[FieldNames.ExternalSystemPassword];
            this.ExternalSystemAccessToken = item[FieldNames.ExternalSystemAccessToken];
            this.IsExternalSystemAccessTokenTemporary = item[FieldNames.IsExternalSystemAccessTokenTemporary] == "1";
            this.ExternalSystemAccessTokenSecret = item[FieldNames.ExternalSystemAccessTokenSecret];
            this.RelyingPartyKey = item[FieldNames.RelyingPartyKey];
            this.RelyingPartySecret = item[FieldNames.RelyingPartySecret];

            this.ConformUrlProtocolSecurityToRequest = item[FieldNames.ConformUrlProtocolSecurityToRequest] == "1";
            this.EmbeddedUrlFormat = item[FieldNames.EmbeddedUrlFormat] + "";
            this.EmbeddedHashtagFormat = item[FieldNames.EmbeddedHashtagFormat] + "";
        }

        #region Template Properties

        /// <summary>
        /// Gets the AccessToken
        /// </summary>
        /// <value>The AccessToken.</value>
        [JsonProperty]
        [MTemplateFieldVersion("468a44a4-2aeb-49ed-8108-73df272af3cd", "External System Access Token", "Multi-Line Text")]
        public string ExternalSystemAccessToken { get; protected set; }

        /// <summary>
        /// Gets whether the external system access token is temporary
        /// </summary>
        /// <value>A bool indicating whether the token is temporary</value>
        [JsonProperty]
        [MTemplateFieldVersion("a4eb2130-9f67-46c5-88ff-19a5d8782f76", "Is External System Access Token Temporary", "Checkbox")]
        public bool IsExternalSystemAccessTokenTemporary { get; protected set; }

        /// <summary>
        /// Gets the AccessTokenSecret
        /// </summary>
        /// <value>The AccessTokenSecret.</value>
        [JsonProperty]
        [MTemplateFieldVersion("e91d54c4-c5f4-4e79-a43b-bf8784197b23", "External System Access Token Secret", "Multi-Line Text")]
        public string ExternalSystemAccessTokenSecret { get; protected set; }

        /// <summary>
        /// Gets the Password
        /// </summary>
        /// <value>The Password.</value>
        [JsonProperty]
        [MTemplateFieldVersion("5aa82cde-9d78-4ab5-b81d-668c62f4fa0f", "External System Password", "Single-Line Text")]
        public string ExternalSystemPassword { get; protected set; }

        /// <summary>
        /// Gets the RelyingPartyKey
        /// </summary>
        /// <value>The RelyingPartyKey.</value>
        [JsonProperty]
        [MTemplateFieldVersion("69a9d370-e052-4da1-b473-2ae9a7f0b310", "Relying Party Key", "Multi-Line Text")]
        public string RelyingPartyKey { get; protected set; }

        /// <summary>
        /// Gets the RelyingPartySecret
        /// </summary>
        /// <value>The RelyingPartySecret.</value>
        [JsonProperty]
        [MTemplateFieldVersion("ccdff634-1567-4cbe-9e81-b1d7a8949cce", "Relying Party Secret", "Multi-Line Text")]
        public string RelyingPartySecret { get; protected set; }

        /// <summary>
        /// Gets the UserID
        /// </summary>
        /// <value>The UserID.</value>
        [JsonProperty]
        [MTemplateFieldVersion("3490fe64-00dc-4491-b3af-d21e9c06161f", "External System User ID", "Single-Line Text")]
        public string ExternalSystemUserID { get; protected set; }

        /// <summary>
        /// Gets whether ConformUrlProtocolSecurityToRequest
        /// </summary>
        /// <value>A bool indicating whether ConformUrlProtocolSecurityToRequest</value>
        [JsonProperty]
        [MTemplateFieldVersion("5c7fdbf4-5441-4add-9cb0-6b80ad87c4a6", "Conform URL Protocol Security To Request", "Checkbox")]
        public bool ConformUrlProtocolSecurityToRequest { get; protected set; }

        /// <summary>
        /// Gets a value
        /// </summary>
        /// <value>A string value</value>
        [JsonProperty]
        [MTemplateFieldVersion("f1fa851e-ea44-40fb-9a9a-cfe6370eebec", "Embedded URL Format", "Single-Line Text")]
        public string EmbeddedUrlFormat { get; protected set; }

        /// <summary>
        /// Gets a value
        /// </summary>
        /// <value>A string value</value>
        [JsonProperty]
        [MTemplateFieldVersion("3e43ed8d-ce9e-4dcd-98df-e691265d474f", "Embedded Hashtag Format", "Single-Line Text")]
        public string EmbeddedHashtagFormat { get; protected set; }

        #endregion

        #region IExternalDataSource Properties

        /// <summary>
        /// Gets the AccessToken
        /// </summary>
        string IExternalDataSource.ExternalSystemAccessToken
        {
            get
            {
                return this.ExternalSystemAccessToken;
            }
        }

        /// <summary>
        /// Gets whether the external system access token is temporary
        /// </summary>
        bool IExternalDataSource.IsExternalSystemAccessTokenTemporary
        {
            get
            {
                return this.IsExternalSystemAccessTokenTemporary;
            }
        }

        /// <summary>
        /// Gets the AccessTokenSecret
        /// </summary>
        string IExternalDataSource.ExternalSystemAccessTokenSecret
        {
            get
            {
                return this.ExternalSystemAccessTokenSecret;
            }
        }

        /// <summary>
        /// Gets the Password
        /// </summary>
        string IExternalDataSource.ExternalSystemPassword
        {
            get
            {
                return this.ExternalSystemPassword;
            }
        }

        /// <summary>
        /// Gets the RelyingPartyKey
        /// </summary>
        string IExternalDataSource.RelyingPartyKey
        {
            get
            {
                return this.RelyingPartyKey;
            }
        }

        /// <summary>
        /// Gets the RelyingPartySecret
        /// </summary>
        string IExternalDataSource.RelyingPartySecret
        {
            get
            {
                return this.RelyingPartySecret;
            }
        }

        /// <summary>
        /// Gets the UserID
        /// </summary>
        string IExternalDataSource.ExternalSystemUserID
        {
            get
            {
                return this.ExternalSystemUserID;
            }
        }

        /// <summary>
        /// Gets the ConformUrlProtocolSecurityToRequest
        /// </summary>
        bool IExternalDataSource.ConformUrlProtocolSecurityToRequest
        {
            get
            {
                return this.ConformUrlProtocolSecurityToRequest;
            }
        }

        /// <summary>
        /// Gets the EmbeddedUrlFormat
        /// </summary>
        string IExternalDataSource.EmbeddedUrlFormat
        {
            get
            {
                return this.EmbeddedUrlFormat;
            }
        }

        /// <summary>
        /// Gets the EmbeddedHashtagFormat
        /// </summary>
        string IExternalDataSource.EmbeddedHashtagFormat
        {
            get
            {
                return this.EmbeddedHashtagFormat;
            }
        }

        #endregion

        /// <summary>
        /// Performs an implicit conversion from <see cref="Sitecore.Data.Items.Item"/> to <see cref="ExternalDataSource"/>.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>
        /// The result of the conversion.
        /// </returns>
        public static implicit operator ExternalDataSource(Item item)
        {
            return item.InheritsTemplate(typeof(ExternalDataSource)) ? new ExternalDataSource(item) : null;
        }

        /// <summary>
        /// The Strongly typed field names for <see cref="ExternalDataSource"/>.
        /// </summary>
        public static new class FieldNames
        {
            /// <summary>
            /// The Field Name
            /// </summary>
            public static readonly ID ExternalSystemAccessToken = new ID("468a44a4-2aeb-49ed-8108-73df272af3cd");

            /// <summary>
            /// The Field Name
            /// </summary>
            public static readonly ID IsExternalSystemAccessTokenTemporary = new ID("a4eb2130-9f67-46c5-88ff-19a5d8782f76");

            /// <summary>
            /// The Field Name
            /// </summary>
            public static readonly ID ExternalSystemAccessTokenSecret = new ID("e91d54c4-c5f4-4e79-a43b-bf8784197b23");

            /// <summary>
            /// The Field Name
            /// </summary>
            public static readonly ID ExternalSystemPassword = new ID("5aa82cde-9d78-4ab5-b81d-668c62f4fa0f");

            /// <summary>
            /// The Field Name
            /// </summary>
            public static readonly ID RelyingPartyKey = new ID("69a9d370-e052-4da1-b473-2ae9a7f0b310");

            /// <summary>
            /// The Field Name
            /// </summary>
            public static readonly ID RelyingPartySecret = new ID("ccdff634-1567-4cbe-9e81-b1d7a8949cce");

            /// <summary>
            /// The Field Name
            /// </summary>
            public static readonly ID ExternalSystemUserID = new ID("3490fe64-00dc-4491-b3af-d21e9c06161f");

            /// <summary>
            /// The Field Name
            /// </summary>
            public static readonly ID ConformUrlProtocolSecurityToRequest = new ID("5c7fdbf4-5441-4add-9cb0-6b80ad87c4a6");

            /// <summary>
            /// The Field Name
            /// </summary>
            public static readonly ID EmbeddedUrlFormat = new ID("f1fa851e-ea44-40fb-9a9a-cfe6370eebec");

            /// <summary>
            /// The Field Name
            /// </summary>
            public static readonly ID EmbeddedHashtagFormat = new ID("3e43ed8d-ce9e-4dcd-98df-e691265d474f");

        }

        #region Utility code

        /// <summary>
        /// A compiled regular expression for matching on hashtags
        /// </summary>
        private static Regex HashtagRegex = new Regex("\\#[a-zA-Z0-9]*", RegexOptions.Compiled & RegexOptions.Singleline & RegexOptions.CultureInvariant);

        /// <summary>
        /// A compiled regular expression useful for matching on URLs in free text
        /// </summary>
        private static Regex UrlRegex = new Regex(@"(http|https|ftp|mailto)\://[a-zA-Z0-9\-\.]+\.[a-zA-Z](:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;%\$#\=~])*[^\.\,\)\(\s]", RegexOptions.Compiled & RegexOptions.Singleline & RegexOptions.CultureInvariant);

        /// <summary>
        /// Replaces the occurrences of hashtags in the specified text with the specified replacement pattern
        /// </summary>
        /// <param name="text">The text in which to replace values</param>
        /// <param name="replacementPattern">The replacement/expansion pattern to use</param>
        /// <returns>A text with all hashtags replaced with the expansion pattern</returns>
        protected static string ReplaceHashtags(string text, string replacementPattern)
        {
            if (string.IsNullOrWhiteSpace(text) || replacementPattern == null)
                return text + "";

            foreach (Match match in HashtagRegex.Matches(text))
                if (!string.IsNullOrWhiteSpace(match.Value))
                    text = text.Replace(match.Value, replacementPattern.Replace("{hashtag}", match.Value).Replace("{hashword}", match.Value.Replace("#", "")));

            return text;
        }

        /// <summary>
        /// Replaces the occurrences of URLs in the specified text with the specified replacement pattern
        /// </summary>
        /// <param name="text">The text in which to replace values</param>
        /// <param name="replacementPattern">The replacement/expansion pattern to use</param>
        /// <returns>A text with all URLs replaced with the expansion pattern</returns>
        protected static string ReplaceUrls(string text, string replacementPattern)
        {
            if (string.IsNullOrWhiteSpace(text) || replacementPattern == null)
                return text + "";

            foreach (Match match in UrlRegex.Matches(text))
            {
                if (!string.IsNullOrWhiteSpace(match.Value))
                {
                    string replacement = replacementPattern.Replace("{url}", match.Value);
                    while (replacement.IndexOf("{url}") > -1)
                        replacement = replacement.Replace("{url}", match.Value);
                    text = text.Replace(match.Value, replacement);
                }
            }

            return text;
        }

        #endregion Utility code

    }
}