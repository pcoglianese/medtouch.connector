// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IFacebookDataSource.cs" company="MedTouch">
//   Copyright (c) MedTouch 2015. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//        Developer:    sitecore\admin
// Template Version:    3.0
//             Date:    07/23/2015
// --------------------------------------------------------------------------------------------------------------------
namespace MedTouch.Connector.RestEndpoints.DataSources
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    using Newtonsoft.Json;

    using Sitecore.Data;
    using Sitecore.Data.Items;

    using Facebook;
    using Newtonsoft.Json.Linq;

    using MedTouch.Common;
    using MedTouch.Common.Versioning;
    using System.Net;
    using System.IO;
    using System.Web;
    

    /// <summary>
    /// The 'FacebookDataSource' Interface
    /// </summary>
    public interface IFacebookDataSource : IExternalDataSource
    {

    }

    /// <summary>
    /// The 'FacebookDataSource' Data Template item
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    [Serializable, GuidAttribute("3a120e73-7618-45a8-bd49-78be25296242")]
    [MTemplateVersion("3a120e73-7618-45a8-bd49-78be25296242", "Facebook Data Source")]
    public partial class FacebookDataSource : MedTouch.Connector.RestEndpoints.DataSources.ExternalDataSource, IFacebookDataSource
    {
        /// <summary>
        /// Initializes a new instance of the FacebookDataSource class.
        /// </summary>
        /// <param name="item">The Sitecore item.</param>
        internal FacebookDataSource(Item item)
            : base(item)
        {
        }

        #region Template Properties

        #endregion

        #region IFacebookDataSource Properties

        #endregion

        /// <summary>
        /// Performs an implicit conversion from <see cref="Sitecore.Data.Items.Item"/> to <see cref="FacebookDataSource"/>.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>
        /// The result of the conversion.
        /// </returns>
        public static implicit operator FacebookDataSource(Item item)
        {
            return item.InheritsTemplate(typeof(FacebookDataSource)) ? new FacebookDataSource(item) : null;
        }

        /// <summary>
        /// The Strongly typed field names for <see cref="FacebookDataSource"/>.
        /// </summary>
        public static new class FieldNames
        {

        }

        #region Custom code

        /// <summary>
        /// A Facebook-specific ISO-8601-compliant date-time format
        /// </summary>
        private const string FacebookTimestampFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'sszzz";

        /// <summary>
        /// An input stream buffer size
        /// </summary>
        private const int InputStreamBufferSize = 256;

        /// <summary>
        /// A date-time format used by the Newtonsoft JSON package
        /// </summary>
        private const string NewtonsoftTimestampFormat = "M/d/yyyy h:mm:ss tt";

        /// <summary>
        /// A Facebook profile URL format
        /// </summary>
        private const string FacebookProfileUrlFormat = "https://www.facebook.com/app_scoped_user_id/{0}";

        /// <summary>
        /// A Facebook profile picture URL format
        /// </summary>
        private const string FacebookProfilePictureUrlFormat = "//graph.facebook.com/{0}/picture?type=square";

        /// <summary>
        /// A Facebook post URL format
        /// </summary>
        private const string FacebookPostUrlFormat = "http://www.facebook.com/{0}";
//        private const string FacebookPostUrlFormat = "http://www.facebook.com/{0}/posts/{1}";

        /// <summary>
        /// A result data field name
        /// </summary>
        private const string AuthorNameFieldName = "Author Name";
        /// <summary>
        /// A result data field name
        /// </summary>
        private const string AuthorAvatarUrlFieldName = "Author Avatar URL";
        /// <summary>
        /// A result data field name
        /// </summary>
        private const string AuthorProfileUrlFieldName = "Author Profile URL";
        /// <summary>
        /// A result data field name
        /// </summary>
        private const string LinkedUrlFieldName = "Linked URL";
        /// <summary>
        /// A result data field name
        /// </summary>
        private const string ContentFieldName = "Content";

        /// <summary>
        /// A format for generating token-exchange URLs
        /// </summary>
        private const string TokenExchangeUrlFormat = "https://graph.facebook.com/oauth/access_token?client_id={0}&client_secret={1}&grant_type=fb_exchange_token&fb_exchange_token={2}";

        /// <summary>
        /// Reads raw text response output from the remote service
        /// </summary>
        /// <param name="urlFormat">The format of a URL to use for the request</param>
        /// <returns>text if it was able to be read, otherwise an empty string</returns>
        private string GetText(string url)
        {
            try
            {
                // Avoids errors from expired SSL certificates when requesting over HTTPS
                ServicePointManager.ServerCertificateValidationCallback = (s, cert, chain, ssl) => true;

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
                req.Method = "GET";
                req.ContentType = "application/x-www-form-urlencoded";
                WebResponse resp = (HttpWebResponse)req.GetResponse();

                string text;
                using (StreamReader reader = new StreamReader(new BufferedStream(resp.GetResponseStream(), InputStreamBufferSize)))
                    text = reader.ReadToEnd();

                return text + "";
            }
            catch
            {
            }

            return "";
        }

        /// <summary>
        /// Gets a long-lived Facebook access token using a short-lived one
        /// </summary>
        /// <param name="token">The short-lived token to exchange for a long one</param>
        /// <param name="request">The REST request for which the token is being requested</param>
        /// <returns>A long-lived token, with an expiration date up to 60 days in the future</returns>
        private string GetLongLivedToken(string token, RestRequest request)
        {
            if (string.IsNullOrWhiteSpace(token)) return "";
            else if (!this.IsExternalSystemAccessTokenTemporary) return token;

            request.Context.Log("Access token is expired or short-lived, exchanging for long-lived token...");

            using (new ItemMutex((MItem)this)) 
            {
                //if (!this.IsExternalSystemAccessTokenTemporary) 
                //    return this.ExternalSystemAccessToken;

                // See https://developers.facebook.com/docs/roadmap/completed-changes/offline-access-removal
                // https://graph.facebook.com/oauth/access_token?client_id=APP_ID&client_secret=APP_SECRET&grant_type=fb_exchange_token&fb_exchange_token=EXISTING_ACCESS_TOKEN
                string url = string.Format(TokenExchangeUrlFormat, this.RelyingPartyKey, this.RelyingPartySecret, token);

                request.Context.Log("Making web request...");
                token = HttpUtility.ParseQueryString(GetText(url).Trim())["access_token"];
                request.Context.Log("Received: " + token);

                Item item = this.GetOriginalItem();

                try
                {
                    if (!item.Editing.IsEditing)
                        item.Editing.BeginEdit();

                    item["Is External System Access Token Temporary"] = "0";
                    item["External System Access Token"] = token;
                }
                catch { }
                finally
                {
                    try { item.Editing.EndEdit(); } catch {}
                }

                this.IsExternalSystemAccessTokenTemporary = false;
                this.ExternalSystemAccessToken = token;

                return token;
            }            
        }

        /// <summary>
        /// Creates a data item from a JSON parsed token
        /// </summary>
        /// <param name="jsonItem">A parsed JSON token</param>
        /// <param name="request">The REST request</param>
        /// <returns>A new data item, or null if one cannot be parsed</returns>
        private SortedDictionary<string, object> CreateStatusItem(JToken jsonItem, RestRequest request)
        {
            SortedDictionary<string, object> item = new SortedDictionary<string, object>();

            string postID = jsonItem["id"].ToString();
            string authorID = "";

            item.SetExternalID(postID);
            try
            {
                DateTime createdTime = DateTime.Now;
                if (DateTime.TryParseExact(jsonItem["created_time"].ToString(), NewtonsoftTimestampFormat, null, System.Globalization.DateTimeStyles.None, out createdTime))
                    item.SetTimestamp(createdTime);
                else
                    item.SetTimestamp(jsonItem["created_time"].ToString());
            } catch { }
            
            item.SetUrl(string.Format(FacebookPostUrlFormat, postID));

            try { item[AuthorNameFieldName] = jsonItem["from"]["name"].ToString(); } catch { }
            try { authorID = jsonItem["from"]["id"].ToString(); } catch { }
            if (!string.IsNullOrWhiteSpace(authorID))
            {
                item[AuthorAvatarUrlFieldName] = string.Format(FacebookProfilePictureUrlFormat, authorID);
                item[AuthorProfileUrlFieldName] = string.Format(FacebookProfileUrlFormat, authorID);
            }

            try { item[LinkedUrlFieldName] = jsonItem["link"].ToString(); } catch { }
            if (!item.ContainsKey(LinkedUrlFieldName))
                try { item[LinkedUrlFieldName] = jsonItem["picture"].ToString().Replace("http://", "//"); } catch {}
            if (!item.ContainsKey(LinkedUrlFieldName))
                try { item[LinkedUrlFieldName] = jsonItem["source"].ToString(); } catch {}
            if (item.ContainsKey(LinkedUrlFieldName) && string.IsNullOrWhiteSpace(item[LinkedUrlFieldName].ToString()))
                item.Remove(LinkedUrlFieldName);

            string content = jsonItem.Get("message", "name");
            if (!string.IsNullOrWhiteSpace(content))
            {
                if (!string.IsNullOrWhiteSpace(this.EmbeddedUrlFormat))
                    content = ReplaceUrls(content, this.EmbeddedUrlFormat);

                if (!string.IsNullOrWhiteSpace(this.EmbeddedHashtagFormat))
                    content = ReplaceHashtags(content, this.EmbeddedHashtagFormat);

                if (!string.IsNullOrWhiteSpace(this.EmbeddedUrlFormat) || !string.IsNullOrWhiteSpace(this.EmbeddedHashtagFormat))
                    content = content.ReplaceFieldTokens(item).ReplaceDictionarySettingTokens();

                if (this.ConformUrlProtocolSecurityToRequest && request.HttpRequest.RawUrl.StartsWith("https://"))
                    while (content.Contains("http://"))
                        content = content.Replace("http://", "https://");
            }

            item[ContentFieldName] = content;

            return item;
        }

        /// <summary>
        /// Creates a data item from a JSON parsed token
        /// </summary>
        /// <param name="jsonItem">A parsed JSON token</param>
        /// <param name="request">The REST request</param>
        /// <returns>A new data item, or null if one cannot be parsed</returns>
        private SortedDictionary<string, object> CreateLinkItem(JToken jsonItem, RestRequest request)
        {
            SortedDictionary<string, object> item = new SortedDictionary<string, object>();

            string postID = jsonItem["id"].ToString();
            string authorID = "";

            item.SetExternalID(postID);
            try
            {
                DateTime createdTime = DateTime.Now;
                if (DateTime.TryParseExact(jsonItem["created_time"].ToString(), NewtonsoftTimestampFormat, null, System.Globalization.DateTimeStyles.None, out createdTime))
                    item.SetTimestamp(createdTime);
                else
                    item.SetTimestamp(jsonItem["created_time"].ToString());
            } catch { }
            
            item.SetUrl(string.Format(FacebookPostUrlFormat, postID));

            try { item[AuthorNameFieldName] = jsonItem["from"]["name"].ToString(); } catch { }
            try { authorID = jsonItem["from"]["id"].ToString(); } catch { }
            if (!string.IsNullOrWhiteSpace(authorID))
            {
                item[AuthorAvatarUrlFieldName] = string.Format(FacebookProfilePictureUrlFormat, authorID);
                item[AuthorProfileUrlFieldName] = string.Format(FacebookProfileUrlFormat, authorID);
            }

            try { item[LinkedUrlFieldName] = jsonItem["link"].ToString(); } catch { }
            if (!item.ContainsKey(LinkedUrlFieldName))
                try { item[LinkedUrlFieldName] = jsonItem["source"].ToString(); } catch {}
            if (!item.ContainsKey(LinkedUrlFieldName))
                try { item[LinkedUrlFieldName] = jsonItem["picture"].ToString().Replace("http://", "//"); } catch {}

            string content = jsonItem.Get("description", "message", "name");
            if (!string.IsNullOrWhiteSpace(content))
            {
                if (!string.IsNullOrWhiteSpace(this.EmbeddedUrlFormat))
                    content = ReplaceUrls(content, this.EmbeddedUrlFormat);

                if (!string.IsNullOrWhiteSpace(this.EmbeddedHashtagFormat))
                    content = ReplaceHashtags(content, this.EmbeddedHashtagFormat);

                if (!string.IsNullOrWhiteSpace(this.EmbeddedUrlFormat) || !string.IsNullOrWhiteSpace(this.EmbeddedHashtagFormat))
                    content = content.ReplaceFieldTokens(item).ReplaceDictionarySettingTokens();

                if (this.ConformUrlProtocolSecurityToRequest && request.HttpRequest.RawUrl.StartsWith("https://"))
                    while (content.Contains("http://"))
                        content = content.Replace("http://", "https://");
            }

            item[ContentFieldName] = content;
            
            return item;
        }

        /// <summary>
        /// Creates a data item from a JSON parsed token
        /// </summary>
        /// <param name="jsonItem">A parsed JSON token</param>
        /// <param name="request">The REST request</param>
        /// <returns>A new data item, or null if one cannot be parsed</returns>
        private SortedDictionary<string, object> CreatePhotoItem(JToken jsonItem, RestRequest request)
        {
            SortedDictionary<string, object> item = new SortedDictionary<string, object>();

            string postID = jsonItem["id"].ToString();
            string authorID = "";

            item.SetExternalID(postID);
            try
            {
                DateTime createdTime = DateTime.Now;
                if (DateTime.TryParseExact(jsonItem["created_time"].ToString(), NewtonsoftTimestampFormat, null, System.Globalization.DateTimeStyles.None, out createdTime))
                    item.SetTimestamp(createdTime);
                else
                    item.SetTimestamp(jsonItem["created_time"].ToString());
            } catch { }
            
            item.SetUrl(string.Format(FacebookPostUrlFormat, postID));

            try { item[AuthorNameFieldName] = jsonItem["from"]["name"].ToString(); } catch { }
            try { authorID = jsonItem["from"]["id"].ToString(); } catch { }
            if (!string.IsNullOrWhiteSpace(authorID))
            {
                item[AuthorAvatarUrlFieldName] = string.Format(FacebookProfilePictureUrlFormat, authorID);
                item[AuthorProfileUrlFieldName] = string.Format(FacebookProfileUrlFormat, authorID);
            }

            try { item[LinkedUrlFieldName] = jsonItem["source"].ToString().Replace("http://", "//"); } catch { }
            if (!item.ContainsKey(LinkedUrlFieldName))
                try { item[LinkedUrlFieldName] = jsonItem["picture"].ToString().Replace("http://", "//"); } catch {}
            if (!item.ContainsKey(LinkedUrlFieldName))
                try { item[LinkedUrlFieldName] = jsonItem["link"].ToString().Replace("http://", "//"); } catch {}

            string content = jsonItem.Get("description", "message", "name");
            if (!string.IsNullOrWhiteSpace(content))
            {
                if (!string.IsNullOrWhiteSpace(this.EmbeddedUrlFormat))
                    content = ReplaceUrls(content, this.EmbeddedUrlFormat);

                if (!string.IsNullOrWhiteSpace(this.EmbeddedHashtagFormat))
                    content = ReplaceHashtags(content, this.EmbeddedHashtagFormat);

                if (!string.IsNullOrWhiteSpace(this.EmbeddedUrlFormat) || !string.IsNullOrWhiteSpace(this.EmbeddedHashtagFormat))
                    content = content.ReplaceFieldTokens(item).ReplaceDictionarySettingTokens();

                if (this.ConformUrlProtocolSecurityToRequest && request.HttpRequest.RawUrl.StartsWith("https://"))
                    while (content.Contains("http://"))
                        content = content.Replace("http://", "https://");
            }

            item[ContentFieldName] = content;

            return item;
        }

        /// <summary>
        /// Creates a data item from a JSON parsed token
        /// </summary>
        /// <param name="jsonItem">A parsed JSON token</param>
        /// <param name="request">The REST request</param>
        /// <returns>A new data item, or null if one cannot be parsed</returns>
        private SortedDictionary<string, object> CreateVideoItem(JToken jsonItem, RestRequest request)
        {
            SortedDictionary<string, object> item = new SortedDictionary<string, object>();

            string postID = jsonItem["id"].ToString();
            string authorID = "";

            item.SetExternalID(postID);
            try
            {
                DateTime createdTime = DateTime.Now;
                if (DateTime.TryParseExact(jsonItem["created_time"].ToString(), NewtonsoftTimestampFormat, null, System.Globalization.DateTimeStyles.None, out createdTime))
                    item.SetTimestamp(createdTime);
                else
                    item.SetTimestamp(jsonItem["created_time"].ToString());
            } catch { }
            
            item.SetUrl(string.Format(FacebookPostUrlFormat, postID));

            try { item[AuthorNameFieldName] = jsonItem["from"]["name"].ToString(); } catch { }
            try { authorID = jsonItem["from"]["id"].ToString(); } catch { }
            if (!string.IsNullOrWhiteSpace(authorID))
            {
                item[AuthorAvatarUrlFieldName] = string.Format(FacebookProfilePictureUrlFormat, authorID);
                item[AuthorProfileUrlFieldName] = string.Format(FacebookProfileUrlFormat, authorID);
            }

            item[LinkedUrlFieldName] = jsonItem.Get("source", "full_picture", "picture", "link");

            string content = jsonItem.Get("description", "message", "name");
            if (!string.IsNullOrWhiteSpace(content))
            {
                if (!string.IsNullOrWhiteSpace(this.EmbeddedUrlFormat))
                    content = ReplaceUrls(content, this.EmbeddedUrlFormat);

                if (!string.IsNullOrWhiteSpace(this.EmbeddedHashtagFormat))
                    content = ReplaceHashtags(content, this.EmbeddedHashtagFormat);

                if (!string.IsNullOrWhiteSpace(this.EmbeddedUrlFormat) || !string.IsNullOrWhiteSpace(this.EmbeddedHashtagFormat))
                    content = content.ReplaceFieldTokens(item).ReplaceDictionarySettingTokens();

                if (this.ConformUrlProtocolSecurityToRequest && request.HttpRequest.RawUrl.StartsWith("https://"))
                    while (content.Contains("http://"))
                        content = content.Replace("http://", "https://");
            }

            item[ContentFieldName] = content;
            
            return item;
        }

        protected override void GetData(RestRequest request, List<SortedDictionary<string, object>> data, List<string> sources)
        {
            if (request == null || data == null) throw new ArgumentNullException();

            request.Context.Log("Checking access token longevity...");
            string token = this.ExternalSystemAccessToken;
            if (this.IsExternalSystemAccessTokenTemporary)
                token = GetLongLivedToken(token, request);

            request.Context.Log("Creating Facebook client...");
            FacebookClient facebookClient = new FacebookClient(token);
            facebookClient.AppId = this.RelyingPartyKey;
            facebookClient.AppSecret = this.RelyingPartySecret;

            request.Context.Log("Accessing feed...");
//            JsonObject envelope = (JsonObject)facebookClient.Get("/me/feed");
//            JsonObject envelope = (JsonObject)facebookClient.Get("/me/posts?fields=message,created_time,id,description,from,name,picture,source,to,type");

            string json = facebookClient.Get("/me/posts?fields=message,created_time,id,description,from,name,full_picture,picture,source,to,type").ToString();
          
//            Used to inspect raw output during testing:
//            request.Context.Response.Write(Environment.NewLine + Environment.NewLine + envelope.ToString() + Environment.NewLine + Environment.NewLine);

            bool authorIDWarningWritten = false;

            JArray jsonItems = JArray.Parse(JObject.Parse(json)["data"].ToString());

            for (int x = 0; x < jsonItems.Count; x++)
            {
                JToken jsonItem = jsonItems[x];
                try
                {
                    SortedDictionary<string, object> item = null;

                    switch (jsonItem["type"].ToString())
                    {
                        case "status":
                            item = CreateStatusItem(jsonItem, request);
                            break;
                        case "link":
                            item = CreateLinkItem(jsonItem, request);
                            break;
                        case "photo":
                            item = CreatePhotoItem(jsonItem, request);
                            break;
                        case "video":
                            item = CreateVideoItem(jsonItem, request);
                            break;
                        default:
                            break;
                    }

                    if (item != null)
                        data.Add(item);
                }
                catch { }
            }
        }

        #endregion Custom code

    }
}