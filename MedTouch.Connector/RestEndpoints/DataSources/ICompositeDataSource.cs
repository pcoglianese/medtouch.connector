// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICompositeDataSource.cs" company="MedTouch">
//   Copyright (c) MedTouch 2015. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//        Developer:    sitecore\admin
// Template Version:    3.0
//             Date:    07/23/2015
// --------------------------------------------------------------------------------------------------------------------
namespace MedTouch.Connector.RestEndpoints.DataSources
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.InteropServices;
    using System.Text;

    using MedTouch.Common;
    using MedTouch.Common.Versioning;

    using Newtonsoft.Json;

    using Sitecore.Data;
    using Sitecore.Data.Items;

    /// <summary>
    /// The 'CompositeDataSource' Interface
    /// </summary>
    public interface ICompositeDataSource : IMItem, IDataSource
    {
        /// <summary>
        /// Gets the DataSources
        /// </summary>
        List<IDataSource> DataSources { get; }

    }

    /// <summary>
    /// The 'CompositeDataSource' Data Template item
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    [Serializable, GuidAttribute("04b9a6bb-0820-4e0d-b638-34237238babb")]
    [MTemplateVersion("04b9a6bb-0820-4e0d-b638-34237238babb", "Composite Data Source")]
    public partial class CompositeDataSource : MedTouch.Connector.RestEndpoints.DataSource, ICompositeDataSource
    {
        /// <summary>
        /// A template name
        /// </summary>
        private const string FacebookDataSourceTemplateName = "Facebook Data Source";

        /// <summary>
        /// A template name
        /// </summary>
        private const string InternalDataSourceTemplateName = "Internal Data Source";

        /// <summary>
        /// A template name
        /// </summary>
        private const string PinterestDataSourceTemplateName = "Pinterest Data Source";

        /// <summary>
        /// A template name
        /// </summary>
        private const string TwitterDataSourceTemplateName = "Twitter Data Source";

        /// <summary>
        /// A template name
        /// </summary>
        private const string YoutubeDataSourceTemplateName = "Youtube Data Source";

        /// <summary>
        /// Initializes a new instance of the CompositeDataSource class.
        /// </summary>
        /// <param name="item">The Sitecore item.</param>
        internal CompositeDataSource(Item item)
            : base(item)
        {
            this.DataSources = new List<IDataSource>();

            IDataSource dataSource;
            foreach(MItem mItem in item.Fields[FieldNames.DataSources].ConvertToMItems<MItem>()) 
            {
                dataSource = null;

                switch (mItem.TemplateName)
                {
                    case FacebookDataSourceTemplateName:
                        try
                        {
                            dataSource = mItem.GetOriginalItem().AsMItemTypeOf<FacebookDataSource>();
                        }
                        catch { }
                        break;
                    case InternalDataSourceTemplateName:
                        try
                        {
                            dataSource = mItem.GetOriginalItem().AsMItemTypeOf<InternalDataSource>();
                        }
                        catch { }
                        break;
                    case PinterestDataSourceTemplateName:
                        try
                        {
                            dataSource = mItem.GetOriginalItem().AsMItemTypeOf<PinterestDataSource>();
                        }
                        catch { }
                        break;
                    case TwitterDataSourceTemplateName:
                        try
                        {
                            dataSource = mItem.GetOriginalItem().AsMItemTypeOf<TwitterDataSource>();
                        }
                        catch { }
                        break;
                    case YoutubeDataSourceTemplateName:
                        try
                        {
                            dataSource = mItem.GetOriginalItem().AsMItemTypeOf<YoutubeDataSource>();
                        }
                        catch { }
                        break;
                }

                if (dataSource != null)
                    DataSources.Add(dataSource);
            }
        }

        #region Template Properties

        /// <summary>
        /// Gets the DataSources
        /// </summary>
        /// <value>The DataSources.</value>
        [JsonProperty]
        [MTemplateFieldVersion("70143369-fa78-45ba-ae0b-fcf78ec8b8ac", "Data Sources", "Treelist")]
        public List<IDataSource> DataSources { get; private set; }

        #endregion

        #region ICompositeDataSource Properties

        /// <summary>
        /// Gets the DataSources
        /// </summary>
        List<IDataSource> ICompositeDataSource.DataSources
        {
            get
            {
                return this.DataSources;
            }
        }

        #endregion

        /// <summary>
        /// Performs an implicit conversion from <see cref="Sitecore.Data.Items.Item"/> to <see cref="CompositeDataSource"/>.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>
        /// The result of the conversion.
        /// </returns>
        public static implicit operator CompositeDataSource(Item item)
        {
            return item.InheritsTemplate(typeof(CompositeDataSource)) ? new CompositeDataSource(item) : null;
        }

        /// <summary>
        /// The Strongly typed field names for <see cref="CompositeDataSource"/>.
        /// </summary>
        public static new class FieldNames
        {
            /// <summary>
            /// The Field Name
            /// </summary>
            public static readonly ID DataSources = new ID("70143369-fa78-45ba-ae0b-fcf78ec8b8ac");

        }


        #region Custom code

        public override bool IsValid
        {
            get 
            {
                foreach (IDataSource dataSource in this.DataSources)
                    if (!dataSource.IsValid)
                        return false;

                return true;
            }
        }

        public override string ValidationMessage
        {
            get 
            {
                StringBuilder sb = new StringBuilder();

                string message;
                foreach (IDataSource dataSource in this.DataSources)
                {
                    message = dataSource.ValidationMessage;

                    if (!string.IsNullOrWhiteSpace(message))
                    {
                        if (sb.Length > 0)
                            sb.Append("; ");

                        sb.Append(dataSource.GetType().FullName);
                        if (!string.IsNullOrWhiteSpace(dataSource.Name))
                            sb.Append(" (").Append(dataSource.Name).Append(")");
                        sb.Append(": ").Append(message);
                    }
                }

                return sb.ToString();
            }
        }

        /// <summary>
        /// Gets data from this data source
        /// </summary>
        /// <returns>A list of data objects</returns>
        public override List<SortedDictionary<string, object>> GetData(RestRequest request)
        {
            if (request == null) throw new ArgumentNullException();

            List<SortedDictionary<string, object>> data = new List<SortedDictionary<string, object>>();
            if (!this.IsActive) return data;

            List<SortedDictionary<string, object>> childData;
            foreach (IDataSource dataSource in DataSources)
            {
                try
                {
                    childData = dataSource.GetData(request);
                    if (childData.Count > 0) 
                    {
                        if (data.Count > 0) 
                        {
                            data = data.Union(childData).ToList();
                        }
                    }
                }
                catch (Exception e)
                {
                    request.Context.Log(e);
                }
            }
            
            this.FieldTransforms.Transform(data);

            return data;
        }

        #endregion Custom code


    }
}