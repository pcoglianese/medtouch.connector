// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IYoutubeDataSource.cs" company="MedTouch">
//   Copyright (c) MedTouch 2015. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//        Developer:    sitecore\admin
// Template Version:    3.0
//             Date:    07/23/2015
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Text;
using System.Web;

//using Google.Apis.Auth.OAuth2;
//using Google.Apis.Services;
//using Google.Apis.Upload;
//using Google.Apis.Util.Store;
//using Google.Apis.YouTube.v3;
//using Google.Apis.YouTube.v3.Data;


using Sitecore.Data;
using Sitecore.Data.Items;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using MedTouch.Common;
using MedTouch.Common.Versioning;


namespace MedTouch.Connector.RestEndpoints.DataSources
{    
    /// <summary>
    /// The 'YoutubeDataSource' Interface
    /// </summary>
    public interface IYoutubeDataSource : IExternalDataSource
    {
        /// <summary>
        /// Gets the playlist IDs
        /// </summary>
        List<string> PlaylistIDs { get; }

        /// <summary>
        /// Gets the channel IDs
        /// </summary>
        List<string> ChannelIDs { get; }

    }

    /// <summary>
    /// The 'YoutubeDataSource' Data Template item
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    [Serializable, GuidAttribute("40ee9863-d097-46dc-9c45-7c46fc4e6dd9")]
    [MTemplateVersion("40ee9863-d097-46dc-9c45-7c46fc4e6dd9", "Youtube Data Source")]
    public partial class YoutubeDataSource : MedTouch.Connector.RestEndpoints.DataSources.ExternalDataSource, IYoutubeDataSource
    {
        /// <summary>
        /// Initializes a new instance of the YoutubeDataSource class.
        /// </summary>
        /// <param name="item">The Sitecore item.</param>
        internal YoutubeDataSource(Item item)
            : base(item)
        {
            this.ChannelIDs = item[FieldNames.ChannelIDs].GetMultipleFieldValues();
            this.PlaylistIDs = item[FieldNames.PlaylistIDs].GetMultipleFieldValues();
        }

        #region Template Properties

        /// <summary>
        /// Gets the Playlist IDs
        /// </summary>
        /// <value>The Playlist IDs</value>
        [JsonProperty]
        [MTemplateFieldVersion("856a0a47-187c-481c-9cc6-c1417cea99b7", "Channel IDs", "Single-Line Text")]
        public List<string> ChannelIDs { get; private set; }

        /// <summary>
        /// Gets the Playlist IDs
        /// </summary>
        /// <value>The Playlist IDs</value>
        [JsonProperty]
        [MTemplateFieldVersion("d0455536-0828-4b4d-b457-5a7efe01a2eb", "Playlist IDs", "Single-Line Text")]
        public List<string> PlaylistIDs { get; private set; }

        #endregion

        #region IYoutubeDataSource Properties

        /// <summary>
        /// Gets the channel IDs
        /// </summary>
        List<string> IYoutubeDataSource.ChannelIDs
        {
            get
            {
                return this.ChannelIDs;
            }
        }

        /// <summary>
        /// Gets the playlist IDs
        /// </summary>
        List<string> IYoutubeDataSource.PlaylistIDs
        {
            get
            {
                return this.PlaylistIDs;
            }
        }

        #endregion

        /// <summary>
        /// Performs an implicit conversion from <see cref="Sitecore.Data.Items.Item"/> to <see cref="YoutubeDataSource"/>.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>
        /// The result of the conversion.
        /// </returns>
        public static implicit operator YoutubeDataSource(Item item)
        {
            return item.InheritsTemplate(typeof(YoutubeDataSource)) ? new YoutubeDataSource(item) : null;
        }

        /// <summary>
        /// The Strongly typed field names for <see cref="YoutubeDataSource"/>.
        /// </summary>
        public static new class FieldNames
        {
            /// <summary>
            /// The Field Name
            /// </summary>
            public static readonly ID ChannelIDs = new ID("856a0a47-187c-481c-9cc6-c1417cea99b7");

            /// <summary>
            /// The Field Name
            /// </summary>
            public static readonly ID PlaylistIDs = new ID("d0455536-0828-4b4d-b457-5a7efe01a2eb");
        }

        #region Custom code

        /// <summary>
        /// A result data field name
        /// </summary>
        private const string AuthorNameFieldName = "Author Name";
        /// <summary>
        /// A result data field name
        /// </summary>
        private const string AuthorAvatarUrlFieldName = "Author Avatar URL";
        /// <summary>
        /// A result data field name
        /// </summary>
        private const string AuthorProfileUrlFieldName = "Author Profile URL";
        /// <summary>
        /// A result data field name
        /// </summary>
        private const string EmbeddedUrlFieldName = "Embedded URL";
        /// <summary>
        /// A result data field name
        /// </summary>
        private const string UrlFieldName = "URL";
        /// <summary>
        /// A result data field name
        /// </summary>
        private const string ThumbnailUrlFieldName = "Thumbnail URL";
        /// <summary>
        /// A result data field name
        /// </summary>
        private const string ContentFieldName = "Content";
        /// <summary>
        /// A result data field name
        /// </summary>
        private const string TitleFieldName = "Title";

        /// <summary>
        /// A result data field name
        /// </summary>
        private const string ChannelIDFieldName = "Channel ID";

        /// <summary>
        /// A result data field name
        /// </summary>
        private const string ChannelTitleFieldName = "Channel Title";

        /// <summary>
        /// A result data field name
        /// </summary>
        private const string PlaylistIDFieldName = "Playlist ID";

        /// <summary>
        /// A format for constructing URLs to shared content
        /// </summary>
        // private const string SearchUrlFormat = "https://www.googleapis.com/youtube/v3/search?part=snippet&q={1}&type=video&key={0}";

        /// <summary>
        /// A format for constructing URLs to list data
        /// </summary>
        private const string UserPlaylistsUrlFormat = "https://www.googleapis.com/youtube/v3/channels?part=contentDetails,snippet&forUsername={1}&key={0}";

        /// <summary>
        /// A format for constructing URLs to list data
        /// </summary>
        private const string ChannelPlaylistsUrlFormat = "https://www.googleapis.com/youtube/v3/channels?part=contentDetails,snippet&id={1}&key={0}";

        /// <summary>
        /// A format for constructing URLs to list data
        /// </summary>
        private const string PlaylistVideosUrlFormat = "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&playlistId={1}&key={0}";

        /// <summary>
        /// A format for constructing URLs to list data
        /// </summary>
        private const string ChannelVideosUrlFormat = "https://www.googleapis.com/youtube/v3/search?part=snippet&channelId={1}&type=video&key={0}";

        /// <summary>
        /// A format for constructing URLs to list data
        /// </summary>
        private const string ChannelAndVideosUrlFormat = "https://www.googleapis.com/youtube/v3/search?part=snippet&channelId={1}&type=video,channel&key={0}";

        /// <summary>
        /// A format for constructing URLs to list data
        /// </summary>
        private const string UserChannelsUrlFormat = "https://www.googleapis.com/youtube/v3/channels?part=snippet&forUsername={1}&key={0}";

        /// <summary>
        /// A format for constructing URLs to shared content
        /// </summary>
        private const string VideoEmbeddedUrlFormat = "https://www.youtube.com/embed/{0}";

        /// <summary>
        /// A format for URLs to videos hosted externally
        /// </summary>
        private const string VideoUrlFormat = "https://www.youtube.com/watch?v={0}";

        /// <summary>
        /// A format for URLs to Youtube profiles
        /// </summary>
        private const string ProfileUrlFormat = "https://www.youtube.com/user/{0}";

//        private const string YoutubeTimestampFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff";

        /// <summary>
        /// A date-time format used by the Newtonsoft JSON package
        /// </summary>
        private const string NewtonsoftTimestampFormat = "M/d/yyyy h:mm:ss tt";


        /// <summary>
        /// Indicates whether this data source is correctly configured
        /// </summary>
        public override bool IsValid
        {
            get
            {
                return (!string.IsNullOrWhiteSpace(this.RelyingPartyKey) || (string.IsNullOrWhiteSpace(this.ExternalSystemUserID) && this.ChannelIDs.Count == 0 && this.PlaylistIDs.Count == 0));
            }
        }

        /// <summary>
        /// If this data source is correctly configured, returns an empty string, otherwise an appropriate message
        /// </summary>
        public override string ValidationMessage
        {
            get
            {
                if (IsValid) return "";
                else return "Configuration must include a relying-party/API key and at least a user ID, channel ID or playlist ID";
            }
        }


        /// <summary>
        /// The input stream buffer size
        /// </summary>
        private const int InputStreamBufferSize = 8192;

        /// <summary>
        /// Reads JSON from the remote service
        /// </summary>
        /// <param name="urlFormat">The format of a URL to use for the request</param>
        /// <returns>JSON if it was able to be read, otherwise an empty string</returns>
        private string GetJson(string url)
        {
            try
            {
                // Avoids errors from expired SSL certificates when requesting over HTTPS
                ServicePointManager.ServerCertificateValidationCallback = (s, cert, chain, ssl) => true;

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
                req.Method = "GET";
                req.ContentType = "application/x-www-form-urlencoded";
                WebResponse resp = (HttpWebResponse)req.GetResponse();

                string json;
                using (StreamReader reader = new StreamReader(new BufferedStream(resp.GetResponseStream(), InputStreamBufferSize)))
                    json = reader.ReadToEnd();

                return json;
            }
            catch
            {
            }

            return "";
        }

        /// <summary>
        /// Finds channel IDs for this data source's
        /// </summary>
        /// <param name="url">The URL at which to request the list of channels</param>
        /// <param name="channelIDs">The list of IDs to which to add</param>
        /// <param name="channelAvatarUrlMap">A map of channel IDs to avatar URLs</param>
        private void FindChannelIDs(List<string> channelIDs, Dictionary<string, string> channelAvatarUrlMap)
        {
            string url = string.Format(UserChannelsUrlFormat, this.RelyingPartyKey, this.ExternalSystemUserID);
            string json = GetJson(url);

            JArray channels = JArray.Parse(JObject.Parse(json)["items"].ToString());
            for (int x = 0; x < channels.Count; x++)
            {
                try
                {
                    string channelID = channels[x]["id"].ToString();
                    channelIDs.Add(channelID);

                    string channelAvatarUrl = channels[x]["snippet"]["thumbnails"]["default"]["url"].ToString().Replace("http://", "https://");

                    if (!string.IsNullOrWhiteSpace(channelID) && !string.IsNullOrWhiteSpace(channelAvatarUrl))
                        channelAvatarUrlMap[channelID] = channelAvatarUrl;
                }
                catch { }
            }
        }

        /// <summary>
        /// Finds playlists
        /// </summary>
        /// <param name="url">The URL at which to request the list of playlists</param>
        /// <param name="playlistIDs">The list of IDs to which to add</param>
        /// <param name="channelAvatarUrlMap">A map of channel IDs to avatar URLs</param>
        private void FindPlaylistIDs(string url, List<string> playlistIDs, Dictionary<string, string> channelAvatarUrlMap)
        {
            string json = GetJson(url);

            JArray channels = JArray.Parse(JObject.Parse(json)["items"].ToString());
            for (int x = 0; x < channels.Count; x++)
            {
                try
                {
                    playlistIDs.Add(channels[x]["contentDetails"]["relatedPlaylists"]["uploads"].ToString());
                }
                catch { }

                try
                {
                    string channelID = channels[x]["id"].ToString();
                    string channelAvatarUrl = channels[x]["snippet"]["thumbnails"]["default"]["url"].ToString().Replace("http://", "https://");

                    if (!string.IsNullOrWhiteSpace(channelID) && !string.IsNullOrWhiteSpace(channelAvatarUrl))
                        channelAvatarUrlMap[channelID] = channelAvatarUrl;
                }
                catch { }
            }
        }

        /// <summary>
        /// Finds the playlists for the specified user
        /// </summary>
        /// <param name="userID">The user ID for which to find playlists</param>
        /// <param name="playlistIDs">The list of IDs to which to add</param>
        /// <param name="channelAvatarUrlMap">A map of channel IDs to avatar URLs</param>
        private void FindPlaylistIDsByUserID(string userID, List<string> playlistIDs, Dictionary<string, string> channelAvatarUrlMap)
        {
            FindPlaylistIDs(string.Format(UserPlaylistsUrlFormat, this.RelyingPartyKey, this.ExternalSystemUserID), playlistIDs, channelAvatarUrlMap); 
        }

        /// <summary>
        /// Finds the playlists for the specified channel
        /// </summary>
        /// <param name="channelIDs">The ID of the channels for which to find the playlist IDs</param>
        /// <param name="playlistIDs">The list of IDs to which to add</param>
        /// <param name="channelAvatarUrlMap">A map of channel IDs to avatar URLs</param>
        private void FindPlaylistIDsByChannelIDs(List<string> channelIDs, List<string> playlistIDs, Dictionary<string, string> channelAvatarUrlMap)
        {
            FindPlaylistIDs(string.Format(ChannelPlaylistsUrlFormat, this.RelyingPartyKey, channelIDs.Join(",")), playlistIDs, channelAvatarUrlMap);
        }

        /// <summary>
        /// Gets a mapping of channel to avatar URLs for the specified user's channels
        /// </summary>
        /// <param name="userID">The user ID for which to get the avatar URL mappings</param>
        /// <returns>A mapping of channel IDs to avatar URLs</returns>
        private Dictionary<string, string> FindChannelAvatarUrls(string userID)
        {
            Dictionary<string, string> map = new Dictionary<string, string>();
            if (string.IsNullOrWhiteSpace(userID)) return map;

            string url = string.Format(UserChannelsUrlFormat, this.RelyingPartyKey, userID);
            string json = GetJson(url);

            JArray channels = JArray.Parse(JObject.Parse(json)["items"].ToString());
            string channelID, channelAvatarUrl;
            for (int x = 0; x < channels.Count; x++)
            {
                try 
                {
                    channelID = channels[x]["id"].ToString();
                    channelAvatarUrl = channels[x]["snippet"]["thumbnails"]["default"]["url"].ToString().Replace("http://", "https://");

                    if (!string.IsNullOrWhiteSpace(channelID) && !string.IsNullOrWhiteSpace(channelAvatarUrl))
                        map[channelID] = channelAvatarUrl;
                }
                catch {}
            }

            return map;
        }

        private string profileUrl;

        /// <summary>
        /// Gets the profile URL for this user
        /// </summary>
        private string ProfileUrl
        {
            get
            {
                if (string.IsNullOrWhiteSpace(profileUrl)) 
                    profileUrl = string.Format(ProfileUrlFormat, this.ExternalSystemUserID);

                return profileUrl;
            }
        }

        /// <summary>
        /// Creates an item from video data
        /// </summary>
        /// <param name="video">The video data to use</param>
        /// <param name="videoID">The external ID of the video</param>
        /// <param name="avatarUrlMap">The channel/avatar-URL map</param>
        /// <returns>A new item</returns>
        private SortedDictionary<string, object> CreateItem(JToken video, string videoID, Dictionary<string, string> avatarUrlMap, RestRequest request)
        {
            SortedDictionary<string, object> item = new SortedDictionary<string, object>();

            string channelTitle = "";
            try { channelTitle = video["channelTitle"].ToString(); } catch {}
            string channelID = "";
            try { channelID = video["channelId"].ToString(); } catch {}

            string videoUrl = string.Format(VideoUrlFormat, videoID);

            item.SetExternalID(videoID);
            item.SetDataSourceName(this.Name);
            item.SetUrl(videoUrl);
            try 
            {
                // Sample: "publishedAt": "2014-09-29T17:23:28.000Z",
                DateTime videoTimestamp = DateTime.Now;
                string publishedDateString = video["publishedAt"].ToString();
                if (DateTime.TryParseExact(publishedDateString, NewtonsoftTimestampFormat, null, System.Globalization.DateTimeStyles.None, out videoTimestamp))
                    item.SetTimestamp(videoTimestamp);
                else
                    item.SetTimestamp(publishedDateString);
            }
            catch { }

            item[AuthorNameFieldName] = channelTitle;

            string authorAvatarUrl = "";
            if (!string.IsNullOrWhiteSpace(channelID) && avatarUrlMap.ContainsKey(channelID))
                authorAvatarUrl = avatarUrlMap[channelID];
            if (string.IsNullOrWhiteSpace(authorAvatarUrl))
                authorAvatarUrl = avatarUrlMap.Values.FirstOrDefault() + "";
            if (!string.IsNullOrWhiteSpace(authorAvatarUrl))
                item[AuthorAvatarUrlFieldName] = authorAvatarUrl;
                    
            item[AuthorProfileUrlFieldName] = ProfileUrl;
            item[EmbeddedUrlFieldName] = string.Format(VideoEmbeddedUrlFormat, videoID);
            item[UrlFieldName] = videoUrl;
            try { item[ThumbnailUrlFieldName] = video["thumbnails"]["default"]["url"].ToString().Replace("http://", "https://"); } catch { }
            try { item[TitleFieldName] = video["title"].ToString(); } catch {}

            #region Set content

            string content = "";
            try { content = video["description"].ToString(); } catch {}

            if (!string.IsNullOrWhiteSpace(this.EmbeddedUrlFormat))
                content = ReplaceUrls(content, this.EmbeddedUrlFormat);

            if (!string.IsNullOrWhiteSpace(this.EmbeddedHashtagFormat))
                content = ReplaceHashtags(content, this.EmbeddedHashtagFormat);

            if (!string.IsNullOrWhiteSpace(this.EmbeddedUrlFormat) || !string.IsNullOrWhiteSpace(this.EmbeddedHashtagFormat))
                content = content.ReplaceFieldTokens(item).ReplaceDictionarySettingTokens();

            if (this.ConformUrlProtocolSecurityToRequest && request.HttpRequest.RawUrl.StartsWith("https://"))
                while (content.Contains("http://"))
                    content = content.Replace("http://", "https://");

            item[ContentFieldName] = content;

            #endregion Set content
            
            item[ChannelIDFieldName] = channelID;
            item[ChannelTitleFieldName] = channelTitle;
            try { item[PlaylistIDFieldName] = video["playlistId"].ToString(); } catch {}

            return item;
        }

        /// <summary>
        /// Finds videos for the specified playlist
        /// </summary>
        /// <param name="playlistID">The ID of the playlist for which to find and add video data</param>
        /// <param name="data">The list of item data to be returned by this data source</param>
        /// <param name="seenItems">The list of already-found videos, to allow restricting to unique results when videos appear in more than one playlist</param>
        /// <param name="avatarUrlMap">A map of channel IDs to avatar URLs</param>
        private void FindVideosByPlaylistID(string playlistID, List<SortedDictionary<string, object>> data, Dictionary<string, SortedDictionary<string, object>> seenItems, Dictionary<string, string> avatarUrlMap, RestRequest request)
        {
            string url = string.Format(PlaylistVideosUrlFormat, this.RelyingPartyKey, playlistID);
            string json = GetJson(url);

            JArray videos = JArray.Parse(JObject.Parse(json)["items"].ToString());
            for (int x = 0; x < videos.Count; x++)
            {
                try 
                {
                    string videoID = videos[x]["id"]["videoId"].ToString();
                    SortedDictionary<string, object> item = CreateItem(videos[x]["snippet"], videoID, avatarUrlMap, request);

                    if (!seenItems.ContainsKey(item.GetExternalID())) 
                    {
                        data.Add(item);
                        seenItems[item.GetExternalID()] = item;
                    }
                }
                catch {}
            }
        }

        /// <summary>
        /// Finds videos for the specified channel
        /// </summary>
        /// <param name="channelIDs">The ID of the channel for which to find and add video data</param>
        /// <param name="data">The list of item data to be returned by this data source</param>
        /// <param name="seenItems">The list of already-found videos, to allow restricting to unique results when videos appear in more than one playlist</param>
        /// <param name="avatarUrlMap">A map of channel IDs to avatar URLs</param>
        private void FindVideosByChannelID(string channelID, List<SortedDictionary<string, object>> data, Dictionary<string, SortedDictionary<string, object>> seenItems, Dictionary<string, string> avatarUrlMap, RestRequest request)
        {
            string url = string.Format(ChannelAndVideosUrlFormat, this.RelyingPartyKey, channelID);
            string json = GetJson(url);

            JArray jsonItems = JArray.Parse(JObject.Parse(json)["items"].ToString());

            #region 1. Do a first pass to find the channel avatar URL

            for (int x = 0; x < jsonItems.Count; x++)
            {
                try
                {
                    if (jsonItems[x]["id"]["kind"].ToString() == "youtube#channel")
                    {
                        avatarUrlMap[channelID] = jsonItems[x]["snippet"]["thumbnails"]["default"]["url"].ToString().Replace("http://", "https://");
                        break;
                    }
                } catch { }
            }

            #endregion 1. Do a first pass to find the channel avatar URL

            #region 2. Do a second pass to load video data

            for (int x = 0; x < jsonItems.Count; x++)
            {
                try
                {
                    if (jsonItems[x]["id"]["kind"].ToString() == "youtube#channel")
                        continue;

                    string videoID = jsonItems[x]["id"]["videoId"].ToString();
                    SortedDictionary<string, object> item = CreateItem(jsonItems[x]["snippet"], videoID, avatarUrlMap, request);

                    if (!seenItems.ContainsKey(item.GetExternalID()))
                    {
                        data.Add(item);
                        seenItems[item.GetExternalID()] = item;
                    }
                } 
                catch { }
            }

            #endregion 2. Do a second pass to load video data
        }

        /// <summary>
        /// Implements logic to get data from this data soruce
        /// </summary>
        /// <param name="request">The request for which to get the data</param>
        /// <param name="data">A list of data objects, to which to append</param>
        /// <param name="sources">The list of sources from which to get the data</param>
        protected override void GetData(RestRequest request, List<SortedDictionary<string, object>> data, List<string> sources)
        {
            if (request == null || data == null) throw new ArgumentNullException();
            else if (!IsValid) throw new InvalidOperationException(ValidationMessage);

            List<string> channelIDs = this.ChannelIDs;
            List<string> playlistIDs = this.PlaylistIDs;

            Dictionary<string, string> channelAvatarUrlMap = new Dictionary<string,string>();

            if (playlistIDs.Count == 0 && ChannelIDs.Count == 0)
                FindChannelIDs(channelIDs, channelAvatarUrlMap);

            Dictionary<string, SortedDictionary<string, object>> seenVideos = new Dictionary<string, SortedDictionary<string, object>>();

            if (channelIDs.Count > 0)
            {
                foreach (string channelID in channelIDs)
                    FindVideosByChannelID(channelID, data, seenVideos, channelAvatarUrlMap, request);
            }
            else if (playlistIDs.Count > 0)
            {
                foreach (string playlistID in playlistIDs)
                    FindVideosByPlaylistID(playlistID, data, seenVideos, channelAvatarUrlMap, request);
            }
        }

        #region Archived code using Google C# API

        /*        
        protected override void GetData(RestRequest request, List<SortedDictionary<string, object>> data)
        {
            string result = GetDataAsync(request, data).Result;
        }

        protected async Task<string> GetDataAsync(RestRequest request, List<SortedDictionary<string, object>> data)
        {
            if (request == null || data == null) throw new ArgumentNullException();

            UserCredential credential;

            ClientSecrets secrets = new ClientSecrets();

            secrets.ClientId = "AIzaSyCFdFX1IA60i6RD_WnNebOSDOBQ7L6KzSw";
            
//            using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(this.ExternalSystemAccessTokenSecret)))
            using (var stream = new MemoryStream(Encoding.UTF8.GetBytes("AIzaSyCFdFX1IA60i6RD_WnNebOSDOBQ7L6KzSw")))
            {
                credential = await GoogleWebAuthorizationBroker.AuthorizeAsync(
//                    GoogleClientSecrets.Load(stream).Secrets,
                    secrets,
                    // This OAuth 2.0 access scope allows for read-only access to the authenticated 
                    // user's account, but not other types of account access.
                    new[] { YouTubeService.Scope.YoutubeReadonly },
                    "O39QXQVSUmGzrL4Rl5NNdQ",
//                    this.ExternalSystemUserID,
                    CancellationToken.None
                );
            }

            YouTubeService youtubeService = new YouTubeService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = this.GetType().ToString()
            });

            var channelsListRequest = youtubeService.Channels.List("contentDetails");
            channelsListRequest.Mine = true;

            // Retrieve the contentDetails part of the channel resource for the authenticated user's channel.
            var channelsListResponse = await channelsListRequest.ExecuteAsync();

            foreach (Google.Apis.YouTube.v3.Data.Channel channel in channelsListResponse.Items)
            {
                // From the API response, extract the Playlist IDs that identifies the list
                // of videos uploaded to the authenticated user's channel.
                string uploadsListId = channel.ContentDetails.RelatedPlaylists.Uploads;

                string nextPageToken = "";
                while (nextPageToken != null)
                {
                    var playlistItemsListRequest = youtubeService.PlaylistItems.List("snippet");
                    playlistItemsListRequest.PlaylistIDs = uploadsListId;
                    playlistItemsListRequest.MaxResults = 50;
                    playlistItemsListRequest.PageToken = nextPageToken;

                    // Retrieve the list of videos uploaded to the authenticated user's channel.
                    var playlistItemsListResponse = await playlistItemsListRequest.ExecuteAsync();

                    foreach (PlaylistItem playlistItem in playlistItemsListResponse.Items)
                    {
                        SortedDictionary<string, object> item = new SortedDictionary<string,object>();
                        string url = "https://www.youtube.com/watch?v=" + playlistItem.Snippet.ResourceId.VideoId;

                        item.SetExternalID(playlistItem.Snippet.ResourceId.VideoId);
                        item.SetUrl(url);

                        item[AuthorNameFieldName] = playlistItem.Snippet.ChannelTitle;
                        item[AuthorAvatarUrlFieldName] = "";
                        item[ProfileUrlFieldName] = "http://www.youtube.com/user/" + ExternalSystemUserID.Replace("@", "").Replace(" ", "");
                        item[LinkedUrlFieldName] = url;
                        item[ContentFieldName] = HttpUtility.HtmlEncode(playlistItem.Snippet.Title) + " <br/><br/>" + HttpUtility.HtmlEncode(playlistItem.Snippet.Description);

                        data.Add(item);
                    }

                    nextPageToken = playlistItemsListResponse.NextPageToken;
                }
            }

            return "";
        }
         */

        #endregion Archived code using Google C# API

        #endregion Custom code


    }
}
