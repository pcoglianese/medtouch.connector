// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITwitterDataSource.cs" company="MedTouch">
//   Copyright (c) MedTouch 2015. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//        Developer:    sitecore\admin
// Template Version:    3.0
//             Date:    07/23/2015
// --------------------------------------------------------------------------------------------------------------------
namespace MedTouch.Connector.RestEndpoints.DataSources
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Runtime.InteropServices;
    using System.Security.Cryptography;
    using System.Text;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    using Sitecore.Data;
    using Sitecore.Data.Items;

    using MedTouch.Common;
    using MedTouch.Common.Versioning;
    using System.Text.RegularExpressions;


    /// <summary>
    /// The 'TwitterDataSource' Interface
    /// </summary>
    public interface ITwitterDataSource : IExternalDataSource
    {
        bool ConformUrlProtocolSecurityToRequest { get; }
        string EmbeddedUrlFormat { get; }
        string EmbeddedHashtagFormat { get; }
    }

    /// <summary>
    /// The 'TwitterDataSource' Data Template item
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    [Serializable, GuidAttribute("b21367f7-26dd-43d9-9905-80a6f3ba6ae8")]
    [MTemplateVersion("b21367f7-26dd-43d9-9905-80a6f3ba6ae8", "Twitter Data Source")]
    public partial class TwitterDataSource : MedTouch.Connector.RestEndpoints.DataSources.ExternalDataSource, ITwitterDataSource
    {
        /// <summary>
        /// Initializes a new instance of the TwitterDataSource class.
        /// </summary>
        /// <param name="item">The Sitecore item.</param>
        internal TwitterDataSource(Item item)
            : base(item)
        {
            this.ConformUrlProtocolSecurityToRequest = item[FieldNames.ConformUrlProtocolSecurityToRequest] == "1";
            this.EmbeddedUrlFormat = item[FieldNames.EmbeddedUrlFormat] + "";
            this.EmbeddedHashtagFormat = item[FieldNames.EmbeddedHashtagFormat] + "";
        }

        #region Template Properties

        /// <summary>
        /// Gets whether ConformUrlProtocolSecurityToRequest
        /// </summary>
        /// <value>A bool indicating whether ConformUrlProtocolSecurityToRequest</value>
        [JsonProperty]
        [MTemplateFieldVersion("5c7fdbf4-5441-4add-9cb0-6b80ad87c4a6", "Conform URL Protocol Security To Request", "Checkbox")]
        public bool ConformUrlProtocolSecurityToRequest { get; protected set; }

        /// <summary>
        /// Gets a value
        /// </summary>
        /// <value>A string value</value>
        [JsonProperty]
        [MTemplateFieldVersion("f1fa851e-ea44-40fb-9a9a-cfe6370eebec", "Embedded URL Format", "Single-Line Text")]
        public string EmbeddedUrlFormat { get; protected set; }

        /// <summary>
        /// Gets a value
        /// </summary>
        /// <value>A string value</value>
        [JsonProperty]
        [MTemplateFieldVersion("3e43ed8d-ce9e-4dcd-98df-e691265d474f", "Embedded Hashtag Format", "Single-Line Text")]
        public string EmbeddedHashtagFormat { get; protected set; }

        #endregion

        #region ITwitterDataSource Properties

        /// <summary>
        /// Gets the ConformUrlProtocolSecurityToRequest
        /// </summary>
        bool ITwitterDataSource.ConformUrlProtocolSecurityToRequest
        {
            get
            {
                return this.ConformUrlProtocolSecurityToRequest;
            }
        }

        /// <summary>
        /// Gets the EmbeddedUrlFormat
        /// </summary>
        string ITwitterDataSource.EmbeddedUrlFormat
        {
            get
            {
                return this.EmbeddedUrlFormat;
            }
        }

        /// <summary>
        /// Gets the EmbeddedHashtagFormat
        /// </summary>
        string ITwitterDataSource.EmbeddedHashtagFormat
        {
            get
            {
                return this.EmbeddedHashtagFormat;
            }
        }

        #endregion

        /// <summary>
        /// Performs an implicit conversion from <see cref="Sitecore.Data.Items.Item"/> to <see cref="TwitterDataSource"/>.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>
        /// The result of the conversion.
        /// </returns>
        public static implicit operator TwitterDataSource(Item item)
        {
            return item.InheritsTemplate(typeof(TwitterDataSource)) ? new TwitterDataSource(item) : null;
        }

        #region Custom code

        /// <summary>
        /// Indicates whether this data source is fully configured
        /// </summary>
        public override bool IsValid
        {
            get
            {
                return base.IsValid;
            }
        }

        /// <summary>
        /// Gets an appropriate message when this data source is not fully configured
        /// </summary>
        public override string ValidationMessage
        {
            get
            {
                return base.ValidationMessage;
            }
        }

        /// <summary>
        /// The OAuth version used by this data source
        /// </summary>
        private const string OAuthVersion = "1.0";

        /// <summary>
        /// The OAuth signature method for this data source
        /// </summary>
        private const string OAuthSignatureMethod = "HMAC-SHA1";

        /// <summary>
        /// The beginning of date-time ranges to the present
        /// </summary>
        private static DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);

        /// <summary>
        /// The Twitter output date-time format
        /// </summary>
        private const string ResourceDateTimeFormat = "ddd MMM dd HH:mm:ss +ffff yyyy";

        /// <summary>
        /// An ISO-8601-compliant date-time format
        /// </summary>
        private const string Iso8601DateTimeFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fffzzz";

        /// <summary>
        /// A format for OAuth signature parameters
        /// </summary>
        private const string OAuthParameterFormat = "oauth_consumer_key={0}&oauth_nonce={1}&oauth_signature_method={2}" +
                        "&oauth_timestamp={3}&oauth_token={4}&oauth_version={5}&{6}";

        //        private const string OAuthParameterFormat = "oauth_consumer_key={0}&oauth_nonce={1}&oauth_signature_method={2}" +
        //                "&oauth_timestamp={3}&oauth_token={4}&oauth_version={5}&q={6}";

        /// <summary>
        /// The base URL to the external resource
        /// </summary>
        private const string ResourceUrl = "https://api.twitter.com/1.1/statuses/user_timeline.json";

        /// <summary>
        /// The maximum result count for the external service
        /// </summary>
        private const int ResourceMaximumResultCount = 200;

        /// <summary>
        /// A result data field name
        /// </summary>
        private const string AuthorNameFieldName = "Author Name";
        /// <summary>
        /// A result data field name
        /// </summary>
        private const string AuthorAvatarUrlFieldName = "Author Avatar URL";
        /// <summary>
        /// A result data field name
        /// </summary>
        private const string AuthorProfileUrlFieldName = "Author Profile URL";
        /// <summary>
        /// A result data field name
        /// </summary>
        private const string ContentFieldName = "Content";

        /// <summary>
        /// A URL format
        /// </summary>
        private const string UserProfileUrlFormat = "https://twitter.com/{0}";

        /// <summary>
        /// A URL format
        /// </summary>
        private const string TwitterTweetUrlFormat = "https://twitter.com/statuses/{0}";

        /// <summary>
        /// A regular expression pattern for matching on hashtags
        /// </summary>
        private const string HashtagPattern = @"(^|\s)\#([a-zA-Z0-9]+)((\.\.\.)|\x85|\x2026|)";

        /// <summary>
        /// An ellipsis character (Twitter may append ellipses when truncating retweets)
        /// </summary>
        private static string Ellipsis1 = (char)133 + "";

        /// <summary>
        /// An ellipsis character (Twitter may append ellipses when truncating retweets)
        /// </summary>
        private static char Ellipsis1Char = (char)133;

        /// <summary>
        /// An ellipsis character (Twitter may append ellipses when truncating retweets)
        /// </summary>
        private static string Ellipsis2 = (char)8230 + "";

        /// <summary>
        /// An ellipsis character (Twitter may append ellipses when truncating retweets)
        /// </summary>
        private static char Ellipsis2Char = (char)8230;

        /// <summary>
        /// A regular expression pattern for matching t.co condensed URLs
        /// </summary>
        private const string CondensedUrlPattern = @"http[s]?\:\/\/t\.co\\/[a-zA-Z0-9]+((\.\.\.)|\x85|\x2026|)";

        /// <summary>
        /// Indicates whether the specified string is a truncated retweet
        /// </summary>
        /// <param name="s">The string to check</param>
        /// <returns>true if a truncated retweet, otherwise false</returns>
        private static bool IsTruncatedRetweet(string s)
        {
            s = (s + "").Trim().ToLower();
            return s.StartsWith("rt @") && s.EndsWith("...");
        }

        /// <summary>
        /// Implements logic to get data from this data soruce
        /// </summary>
        /// <param name="request">The request for which to get the data</param>
        /// <param name="data">A list of data objects, to which to append</param>
        /// <param name="sources">The list of sources from which to get the data</param>
        protected override void GetData(RestRequest request, List<SortedDictionary<string, object>> data, List<string> sources)
        {
            if (request == null || data == null) throw new ArgumentNullException();
            else if (!IsValid) throw new InvalidOperationException(ValidationMessage);

            #region Request data from Twitter

            int statusCount = Math.Min(GetNonPagedCount(request.PageSize, request.PageNumber), ResourceMaximumResultCount);
            string urlParameters = "screen_name=" + Uri.EscapeUriString(this.ExternalSystemUserID); // +"&count=" + statusCount; // +"&exclude_replies=true&include_rts=false";

            string oAuthToken = this.ExternalSystemAccessToken;
            string oAuthTokenSecret = this.ExternalSystemAccessTokenSecret;

            string oAuthConsumerKey = this.RelyingPartyKey;
            string oAuthConsumerSecret = this.RelyingPartySecret;

            // A nonce is a time-unique string required by the OAuth spec to be used to differentiate requests
            string oAuthNonce = Convert.ToBase64String(new ASCIIEncoding().GetBytes(DateTime.Now.Ticks.ToString()));
            TimeSpan timeSpan = DateTime.UtcNow - Epoch;
            string oAuthTimestamp = Convert.ToInt64(timeSpan.TotalSeconds).ToString();

            string baseString = string.Format(OAuthParameterFormat,
                                        oAuthConsumerKey,
                                        oAuthNonce,
                                        OAuthSignatureMethod,
                                        oAuthTimestamp,
                                        oAuthToken,
                                        OAuthVersion,
                                        urlParameters
                                        );

            baseString = "GET&" + Uri.EscapeDataString(ResourceUrl) + "&" + Uri.EscapeDataString(baseString);

            string compositeKey = string.Concat(Uri.EscapeDataString(oAuthConsumerSecret),
                                    "&", Uri.EscapeDataString(oAuthTokenSecret));

            string oAuthSignature;
            using (HMACSHA1 hasher = new HMACSHA1(ASCIIEncoding.ASCII.GetBytes(compositeKey)))
            {
                oAuthSignature = Convert.ToBase64String(
                    hasher.ComputeHash(ASCIIEncoding.ASCII.GetBytes(baseString)));
            }

            // create the request header
            string headerFormat = "OAuth oauth_nonce=\"{0}\", oauth_signature_method=\"{1}\", " +
                               "oauth_timestamp=\"{2}\", oauth_consumer_key=\"{3}\", " +
                               "oauth_token=\"{4}\", oauth_signature=\"{5}\", " +
                               "oauth_version=\"{6}\"";

            string authHeader = string.Format(headerFormat,
                                    Uri.EscapeDataString(oAuthNonce),
                                    Uri.EscapeDataString(OAuthSignatureMethod),
                                    Uri.EscapeDataString(oAuthTimestamp),
                                    Uri.EscapeDataString(oAuthConsumerKey),
                                    Uri.EscapeDataString(oAuthToken),
                                    Uri.EscapeDataString(oAuthSignature),
                                    Uri.EscapeDataString(OAuthVersion)
                            );

            ServicePointManager.Expect100Continue = false;

            // For documentation, see https://dev.twitter.com/rest/reference/get/statuses/user_timeline
            // Example: https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=twitterapi&count=2
            string remoteRequestUrl = ResourceUrl + "?" + urlParameters;

            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(remoteRequestUrl);
            req.Headers.Add("Authorization", authHeader);
            req.Method = "GET";
            req.ContentType = "application/x-www-form-urlencoded";
            WebResponse resp = (HttpWebResponse)req.GetResponse();
            StreamReader reader = new StreamReader(resp.GetResponseStream());

            string json = reader.ReadToEnd();

            #endregion Request data from Twitter

            #region Process tweets

            JArray tweetData = JArray.Parse(json);
            DateTime tweetTimestamp = DateTime.MinValue;

            string sourceUrlProtocol = "http://", targetUrlProtocol = "https://";

            //if (this.ConformUrlProtocolSecurityToRequest)
            //{
            //    if (request.HttpRequest.RawUrl.StartsWith("http://"))
            //    {
            //        sourceUrlProtocol = "https://";
            //        targetUrlProtocol = "http://";
            //    }
            //    else if (request.HttpRequest.RawUrl.StartsWith("https://"))
            //    {
            //        sourceUrlProtocol = "http://";
            //        targetUrlProtocol = "https ://";
            //    }
            //}

            for (int x = 0; x < tweetData.Count; x++)
            {
                JObject tweet = JObject.Parse(tweetData[x].ToString());
                SortedDictionary<string, object> item = new SortedDictionary<string, object>();

                try
                {

                    #region 1. Set ID/timestamp fields

                    string tweetID = tweet["id_str"].ToString();
                    item.SetExternalID(tweetID);
                    item.SetUrl(string.Format(TwitterTweetUrlFormat, tweetID));

                    try
                    {
                        if (DateTime.TryParseExact(tweet["created_at"].ToString(), "ddd MMM dd HH:mm:ss +ffff yyyy", null, System.Globalization.DateTimeStyles.None, out tweetTimestamp))
                            item.SetTimestamp(tweetTimestamp);
                    }
                    catch { }

                    #endregion 1. Set ID/timestamp fields

                    #region 2. Set author fields

                    JObject user = JObject.Parse(tweet["user"].ToString());

                    item[AuthorNameFieldName] = user["name"];
                    item[AuthorAvatarUrlFieldName] = user["profile_image_url"].ToString().Replace("http://", "//");
                    item[AuthorProfileUrlFieldName] = string.Format(UserProfileUrlFormat, user["screen_name"]);

                    #endregion 2. Set author fields

                    #region 3. Set content

                    string content = tweet["text"].ToString().Trim();
                    if (string.IsNullOrWhiteSpace(content)) continue;
                    bool isTruncatedRetweet = IsTruncatedRetweet(content);

                    #region Perform URL replacements

                    try
                    {
                        foreach (JToken url in JArray.Parse(tweet["entities"]["urls"].ToString()))
                        {
                            string condensedUrl = url["url"].ToString();
                            if (!string.IsNullOrWhiteSpace(condensedUrl))
                            {
                                string expandedUrl = condensedUrl;
                                try { expandedUrl = url["expanded_url"].ToString(); }
                                catch { }
                                if (!string.IsNullOrWhiteSpace(expandedUrl))
                                {
                                    if (!string.IsNullOrWhiteSpace(this.EmbeddedUrlFormat))
                                    {
                                        expandedUrl =
                                            this.EmbeddedUrlFormat
                                                .Replace("{url}", expandedUrl)
                                                .ReplaceFieldTokens(item)
                                                .ReplaceDictionarySettingTokens();
                                    }

                                    content = content.Replace(condensedUrl, expandedUrl);
                                }
                            }
                        }

                        if (!string.IsNullOrWhiteSpace(this.EmbeddedUrlFormat))
                        {
                            foreach (Match match in Regex.Matches(content, CondensedUrlPattern))
                            {
                                if (!string.IsNullOrWhiteSpace(match.Value))
                                {
                                    string originalUrl = match.Value.Trim();
                                    if (!isTruncatedRetweet || (!originalUrl.EndsWith("...") && !originalUrl.EndsWith(Ellipsis1) && !originalUrl.EndsWith(Ellipsis2)))
                                    {
                                        if (originalUrl.EndsWith("..."))
                                            originalUrl = originalUrl.Substring(0, originalUrl.Length - 3);
                                        originalUrl = originalUrl.Trim(Ellipsis1Char, Ellipsis2Char);

                                        string replacement = this.EmbeddedUrlFormat
                                                        .Replace("{url}", originalUrl)
                                                        .ReplaceFieldTokens(item)
                                                        .ReplaceDictionarySettingTokens();

                                        content = content.Replace(originalUrl, replacement);
                                    }
                                }
                            }
                        }

                    }
                    catch { }

                    #endregion Perform URL replacements

                    #region Perform hashtag replacements

                    if (!string.IsNullOrWhiteSpace(this.EmbeddedHashtagFormat))
                    {
                        foreach (Match match in Regex.Matches(content, HashtagPattern))
                        {
                            string originalHashtag = match.Value;
                            if (!string.IsNullOrWhiteSpace(originalHashtag))
                            {
                                originalHashtag = originalHashtag.Trim();

                                if (!isTruncatedRetweet || (!originalHashtag.EndsWith("...") && !originalHashtag.EndsWith(Ellipsis1) && !originalHashtag.EndsWith(Ellipsis2)))
                                {
                                    originalHashtag = originalHashtag.Trim('.', Ellipsis1Char, Ellipsis2Char);

                                    string replacement =
                                        this.EmbeddedHashtagFormat
                                        .Replace("{hashtag}", match.Value)
                                        .Replace("{hashword}", match.Value.Replace("#", ""))
                                        .ReplaceDictionarySettingTokens();

                                    content = content.Replace(originalHashtag, replacement);
                                }
                            }
                        }
                    }

                    #endregion Perform hashtag replacements

                    //if (this.ConformUrlProtocolSecurityToRequest)
                    //    while (content.Contains(sourceUrlProtocol))
                    //        content = content.Replace(sourceUrlProtocol, targetUrlProtocol);

                    item[ContentFieldName] = content;

                    #endregion 3. Set content

                    data.Add(item);

                }
                catch (Exception e)
                {
                    request.Context.Log(e);
                }

            }

            #endregion Process tweets
        }


        /// <summary>
        /// The Strongly typed field names for <see cref="TwitterDataSource"/>.
        /// </summary>
        public static new class FieldNames
        {
            /// <summary>
            /// The Field Name
            /// </summary>
            public static readonly ID ConformUrlProtocolSecurityToRequest = new ID("5c7fdbf4-5441-4add-9cb0-6b80ad87c4a6");

            /// <summary>
            /// The Field Name
            /// </summary>
            public static readonly ID EmbeddedUrlFormat = new ID("f1fa851e-ea44-40fb-9a9a-cfe6370eebec");

            /// <summary>
            /// The Field Name
            /// </summary>
            public static readonly ID EmbeddedHashtagFormat = new ID("3e43ed8d-ce9e-4dcd-98df-e691265d474f");

        }
        #endregion
    }
}
