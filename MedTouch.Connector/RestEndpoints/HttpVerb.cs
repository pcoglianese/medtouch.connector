﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedTouch.Connector.RestEndpoints
{
    /// <summary>
    /// Enumerates HTTP verbs
    /// </summary>
    public enum HttpVerb
    {
        /// <summary>
        /// The HTTP DELETE verb
        /// </summary>
        Delete,
        /// <summary>
        /// The HTTP GET verb
        /// </summary>
        Get,
        /// <summary>
        /// The HTTP POST verb
        /// </summary>
        Post,
        /// <summary>
        /// The HTTP PUT verb
        /// </summary>
        Put
    }
}
