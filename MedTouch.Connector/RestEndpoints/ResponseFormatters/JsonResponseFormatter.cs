﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// using System.Web.Script.Serialization;

using Newtonsoft.Json;

using MedTouch.Common;


namespace MedTouch.Connector.RestEndpoints.ResponseFormatters
{
    public class JsonResponseFormatter : RestResponseFormatter
    {
        /// <summary>
        /// An ISO 8601 timestamp format
        /// </summary>
        private const string Iso8601TimestampFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fffzzz";

        /// <summary>
        /// A JSON result-list ending (apparently truncated occasionally by the built-in serializer)
        /// </summary>
        private const string ResultsEnding = "]}";

        /* Formatting time seems so minimal, it's not worth this replacement... commenting out for now
        /// <summary>
        /// A token used in post-formatting replacement of timing values
        /// </summary>
        private const string DurationToken = "{{{DURATION}}}";

        /// <summary>
        /// A token used in post-formatting replacement of timing values
        /// </summary>
        private const string DurationTokenWithQuotes = "\"{{{DURATION}}}\"";
        */

        /// <summary>
        /// Prepares a serializable object for serialization in a particular format
        /// </summary>
        /// <param name="response">The response for which to prepare the intermediary representation</param>
        /// <returns>A data envelope, ready for serialization to JSON</returns>
        private static object BuildJsonEnvelope(RestResponse response)
        {
            SortedDictionary<string, object> envelope = new SortedDictionary<string, object>();
            SortedDictionary<string, object> metaInfo = new SortedDictionary<string, object>();
            RestRequest request = response.Context.Request;

            metaInfo.Add("endpoint", response.Context.Item.ID.ToString().Trim('{', '}').ToLower());
            metaInfo.Add("method", response.Context.Request.HttpVerb.ToString().ToUpper());
            metaInfo.Add("code", response.HttpStatusCode);
            metaInfo.Add("description", response.HttpStatusDescription);

            if (response.IsException)
                metaInfo.Add("exception", response.LastException.GetExtendedMessage());

            metaInfo.Add("timestamp", DateTime.Now.ToString(Iso8601TimestampFormat));
            metaInfo.Add("duration", response.Context.DurationInMilliseconds);

            // The total number of items available from any data sources used in a GET request
            metaInfo.Add("items", response.Data.Count);

            // The total number of pages available, for a data-retrieving GET request, otherwise 0
            metaInfo.Add("pages", response.Data.PageCount(request.PageSize));

            if (response.Context.IsLoggingEnabled)
            {
                IList<string> log = response.Context.GetLog();
                if (log.Count > 0)
                    metaInfo.Add("log", log);
            }

            envelope.Add("meta", metaInfo);
            envelope.Add("response", response.Data.GetPage(request.PageSize, request.PageNumber));

            return envelope;
        }


        /// <summary
        /// Formats the response
        /// </summary>
        /// <param name="response">The response to format</param>
        /// <param name="writer">The writer to which to write</param>
        public override void Format(RestResponse response, System.IO.TextWriter writer)
        {
            if (response == null || writer == null) return;

            response.Context.Log("Beginning formatting...");

            response.Data.RemoveSystemFields();

            object jsonEnvelope = BuildJsonEnvelope(response);

//            JavaScriptSerializer serializer = new JavaScriptSerializer();
//            serializer.MaxJsonLength = Int32.MaxValue;
//            string json = JsonConvert.SerializeObject(jsonEnvelope);
//            if (!json.EndsWith(ResultsEnding))
//                json += ResultsEnding;

            Formatting formatting = (Sitecore.Context.PageMode.IsPreview || response.Context.IsPrettyPrintingEnabled) ? Formatting.Indented : Formatting.None;
            string json = JsonConvert.SerializeObject(jsonEnvelope, formatting);
            //            json = json.Replace(DurationTokenWithQuotes, response.Context.DurationInMilliseconds.ToString()) + " ";

            //            if (Sitecore.Context.PageMode.IsPreview || response.Context.IsPrettyPrintingEnabled)
            //                json = RestUtility.PrettyFormatJson(json);
            //            response.HttpResponse.AppendHeader("Content-Length", json.Length.ToString);
            //            response.HttpResponse.Headers["Content-Length"] = json.Length.ToString();

            string jsonp = response.Context.Request.Parameters["callback"];
            if (response.Context.Request.Parameters["callback"] != null)
            {
                response.Write(response.Context.Request.Parameters["callback"] + "(" + json + ")");
            }
            else
            {
                response.Write(json);
            }
        }
    }
}
