﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedTouch.Connector.RestEndpoints.ResponseFormatters
{
    /// <summary>
    /// A base class and factory for response formatters
    /// </summary>
    public abstract class RestResponseFormatter : IRestResponseFormatter
    {
        /// <summary>
        /// Formats the response
        /// </summary>
        /// <param name="response">The response to format</param>
        /// <returns>A string version of the response</returns>
        public string Format(RestResponse response)
        {
            if (response == null) throw new ArgumentNullException();

            StringBuilder sb = new StringBuilder(256);
            Format(response, new StringWriter(sb));
            return sb.ToString();
        }

        /// <summary>
        /// Formats the response
        /// </summary>
        /// <param name="response">The response to format</param>
        /// <param name="writer">The writer to which to write</param>
        public abstract void Format(RestResponse response, System.IO.TextWriter writer);

        /// <summary>
        /// The default response formatter
        /// </summary>
        private static IRestResponseFormatter defaultFormatter = new JsonResponseFormatter();

        /// <summary>
        /// Gets the response formatter for the specified endpoint
        /// </summary>
        /// <param name="context">The context for which to get the response formatter</param>
        /// <returns>A response formatter</returns>
        public static IRestResponseFormatter GetResponseFormatter(RestContext context)
        {
            //if (context == null) throw new ArgumentNullException();

            //string formatterTypeName = context.Endpoint.Res .Configuration.ResponseFormatterType;
            //if (formatterTypeName == typeof(JsonResponseFormatter).FullName || formatterTypeName.ToLower() == "json")
            //    return new JsonResponseFormatter();

            return defaultFormatter;
        }
    }
}
