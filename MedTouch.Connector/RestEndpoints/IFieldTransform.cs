// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IFieldTransform.cs" company="MedTouch">
//   Copyright (c) MedTouch 2015. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//        Developer:    sitecore\admin
// Template Version:    3.0
//             Date:    07/23/2015
// --------------------------------------------------------------------------------------------------------------------
namespace MedTouch.Connector.RestEndpoints
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    using Newtonsoft.Json;

    using Sitecore.Data;
    using Sitecore.Data.Items;

    using MedTouch.Common;
    using MedTouch.Common.Versioning;
    using MedTouch.Connector.RestEndpoints.Filters;
    

    #region Custom code

    /// <summary>
    /// Extension / utility methods for field transforms
    /// </summary>
    public static class FieldTransformExtensions
    {

        /// <summary>
        /// For each transform in this list, transforms the specified data item's values in place, as applicable
        /// </summary>
        /// <param name="item">The data to transform</param>
        public static void Transform(this IEnumerable<IFieldTransform> transforms, SortedDictionary<string, object> item)
        {
            if (transforms == null) return;

            foreach (IFieldTransform transform in transforms)
                transform.Transform(item);
        }

        /// <summary>
        /// For each transform in this list, transforms the specified data items' values in place, as applicable
        /// </summary>
        /// <param name="items">The data to transform</param>
        public static void Transform(this IEnumerable<IFieldTransform> transforms, List<SortedDictionary<string, object>> items)
        {
            if (transforms == null) return;

            foreach (IFieldTransform transform in transforms)
                transform.Transform(items);
        }

    }


    #endregion Custom code


    /// <summary>
    /// The 'FieldTransform' Interface
    /// </summary>
    public interface IFieldTransform : IMItem
    {
        /// <summary>
        /// Gets the SourceFieldDataType
        /// </summary>
        string SourceFieldDataType { get; }

        /// <summary>
        /// Gets the SourceFieldName
        /// </summary>
        List<string> SourceFieldName { get; }

        /// <summary>
        /// Gets the TargetFieldDataType
        /// </summary>
        string TargetFieldDataType { get; }

        /// <summary>
        /// Gets the TargetFieldName
        /// </summary>
        string TargetFieldName { get; }

        /// <summary>
        /// Gets the TargetFormat
        /// </summary>
        string TargetFormat { get; }

        /// <summary>
        /// Transforms the specified data item's values, as applicable
        /// </summary>
        /// <param name="item">The data to transform</param>
        void Transform(SortedDictionary<string, object> item);

        /// <summary>
        /// Transforms the specified data items' values, as applicable
        /// </summary>
        /// <param name="items">The data to transform</param>
        void Transform(List<SortedDictionary<string, object>> items);

        RenderingHint ImageFieldRenderingHint { get; }
        RenderingHint LinkFieldRenderingHint { get; }
        RenderingHint TextFieldRenderingHint { get; }
    }

    /// <summary>
    /// The 'FieldTransform' Data Template item
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    [Serializable, GuidAttribute("4cf19911-b218-44bf-9dcf-a91c8ca6ee18")]
    [MTemplateVersion("4cf19911-b218-44bf-9dcf-a91c8ca6ee18", "Field Transform")]
    public partial class FieldTransform : MItem, IFieldTransform
    {
        /// <summary>
        /// Initializes a new instance of the FieldTransform class.
        /// </summary>
        /// <param name="item">The Sitecore item.</param>
        internal FieldTransform(Item item)
            : base(item)
        {
            this.SourceFieldDataType = item.Fields[FieldNames.SourceFieldDataType].FieldRenderer();
            this.SourceFieldName = (item.Fields[FieldNames.SourceFieldName].Value + "").GetMultipleFieldValues();
            this.TargetFieldDataType = item.Fields[FieldNames.TargetFieldDataType].FieldRenderer();
            this.TargetFieldName = item.Fields[FieldNames.TargetFieldName].Value + "";
            this.TargetFormat = item.Fields[FieldNames.TargetFormat].Value + "";

            this.ImageFieldRenderingHint = RestUtility.DecodeRenderingHint(item["Image Field Rendering Hint"]);
            this.LinkFieldRenderingHint = RestUtility.DecodeRenderingHint(item["Link Field Rendering Hint"]);
            this.TextFieldRenderingHint = RestUtility.DecodeRenderingHint(item["Text Field Rendering Hint"]);

            transformsFieldNames = SourceFieldName.Count > 0 && !string.IsNullOrWhiteSpace(TargetFieldName);
        }

        #region Template Properties

        /// <summary>
        /// Gets the SourceFieldDataType
        /// </summary>
        /// <value>The SourceFieldDataType.</value>
        [JsonProperty]
        [MTemplateFieldVersion("44fced49-dd6a-4122-9363-a7b1fe4974a9", "Source Field Data Type", "Single-Line Text")]
        public string SourceFieldDataType { get; private set; }

        /// <summary>
        /// Gets the SourceFieldName
        /// </summary>
        /// <value>The SourceFieldName.</value>
        [JsonProperty]
        [MTemplateFieldVersion("0d079b6e-9aad-44b0-af17-ba660ae73bdb", "Source Field Name", "Multi-Line Text")]
        public List<string> SourceFieldName { get; private set; }

        /// <summary>
        /// Gets the TargetFieldDataType
        /// </summary>
        /// <value>The TargetFieldDataType.</value>
        [JsonProperty]
        [MTemplateFieldVersion("d98a6d45-6518-4ea3-bec4-a5cf6a0d0b2f", "Target Field Data Type", "Single-Line Text")]
        public string TargetFieldDataType { get; private set; }

        /// <summary>
        /// Gets the TargetFieldName
        /// </summary>
        /// <value>The TargetFieldName.</value>
        [JsonProperty]
        [MTemplateFieldVersion("196bd616-a13a-480b-a56c-99bf792df2c3", "Target Field Name", "Single-Line Text")]
        public string TargetFieldName { get; private set; }

        /// <summary>
        /// Gets the TargetFormat
        /// </summary>
        /// <value>The TargetFormat.</value>
        [JsonProperty]
        [MTemplateFieldVersion("65ad43f2-cd94-43b2-8a45-47e1600bcb39", "Target Format", "Single-Line Text")]
        public string TargetFormat { get; private set; }

        public RenderingHint ImageFieldRenderingHint { get; set; }
        public RenderingHint LinkFieldRenderingHint { get; set; }
        public RenderingHint TextFieldRenderingHint { get; set; }

        #endregion

        #region IFieldTransform Properties

        /// <summary>
        /// Gets the SourceFieldDataType
        /// </summary>
        string IFieldTransform.SourceFieldDataType
        {
            get
            {
                return this.SourceFieldDataType;
            }
        }

        /// <summary>
        /// Gets the SourceFieldName
        /// </summary>
        List<string> IFieldTransform.SourceFieldName
        {
            get
            {
                return this.SourceFieldName;
            }
        }

        /// <summary>
        /// Gets the TargetFieldDataType
        /// </summary>
        string IFieldTransform.TargetFieldDataType
        {
            get
            {
                return this.TargetFieldDataType;
            }
        }

        /// <summary>
        /// Gets the TargetFieldName
        /// </summary>
        string IFieldTransform.TargetFieldName
        {
            get
            {
                return this.TargetFieldName;
            }
        }

        /// <summary>
        /// Gets the TargetFormat
        /// </summary>
        string IFieldTransform.TargetFormat
        {
            get
            {
                return this.TargetFormat;
            }
        }

        RenderingHint IFieldTransform.ImageFieldRenderingHint { get { return this.ImageFieldRenderingHint; } }
        RenderingHint IFieldTransform.LinkFieldRenderingHint { get { return this.LinkFieldRenderingHint; } }
        RenderingHint IFieldTransform.TextFieldRenderingHint { get { return this.TextFieldRenderingHint; } }

        #endregion

        /// <summary>
        /// Performs an implicit conversion from <see cref="Sitecore.Data.Items.Item"/> to <see cref="FieldTransform"/>.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>
        /// The result of the conversion.
        /// </returns>
        public static implicit operator FieldTransform(Item item)
        {
            return item.InheritsTemplate(typeof(FieldTransform)) ? new FieldTransform(item) : null;
        }

        /// <summary>
        /// The Strongly typed field names for <see cref="FieldTransform"/>.
        /// </summary>
        public static new class FieldNames
        {
            /// <summary>
            /// The Field Name
            /// </summary>
            public static readonly ID SourceFieldDataType = new ID("44fced49-dd6a-4122-9363-a7b1fe4974a9");

            /// <summary>
            /// The Field Name
            /// </summary>
            public static readonly ID SourceFieldName = new ID("0d079b6e-9aad-44b0-af17-ba660ae73bdb");

            /// <summary>
            /// The Field Name
            /// </summary>
            public static readonly ID TargetFieldDataType = new ID("d98a6d45-6518-4ea3-bec4-a5cf6a0d0b2f");

            /// <summary>
            /// The Field Name
            /// </summary>
            public static readonly ID TargetFieldName = new ID("196bd616-a13a-480b-a56c-99bf792df2c3");

            /// <summary>
            /// The Field Name
            /// </summary>
            public static readonly ID TargetFormat = new ID("65ad43f2-cd94-43b2-8a45-47e1600bcb39");

        }


        #region Custom code

        /// <summary>
        /// Indicates whether this field transform affects field names
        /// </summary>
        private bool transformsFieldNames;

        /// <summary>
        /// Transforms the specified data item's values in place, as applicable
        /// </summary>
        /// <param name="item">The data to transform</param>
        public void Transform(SortedDictionary<string, object> item)
        {
            if (item == null) return;

            // TODO: implement value transformation

            object value;

            foreach (string sourceFieldName in SourceFieldName)
            {
                if (item.TryGetValue(sourceFieldName, out value))
                {
                    if (value is string)
                    {
                        switch (TextFieldRenderingHint)
                        {
                            case RenderingHint.Plain:
                                item[sourceFieldName] = ((string)value).StripTags();
                                break;
                            default:
                                break;
                        }
                    }
                    
                    item[sourceFieldName] = value;
                }
            }

            if (transformsFieldNames)
            {
                foreach (string sourceFieldName in SourceFieldName)
                {
                    if (item.TryGetValue(sourceFieldName, out value))
                    {
                        item[TargetFieldName] = value;
                        item.Remove(sourceFieldName);
                        return;
                    }
                }
            }
        }

        /// <summary>
        /// Transforms the specified data items' values in place, as applicable
        /// </summary>
        /// <param name="items">The data to transform</param>
        public void Transform(List<SortedDictionary<string, object>> items)
        {
            if (items == null) return;

            foreach (SortedDictionary<string, object> item in items)
                Transform(item);
        }

        #endregion Custom code

    }
}