﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedTouch.Connector.RestEndpoints.RequestHandlers
{
    /// <summary>
    /// The abstract base class for IRestRequestHandler implementations, and a factory
    /// </summary>
    public abstract class RestRequestHandler : IRestRequestHandler
    {
        /// <summary>
        /// Executes the specified request, returning a response
        /// </summary>
        /// <param name="request">The request to execute</param>
        public void Execute(RestRequest request)
        {
            if (request == null) throw new ArgumentNullException();

            request.Context.Log("Executing request...");

            try
            {
                ExecuteImpl(request);
            }
            catch (Exception e)
            {
                request.Context.Log(e);
            }

            request.Context.Log("Finished executing request");
        }

        /// <summary>
        /// Executes the specified request
        /// </summary>
        /// <param name="request">The request to execute</param>
        protected abstract void ExecuteImpl(RestRequest request);

        /// <summary>
        /// Gets an appropriate handler for the specified request
        /// </summary>
        /// <param name="context">The request for which to get a handler</param>
        /// <returns>An appropriate handler</returns>
        public static IRestRequestHandler GetRequestHandler(RestContext context)
        {
            if (context == null) throw new ArgumentNullException();

            switch (context.Request.HttpVerb)
            {
                case HttpVerb.Get:
                    return new HttpGetHandler();
                case HttpVerb.Delete:
                case HttpVerb.Post:
                case HttpVerb.Put:
                    throw new NotImplementedException();
                default:
                    throw new NotImplementedException();
            }
        }
    }
}
