﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Caching;

namespace MedTouch.Connector.RestEndpoints.RequestHandlers
{
    /// <summary>
    /// An HTTP GET RestRequestHandler implementation
    /// </summary>
    public class HttpGetHandler : RestRequestHandler
    {
        /// <summary>
        /// Processes the specified request
        /// </summary>
        /// <param name="request">The request to process, which must have an HTTP GET verb</param>
        protected override void ExecuteImpl(RestRequest request)
        {
            if (request.HttpVerb != HttpVerb.Get) throw new InvalidOperationException("Wrong HTTP verb (must be GET for this handler)");

            RestContext context = request.Context;
            RestResponse response = context.Response;
            IList<SortedDictionary<string, object>> data;

            string dataSourceName = request.Parameters.ContainsKey("datasource") ? request.Parameters["datasource"] : "";

            if (string.IsNullOrWhiteSpace(dataSourceName))
            {
                foreach (IDataSource dataSource in request.Context.Endpoint.DataSources)
                {
                    if (dataSource.IsActive && dataSource.IsValid)
                    {
                        context.Log("Retrieving data using " + dataSource.GetType().FullName + "...");
                        try
                        {
                            var key = HttpContext.Current.Request.Url.ToString();
                            data =
                                (IList<SortedDictionary<string, object>>)HttpContext.Current.Cache[key];
                            if (data == null)
                            {
                                data = dataSource.GetData(request);
                                HttpContext.Current.Cache.Add(key, data, null, DateTime.Now.AddMinutes(1),
                                    Cache.NoSlidingExpiration, CacheItemPriority.Default, null);
                            }
                            if (data != null)
                            {
                                context.Log(data.Count + " results found");
                                response.Add(data);
                            }
                        }
                        catch (Exception e)
                        {
                            response.AddException(e);
                            request.Context.Log(e);
                        }
                    }
                }
            }
            else
            {
                dataSourceName = dataSourceName.Trim();
                context.Log("Attempting to use datasource '" + dataSourceName + "'...");
                foreach (IDataSource dataSource in request.Context.Endpoint.DataSources)
                {
                    context.Log("Looping datasources. Currently on datasource '" + dataSource.DisplayName + "'...");
                    if (dataSource.IsActive && dataSource.IsValid && dataSource.Name.Trim().Equals(dataSourceName, StringComparison.InvariantCultureIgnoreCase))
                    {
                        context.Log("Retrieving data from named data source " + dataSource.Name + " using " + dataSource.GetType().FullName + "...");
                        try
                        {
                            data = dataSource.GetData(request);
                            response.Add(data);
                            context.Log(data.Count + " results found");
                        }
                        catch (Exception e)
                        {
                            response.AddException(e);
                            request.Context.Log(e);
                        }
                        break;
                    }
                }
            }
        }
    }
}
