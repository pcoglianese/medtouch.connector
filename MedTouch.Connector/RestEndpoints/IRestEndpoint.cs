// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IRestEndpoint.cs" company="MedTouch">
//   Copyright (c) MedTouch 2015. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//        Developer:    sitecore\admin
// Template Version:    3.0
//             Date:    07/23/2015
// --------------------------------------------------------------------------------------------------------------------
namespace MedTouch.Connector.RestEndpoints
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    using Newtonsoft.Json;

    using Sitecore.Data;
    using Sitecore.Data.Items;

    using MedTouch.Common;
    using MedTouch.Common.Versioning;

    using MedTouch.Connector.RestEndpoints.DataSources;

    /// <summary>
    /// The 'RestEndpoint' Interface
    /// </summary>
    public interface IRestEndpoint : IMItem
    {
        /// <summary>
        /// Gets a value indicating whether IsActive
        /// </summary>
        bool IsActive { get; }

        /// <summary>
        /// Gets a value indicating whether IsLoggingEnabled
        /// </summary>
        bool IsLoggingEnabled { get; }

        /// <summary>
        /// Gets a value indicating whether IsPrettyPrintingEnabled
        /// </summary>
        bool IsPrettyPrintingEnabled { get; }

        /// <summary>
        /// Gets a value for the HTTP content type of responses
        /// </summary>
        string ContentType { get; }

        /// <summary>
        /// Gets a value for the HTTP content encoding of responses
        /// </summary>
        string ContentEncoding { get; }

        #region Custom code

        /// <summary>
        /// Gets the data sources available to this endpoint
        /// </summary>
        IList<IDataSource> DataSources { get; }

        #endregion Custom code

    }

    /// <summary>
    /// The 'RestEndpoint' Data Template item
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    [Serializable, GuidAttribute("c056020e-3442-42dd-8978-4cd5bfb97df9")]
    [MTemplateVersion("c056020e-3442-42dd-8978-4cd5bfb97df9", "REST Endpoint")]
    public partial class RestEndpoint : MItem, IRestEndpoint
    {
        /// <summary>
        /// A template name
        /// </summary>
        private const string FacebookDataSourceTemplateName = "Facebook Data Source";

        /// <summary>
        /// A template name
        /// </summary>
        private const string InternalDataSourceTemplateName = "Internal Data Source";

        /// <summary>
        /// A template name
        /// </summary>
        private const string PinterestDataSourceTemplateName = "Pinterest Data Source";

        /// <summary>
        /// A template name
        /// </summary>
        private const string TwitterDataSourceTemplateName = "Twitter Data Source";

        /// <summary>
        /// A template name
        /// </summary>
        private const string YoutubeDataSourceTemplateName = "Youtube Data Source";

        /// <summary>
        /// Initializes a new instance of the RestEndpoint class.
        /// </summary>
        /// <param name="item">The Sitecore item.</param>
        internal RestEndpoint(Item item)
            : base(item)
        {
            this.IsActive = item.Fields[FieldNames.IsActive].ConvertAsBoolean();
            this.IsLoggingEnabled = item.Fields[FieldNames.IsLoggingEnabled].ConvertAsBoolean();
            this.IsPrettyPrintingEnabled = item.Fields[FieldNames.IsPrettyPrintingEnabled].ConvertAsBoolean();
            this.ContentType = item[FieldNames.ContentType];
            this.ContentEncoding = item[FieldNames.ContentEncoding];

            this.DataSources = new List<IDataSource>();
            IDataSource dataSource;
            foreach (Item mItem in item.Children["Data Sources"].Children)
            {
                dataSource = null;

                switch (mItem.TemplateName)
                {
                    case FacebookDataSourceTemplateName:
                        try
                        {
                            dataSource = mItem.AsMItemTypeOf<FacebookDataSource>(cacheTime: MedTouch.Core.CachingTime.None);
                        }
                        catch { }
                        break;
                    case InternalDataSourceTemplateName:
                        try
                        {
                            dataSource = mItem.AsMItemTypeOf<InternalDataSource>(cacheTime: MedTouch.Core.CachingTime.None);
                        }
                        catch { }
                        break;
                    case PinterestDataSourceTemplateName:
                        try
                        {
                            dataSource = mItem.AsMItemTypeOf<PinterestDataSource>(cacheTime: MedTouch.Core.CachingTime.None);
                        }
                        catch { }
                        break;
                    case TwitterDataSourceTemplateName:
                        try
                        {
                            dataSource = mItem.AsMItemTypeOf<TwitterDataSource>(cacheTime: MedTouch.Core.CachingTime.None);
                        }
                        catch { }
                        break;
                    case YoutubeDataSourceTemplateName:
                        try
                        {
                            dataSource = mItem.AsMItemTypeOf<YoutubeDataSource>(cacheTime: MedTouch.Core.CachingTime.None);
                        }
                        catch { }
                        break;
                }

                if (dataSource != null)
                    DataSources.Add(dataSource);
            }

        }

        #region Template Properties

        /// <summary>
        /// Gets a value indicating whether IsActive
        /// </summary>
        /// <value>The IsActive.</value>
        [JsonProperty]
        [MTemplateFieldVersion("f0bc26f2-7422-461b-9a61-bee0adb3f9ca", "Is Active", "Checkbox")]
        public bool IsActive { get; private set; }

        /// <summary>
        /// Gets a value indicating whether IsLoggingEnabled
        /// </summary>
        /// <value>The IsLoggingEnabled.</value>
        [JsonProperty]
        [MTemplateFieldVersion("71d0dd10-91a9-405f-b8cc-762a51925003", "Is Logging Enabled", "Checkbox")]
        public bool IsLoggingEnabled { get; private set; }

        /// <summary>
        /// Gets a value indicating whether IsPrettyPrintingEnabled
        /// </summary>
        /// <value>The IsPrettyPrintingEnabled.</value>
        [JsonProperty]
        [MTemplateFieldVersion("abf70f18-4af0-46af-9948-efdb20127e48", "Is Pretty Printing Enabled", "Checkbox")]
        public bool IsPrettyPrintingEnabled { get; private set; }

        /// <summary>
        /// Gets a value for the HTTP content type
        /// </summary>
        /// <value>The value</value>
        [JsonProperty]
        [MTemplateFieldVersion("c6336b33-fb40-4eb3-8fc2-71bc1350da3e", "Content Type", "Single-Line Text")]
        public string ContentType { get; private set; }

        /// <summary>
        /// Gets a value for the HTTP content type
        /// </summary>
        /// <value>The value</value>
        [JsonProperty]
        [MTemplateFieldVersion("0446577e-e08d-434e-90d6-d9877b403529", "Content Type", "Single-Line Text")]
        public string ContentEncoding { get; private set; }

        /// <summary>
        /// Gets the data sources
        /// </summary>
        /// <value>The data sources</value>
        [JsonProperty]
        public IList<IDataSource> DataSources { get; private set; }

        #endregion

        #region IRestEndpoint Properties

        /// <summary>
        /// Gets a value indicating whether IsActive
        /// </summary>
        bool IRestEndpoint.IsActive
        {
            get
            {
                return this.IsActive;
            }
        }

        /// <summary>
        /// Gets a value indicating whether IsLoggingEnabled
        /// </summary>
        bool IRestEndpoint.IsLoggingEnabled
        {
            get
            {
                return this.IsLoggingEnabled;
            }
        }

        /// <summary>
        /// Gets a value indicating whether IsPrettyPrintingEnabled
        /// </summary>
        bool IRestEndpoint.IsPrettyPrintingEnabled
        {
            get
            {
                return this.IsPrettyPrintingEnabled;
            }
        }

        /// <summary>
        /// Gets a value 
        /// </summary>
        string IRestEndpoint.ContentType
        {
            get
            {
                return this.ContentType;
            }
        }

        /// <summary>
        /// Gets a value 
        /// </summary>
        string IRestEndpoint.ContentEncoding
        {
            get
            {
                return this.ContentEncoding;
            }
        }

        /// <summary>
        /// Gets the data sources
        /// </summary>
        IList<IDataSource> IRestEndpoint.DataSources 
        {
            get
            {
                return this.DataSources;
            }
        }


        #endregion

        /// <summary>
        /// Performs an implicit conversion from <see cref="Sitecore.Data.Items.Item"/> to <see cref="RestEndpoint"/>.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>
        /// The result of the conversion.
        /// </returns>
        public static implicit operator RestEndpoint(Item item)
        {
            return item.InheritsTemplate(typeof(RestEndpoint)) ? new RestEndpoint(item) : null;
        }

        /// <summary>
        /// The Strongly typed field names for <see cref="RestEndpoint"/>.
        /// </summary>
        public static new class FieldNames
        {
            /// <summary>
            /// The Field Name
            /// </summary>
            public static readonly ID IsActive = new ID("f0bc26f2-7422-461b-9a61-bee0adb3f9ca");

            /// <summary>
            /// The Field Name
            /// </summary>
            public static readonly ID IsLoggingEnabled = new ID("71d0dd10-91a9-405f-b8cc-762a51925003");

            /// <summary>
            /// The Field Name
            /// </summary>
            public static readonly ID IsPrettyPrintingEnabled = new ID("abf70f18-4af0-46af-9948-efdb20127e48");

            /// <summary>
            /// The Field Name
            /// </summary>
            public static readonly ID ContentType = new ID("c6336b33-fb40-4eb3-8fc2-71bc1350da3e");

            /// <summary>
            /// The Field Name
            /// </summary>
            public static readonly ID ContentEncoding = new ID("0446577e-e08d-434e-90d6-d9877b403529");
        }

        #region Custom code



        #endregion Custom code


    }
}