﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Web;

namespace MedTouch.Connector.RestEndpoints
{
    /// <summary>
    /// A REST endpoint request
    /// </summary>
    public class RestRequest
    {
        /// <summary>
        /// The REST service context for this request
        /// </summary>
        public RestContext Context
        {
            get;
            private set;
        }

        /// <summary>
        /// The HTTP request for this REST request
        /// </summary>
        public HttpRequest HttpRequest { get; private set; }

        /// <summary>
        /// Indicates whether this request's parameters have been URL-decoded yet
        /// </summary>
        private SortedDictionary<string, string> parameters = null;

        /// <summary>
        /// A collection of all parameters received for this request, including from the query string and any other source
        /// </summary>
        public SortedDictionary<string, string> Parameters
        {
            get
            {
                if (parameters == null)
                {
                    SortedDictionary<string, string> copy = new SortedDictionary<string, string>();

                    foreach (string key in HttpRequest.Cookies.Keys)
                        copy[key] = HttpUtility.UrlDecode(HttpRequest.Cookies[key] + "");

                    foreach (string key in HttpRequest.Form.Keys)
                        copy[key] = HttpUtility.UrlDecode(HttpRequest.Form[key] + "");

                    foreach (string key in HttpRequest.QueryString.Keys)
                        copy[key] = HttpUtility.UrlDecode(HttpRequest.QueryString[key] + "");


                    parameters = copy;
                }

                return parameters;
            }
        }

        /// <summary>
        /// A cache key composed of the value of any cookies, form values, and the raw URL
        /// </summary>
        private string cacheKey = null;

        /// <summary>
        /// A delimiter for parameter key-value pairs appended to the cache key
        /// </summary>
        private const char CacheKeyDelimiter = '|';

        /// <summary>
        /// Gets a cache key composed of the value of any cookies, form values, and the raw URL
        /// </summary>
        public string CacheKey
        {
            get
            {
                if (cacheKey == null)
                {
                    string path = HttpRequest.Url.AbsolutePath;
                    SortedDictionary<string, string> p = Parameters;
                    StringBuilder sb = new StringBuilder(path.Length + (p.Count * 50));

                    sb.Append(path);
                    foreach (string key in p.Keys)
                        sb.Append(CacheKeyDelimiter).Append(key).Append('=').Append(p[key]);

                    cacheKey = sb.ToString();
                }

                return cacheKey;
            }
        }

        /// <summary>
        /// Gets the HTTP verb for this request
        /// </summary>
        public HttpVerb HttpVerb
        {
            get
            {
                return HttpVerb.Get;
            }
        }

        /// <summary>
        /// The time at which this request was received
        /// </summary>
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// For HTTP GET requests, the page size requested, or int.MaxValue if none was supplied
        /// </summary>
        public int PageSize { get; private set; }

        /// <summary>
        /// For HTTP GET requests, the page number requested, or 1 if none was supplied
        /// </summary>
        public int PageNumber { get; private set; }

        /// <summary>
        /// The page size parameter
        /// </summary>
        private const string PageSizeParameter = "pagesize";

        /// <summary>
        /// The page number parameter
        /// </summary>
        private const string PageNumberParameter = "page";

        /// <summary>
        /// Constructs a new instance
        /// </summary>
        /// <param name="httpRequest">The HTTP request wrapped by this instance</param>
        /// <param name="context">The REST context for this request</param>
        public RestRequest(HttpRequest httpRequest, RestContext context)
        {
            if (httpRequest == null || context == null) throw new ArgumentNullException();
            this.HttpRequest = httpRequest;
            Timestamp = DateTime.Now;
            Context = context;

            int p = 0;
            try { int.TryParse(Parameters[PageSizeParameter], out p); } catch {}
            this.PageSize = p;
            if (this.PageSize < 1)
                this.PageSize = int.MaxValue;

            if (this.PageSize == int.MaxValue)
            {
                this.PageNumber = 1;
            }
            else
            {
                p = 0;
                try { int.TryParse(Parameters[PageNumberParameter], out p); } catch {}
                this.PageNumber = Math.Max(p, 1);
            }
        }
    }
}