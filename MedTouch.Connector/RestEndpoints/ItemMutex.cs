﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using System.Threading;

using Sitecore.Data.Items;
using MedTouch.Common;

namespace MedTouch.Connector.RestEndpoints
{
    /// <summary>
    /// Wraps a mutex to provide interprocess sync, appropriately for use in 'using' statements. This can be used to ensure that multiple requests for certain transactions with 
    /// external services do not happen at once. See http://stackoverflow.com/questions/229565
    /// </summary>
    internal class ItemMutex : IDisposable
    {
        /// <summary>
        /// Indicates whether this lock currently has the handle to its named mutex
        /// </summary>
        private bool currentlyHasHandle = false;

        /// <summary>
        /// The mutex wrapped by this instance
        /// </summary>
        private Mutex mutex;

        /// <summary>
        /// The name/ID of the mutex wrapped by this instance
        /// </summary>
        private string mutexID;

        /// <summary>
        /// Acquires a mutex for the specified item
        /// </summary>
        public ItemMutex(Item item, int timeout = 0)
            : this(item.ID.ToString()) { }

        /// <summary>
        /// Acquires a mutex for the specified item
        /// </summary>
        public ItemMutex(MItem item, int timeout = 0)
            : this(item.GetOriginalItem().ID.ToString()) { }

        /// <summary>
        /// Wraps and acquires a mutex
        /// </summary>
        /// <param name="_mutexID">The mutex to acquire</param>
        /// <param name="timeoutMilliseconds">The timeout to use</param>
        public ItemMutex(string _mutexID, int timeoutMilliseconds = 0)
        {
            if (string.IsNullOrWhiteSpace(_mutexID)) throw new ArgumentException("Invalid (null or whitespace) mutex ID");
            mutexID = _mutexID.Replace('-', '_').Replace('.', '_').Replace(' ', '_').Replace("{", "").Replace("}", "");

            MutexSecurity mutexSecurity = new MutexSecurity();
            mutexSecurity.AddAccessRule(
                new MutexAccessRule(
                    new SecurityIdentifier(WellKnownSidType.WorldSid, null), 
                    MutexRights.FullControl, 
                    AccessControlType.Allow
                    )
                );

            mutex = new Mutex(false, "Global\\" + mutexID);
            mutex.SetAccessControl(mutexSecurity);

            try
            {
                if (timeoutMilliseconds < 0)
                {
                    currentlyHasHandle = mutex.WaitOne(Timeout.Infinite, false);
                }
                else
                {
                    currentlyHasHandle = mutex.WaitOne(timeoutMilliseconds, false);
                }

                if (currentlyHasHandle == false)
                {
                    throw new TimeoutException("Mutex timeout");
                }
            }
            catch (AbandonedMutexException)
            {
                currentlyHasHandle = true;
            }
        }

        /// <summary>
        /// Disposes of the mutex; when used in a using statement, do not call directly
        /// </summary>
        public void Dispose()
        {
            if (mutex != null)
            {
                if (currentlyHasHandle)
                {
                    mutex.ReleaseMutex();
                }

                mutex.Dispose();
            }
        }
    }

}
