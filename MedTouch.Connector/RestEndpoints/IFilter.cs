// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IFilter.cs" company="MedTouch">
//   Copyright (c) MedTouch 2015. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//        Developer:    sitecore\admin
// Template Version:    3.0
//             Date:    07/23/2015
// --------------------------------------------------------------------------------------------------------------------
namespace MedTouch.Connector.RestEndpoints
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    using MedTouch.Common;
    using MedTouch.Common.Versioning;

    using Newtonsoft.Json;

    using Sitecore.Data;
    using Sitecore.Data.Items;

    /// <summary>
    /// The 'Filter' Interface
    /// </summary>
    public interface IFilter : IMItem
    {
        /// <summary>
        /// Gets the Name
        /// </summary>
        string Name { get; }

        #region Custom code

        /// <summary>
        /// Indicates whether this filter excludes the specified item
        /// </summary>
        /// <param name="item">The item to check</param>
        /// <returns>true if the specified item should be filtered out</returns>
        bool Excludes(SortedDictionary<string, object> item);

        /// <summary>
        /// Indicates whether this filter excludes all items from the specified data source
        /// </summary>
        /// <param name="dataSource">The data source to check</param>
        /// <returns>true if all items from the specified data source should be filtered out</returns>
        bool Excludes(IDataSource dataSource);

        #endregion Custom code
    }

    /// <summary>
    /// The 'Filter' Data Template item
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    [Serializable, GuidAttribute("64e4fbd3-22ec-4424-bd41-8c25497bee2d")]
    [MTemplateVersion("64e4fbd3-22ec-4424-bd41-8c25497bee2d", "Filter")]
    public partial class Filter : MItem, IFilter
    {
        /// <summary>
        /// Initializes a new instance of the Filter class.
        /// </summary>
        /// <param name="item">The Sitecore item.</param>
        internal Filter(Item item)
            : base(item)
        {
            this.Name = item.Fields[FieldNames.Name].FieldRenderer();
        }

        #region Template Properties

        /// <summary>
        /// Gets the Name
        /// </summary>
        /// <value>The Name.</value>
        [JsonProperty]
        [MTemplateFieldVersion("4482a6c8-3d60-4fdf-8085-2846cdfe1d7c", "Name", "Single-Line Text")]
        public string Name { get; private set; }

        #endregion

        #region IFilter Properties

        /// <summary>
        /// Gets the Name
        /// </summary>
        string IFilter.Name
        {
            get
            {
                return this.Name;
            }
        }

        #endregion

        /// <summary>
        /// Performs an implicit conversion from <see cref="Sitecore.Data.Items.Item"/> to <see cref="Filter"/>.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>
        /// The result of the conversion.
        /// </returns>
        public static implicit operator Filter(Item item)
        {
            return item.InheritsTemplate(typeof(Filter)) ? new Filter(item) : null;
        }

        /// <summary>
        /// The Strongly typed field names for <see cref="Filter"/>.
        /// </summary>
        public static new class FieldNames
        {
            /// <summary>
            /// The Field Name
            /// </summary>
            public static readonly ID Name = new ID("4482a6c8-3d60-4fdf-8085-2846cdfe1d7c");

        }


        #region Custom code

        /// <summary>
        /// Indicates whether this filter excludes the specified item
        /// </summary>
        /// <param name="item">The item to check</param>
        /// <returns>true if the specified item should be filtered out</returns>
        public virtual bool Excludes(SortedDictionary<string, object> item) 
        { 
            return false; 
        }

        /// <summary>
        /// Indicates whether this filter excludes all items from the specified data source
        /// </summary>
        /// <param name="dataSource">The data source to check</param>
        /// <returns>true if all items from the specified data source should be filtered out</returns>
        public virtual bool Excludes(IDataSource dataSource) 
        {
            return false;
        }
        
        #endregion Custom code


    }
}