﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Sitecore.Shell.Applications.Dialogs;

using Newtonsoft.Json.Linq;

using MedTouch.Common;
using MedTouch.Common.Helpers;
using MedTouch.Connector.RestEndpoints.Filters;

namespace MedTouch.Connector.RestEndpoints
{
    /// <summary>
    /// Contains utility/helper and extension methods
    /// </summary>
    internal static class RestUtility
    {
        /// <summary>
        /// An ISO 8601 timestamp format
        /// </summary>
        internal const string Iso8601TimestampFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fffzzz";

        #region Extension methods

        #region System fields for tracking items

        /// <summary>
        /// A field type name suffix for link fields
        /// </summary>
        private const string LinkFieldTypeSuffix = "link";

        private const string SystemFieldPrefix = "__";

        /// <summary>
        /// A system field for tracking data sources used to generate data
        /// </summary>
        private const string DataSourceSystemFieldName = "_Data Source";

        /// <summary>
        /// A system field for tracking data sources used to generate data
        /// </summary>
        private const string ItemIDSystemFieldName = "_ID";

        /// <summary>
        /// A system field for tracking data sources used to generate data
        /// </summary>
        private const string ItemNameSystemFieldName = "_Name";

        /// <summary>
        /// A system field for tracking data sources used to generate data
        /// </summary>
        private const string ItemDisplayNameSystemFieldName = "_Display Name";

        /// <summary>
        /// A system field for tracking data sources used to generate data
        /// </summary>
        private const string TemplateIDSystemFieldName = "_Template ID";

        /// <summary>
        /// A system field
        /// </summary>
        private const string TitleSystemFieldName = "_Title";

        /// <summary>
        /// A system field
        /// </summary>
        private const string UrlSystemFieldName = "_URL";

        /// <summary>
        /// A system field
        /// </summary>
        private const string ExternalIDSystemFieldName = "_ID";

        /// <summary>
        /// A system field
        /// </summary>
        private const string TimestampSystemFieldName = "_Timestamp";

        /// <summary>
        /// A list of system field names
        /// </summary>f (
        private static string[] SystemFieldNames = new string[] {
            DataSourceSystemFieldName,
            ItemIDSystemFieldName,
            TemplateIDSystemFieldName, 
            TitleSystemFieldName,
            UrlSystemFieldName,
            ExternalIDSystemFieldName
        };

        /// <summary>
        /// A list of removable system field names
        /// </summary>
        private static string[] RemovableSystemFieldNames = new string[] {
            TemplateIDSystemFieldName,
            TitleSystemFieldName
        };

        /// <summary>
        /// Removes system fields from this item
        /// </summary>
        /// <param name="item">this item</param>
        internal static void RemoveSystemFields(this SortedDictionary<string, object> item)
        {
            if (item == null) return;

            foreach (string fieldName in RemovableSystemFieldNames)
                item.Remove(fieldName);
        }

        /// <summary>
        /// Removes system fields from this item list
        /// </summary>
        /// <param name="item">this item list</param>
        internal static void RemoveSystemFields(this IEnumerable<SortedDictionary<string, object>> items)
        {
            if (items == null) return;

            foreach (SortedDictionary<string, object> item in items)
                item.RemoveSystemFields();
        }

        /// <summary>
        /// A dictionary of system field names for quick lookup
        /// </summary>
        private static Dictionary<string, string> SystemFieldNamesMap = new Dictionary<string, string>();

        /// <summary>
        /// Initializes this class
        /// </summary>
        static RestUtility()
        {
            foreach (string fieldName in SystemFieldNames)
                SystemFieldNamesMap[fieldName] = fieldName;
        }

        /// <summary>
        /// Gets system fields for this item
        /// </summary>
        /// <param name="item">this item</param>
        /// <returns>Just the system fields for this item</returns>
        internal static SortedDictionary<string, object> GetSystemFields(this SortedDictionary<string, object> item)
        {
            if (item == null) throw new ArgumentNullException();

            SortedDictionary<string, object> systemFields = new SortedDictionary<string, object>();

            object value;
            foreach (string fieldName in SystemFieldNames)
                if (item.TryGetValue(fieldName, out value))
                    systemFields[fieldName] = value;

            return systemFields;
        }

        /// <summary>
        /// Gets non-system fields for this item
        /// </summary>
        /// <param name="item">this item</param>
        /// <returns>Just the non-system fields for this item</returns>
        internal static SortedDictionary<string, object> GetNonSystemFields(this SortedDictionary<string, object> item)
        {
            if (item == null) throw new ArgumentNullException();

            SortedDictionary<string, object> nonSystemFields = new SortedDictionary<string, object>();

            object value;
            foreach (string fieldName in item.Keys)
                if (!SystemFieldNamesMap.ContainsKey(fieldName))
                    nonSystemFields[fieldName] = item[fieldName];

            return nonSystemFields;
        }

        /// <summary>
        /// Sets a system field value
        /// </summary>
        /// <param name="item">this item</param>
        /// <returns>The field value as a string</returns>
        private static void SetSystemField(this SortedDictionary<string, object> item, string fieldName, string value)
        {
            if (item == null || string.IsNullOrWhiteSpace(fieldName) || string.IsNullOrWhiteSpace(value)) 
                throw new ArgumentException("Invalid (null or empty) value");
            
            item[fieldName] = value;
        }

        /// <summary>
        /// Gets the value of a system field
        /// </summary>
        /// <param name="item">this item</param>
        /// <returns>The name of the data source which generated this item</returns>
        internal static string GetSystemField(this SortedDictionary<string, object> item, string fieldName)
        {
            object value = "";

            if (item.TryGetValue(fieldName, out value))
                return value.ToString();
            else
                return "";
        }


        /// <summary>
        /// Gets the name of the data source used to generate this item
        /// </summary>
        /// <param name="item">this item</param>
        /// <returns>The name of the data source which generated this item</returns>
        internal static string GetDataSourceName(this SortedDictionary<string, object> item)
        {
            return item.GetSystemField(DataSourceSystemFieldName);
        }

        /// <summary>
        /// Sets the system field for tracking data sources for items
        /// </summary>
        /// <param name="item">this item</param>
        /// <param name="dataSourceName">The name of the data source used to generate this item</param>
        internal static void SetDataSourceName(this SortedDictionary<string, object> item, string dataSourceName)
        {
            item.SetSystemField(DataSourceSystemFieldName, dataSourceName);
        }

        /// <summary>
        /// Gets the item ID as a string in standard Sitecore ID format (brackets + upper case)
        /// </summary>
        /// <param name="item">this item</param>
        /// <returns>The item ID</returns>
        internal static string GetItemID(this SortedDictionary<string, object> item)
        {
            return item.GetSystemField(ItemIDSystemFieldName);
        }

        /// <summary>
        /// Sets a data value
        /// </summary>
        /// <param name="item">this item</param>
        /// <param name="value">The value to set</param>
        internal static void SetItemID(this SortedDictionary<string, object> item, string value)
        {
            item.SetSystemField(ItemIDSystemFieldName, value);
        }

        /// <summary>
        /// Gets the item template ID as a string in standard Sitecore ID format (brackets + upper case)
        /// </summary>
        /// <param name="item">this item</param>
        /// <returns>The item ID</returns>
        internal static string GetTemplateID(this SortedDictionary<string, object> item)
        {
            return item.GetSystemField(TemplateIDSystemFieldName);
        }

        /// <summary>
        /// Sets a data value
        /// </summary>
        /// <param name="item">this item</param>
        /// <param name="value">The value to set</param>
        internal static void SetTemplateID(this SortedDictionary<string, object> item, string value)
        {
            item.SetSystemField(TemplateIDSystemFieldName, value);
        }

        /// <summary>
        /// Gets a data value
        /// </summary>
        /// <param name="item">this item</param>
        /// <returns>The value</returns>
        internal static string GetTitle(this SortedDictionary<string, object> item)
        {
            return item.GetSystemField(TitleSystemFieldName);
        }

        /// <summary>
        /// Sets a data value
        /// </summary>
        /// <param name="item">this item</param>
        /// <param name="value">The value to set</param>
        internal static void SetTitle(this SortedDictionary<string, object> item, string value)
        {
            item.SetSystemField(TitleSystemFieldName, value);
        }

        /// <summary>
        /// Gets a data value
        /// </summary>
        /// <param name="item">this item</param>
        /// <returns>The value</returns>
        internal static string GetUrl(this SortedDictionary<string, object> item)
        {
            return item.GetSystemField(UrlSystemFieldName);
        }

        /// <summary>
        /// Sets a data value
        /// </summary>
        /// <param name="item">this item</param>
        /// <param name="value">The value to set</param>
        internal static void SetUrl(this SortedDictionary<string, object> item, string value)
        {
            item.SetSystemField(UrlSystemFieldName, value);
        }

        /// <summary>
        /// Gets a data value
        /// </summary>
        /// <param name="item">this item</param>
        /// <returns>The value</returns>
        internal static string GetExternalID(this SortedDictionary<string, object> item)
        {
            return item.GetSystemField(ExternalIDSystemFieldName);
        }

        /// <summary>
        /// Sets a data value
        /// </summary>
        /// <param name="item">this item</param>
        /// <param name="value">The value to set</param>
        internal static void SetExternalID(this SortedDictionary<string, object> item, string value)
        {
            item.SetSystemField(ExternalIDSystemFieldName, value);
        }

        /// <summary>
        /// Gets the name of the data source used to generate this item
        /// </summary>
        /// <param name="item">this item</param>
        /// <returns>The name of the data source which generated this item</returns>
        internal static string GetTimestamp(this SortedDictionary<string, object> item)
        {
            return item.GetSystemField(TimestampSystemFieldName);
        }

        /// <summary>
        /// Sets a data value
        /// </summary>
        /// <param name="item">this item</param>
        /// <param name="value">The value to set</param>
        internal static void SetTimestamp(this SortedDictionary<string, object> item, DateTime value)
        {
            item.SetSystemField(TimestampSystemFieldName, value.ToString(Iso8601TimestampFormat));
        }

        /// <summary>
        /// Sets a data value
        /// </summary>
        /// <param name="item">this item</param>
        /// <param name="value">The value to set</param>
        internal static void SetTimestamp(this SortedDictionary<string, object> item, string value)
        {
            item.SetSystemField(TimestampSystemFieldName, value);
        }

        /// <summary>
        /// Sets Sitecore fields for tracking this item
        /// </summary>
        /// <param name="item">this item</param>
        /// <param name="sitecoreItem">The Sitecore item corresponding to this item</param>
        internal static void SetSitecoreFields(this SortedDictionary<string, object> item, Item sitecoreItem)
        {
            if (item == null || sitecoreItem == null) throw new ArgumentNullException();
            
            item.SetItemID(sitecoreItem.ID.ToString());
            item.SetTemplateID(sitecoreItem.TemplateID.ToString());
            item.SetUrl(sitecoreItem.GetItemUrl());
            item.SetTitle(sitecoreItem.DisplayName); 
   
            DateTime? timestamp = sitecoreItem.GetSitecoreDateField("__Updated");
            if (timestamp == null)
                timestamp = sitecoreItem.GetSitecoreDateField("__Created");
            if (timestamp != null)
                item.SetTimestamp((DateTime)timestamp);

            item[ItemNameSystemFieldName] = sitecoreItem.Name;
            item[ItemDisplayNameSystemFieldName] = sitecoreItem.DisplayName;
        }

        /// <summary>
        /// Indicates whether this item contain Sitecore item data
        /// </summary>
        /// <param name="item">this item</param>
        /// <returns>true if this is a Sitecore item, otherwise false</returns>
        internal static bool IsSitecoreItem(this SortedDictionary<string, object> item)
        {
            if (item == null) throw new ArgumentNullException();
            return item.ContainsKey(ItemIDSystemFieldName);
        }

        /// <summary>
        /// Loads this item's data
        /// </summary>
        /// <param name="item">this item</param>
        /// <param name="sitecoreItem">the Sitecore item from which to populate this item's data</param>
        internal static void Load(this SortedDictionary<string, object> item, Item sitecoreItem, RenderingHint imageFieldRenderingHint, RenderingHint linkFieldRenderingHint, RenderingHint textFieldRenderingHint)
        {
            if (item == null || sitecoreItem == null) return;

            string fieldName, fieldValue;
            foreach (Field field in sitecoreItem.Fields)
            {
                fieldName = field.Name;
                fieldValue = "";

                try 
                {
                    if (fieldName.StartsWith(SystemFieldPrefix))
                    {
                        fieldValue = sitecoreItem[fieldName];
                    }
                    else
                    {
                        if (imageFieldRenderingHint == RenderingHint.Raw && linkFieldRenderingHint == RenderingHint.Raw && textFieldRenderingHint == RenderingHint.Raw)
                        {
                            fieldValue = sitecoreItem[fieldName];
                        }
                        else
                        {
                            string fieldType = field.Type.ToLower();

                            if (fieldType == "image")
                            {
                                switch (imageFieldRenderingHint) 
                                {
                                    case RenderingHint.Url:
                                        fieldValue = new ImageField(field).GetMediaUrl();
                                        break;
                                    case RenderingHint.Rendered:
                                    case RenderingHint.Tag:
                                        fieldValue = field.FieldRenderer();
                                        break;
                                    default:
                                        fieldValue = sitecoreItem[fieldName];
                                        break;
                                }
                            }
                            else if (fieldType == "general link")
                            {
                                switch (linkFieldRenderingHint)
                                {
                                    case RenderingHint.Url:
                                        LinkField linkField = field;
                                        if (linkField != null)
                                            fieldValue = linkField.GetGeneralLinkUrl();
                                        break;
                                    case RenderingHint.Rendered:
                                    case RenderingHint.Tag:
                                        fieldValue = field.FieldRenderer();
                                        break;
                                    default:
                                        fieldValue = sitecoreItem[fieldName];
                                        break;
                                }
                            }
                            else if (field.Type.ToLower().EndsWith(LinkFieldTypeSuffix))
                            {
                                switch (linkFieldRenderingHint)
                                {
                                    case RenderingHint.Url:
                                        fieldValue = new LinkField(field).GetFriendlyUrl();
                                        break;
                                    case RenderingHint.Rendered:
                                    case RenderingHint.Tag:
                                        fieldValue = field.FieldRenderer();
                                        break;
                                    default:
                                        fieldValue = sitecoreItem[fieldName];
                                        break;
                                }
                            }
                            else 
                            {
                                switch (linkFieldRenderingHint)
                                {
                                    case RenderingHint.Plain:
                                        fieldValue = sitecoreItem[fieldName].StripTags();
                                        break;
                                    case RenderingHint.Rendered:
                                        fieldValue = field.FieldRenderer();
                                        break;
                                    default:
                                        fieldValue = sitecoreItem[fieldName];
                                        break;
                                }
                            }

                        }
                    }
                }
                catch { }

                if (!string.IsNullOrWhiteSpace(fieldValue)) 
                    item[fieldName] = fieldValue;
            }

            item.SetSitecoreFields(sitecoreItem);
        }

        /*
        /// <summary>
        /// Performs a weak equality test on two objects
        /// </summary>
        /// <param name="item1">this item</param>
        /// <param name="item2">The item to test for equality</param>
        /// <returns>true if the two items seem to be equal</returns>
        internal static bool SeemsEqualTo(this SortedDictionary<string, object> item1, SortedDictionary<string, object> item2)
        {
            if (item1 == null)
                return (item2 == null);
            else if (item2 == null)
                return false;

            if (item1.IsSitecoreItem())
            {
                if (item2.IsSitecoreItem())
                    return item1.GetItemID() == item2.GetItemID();
                else
                    return false;
            }
            else
            {
                if (item2.IsSitecoreItem())
                    return false;
                else
                    return
                        (item1.Count == item2.Count) &&
                        (item1.GetTitle() == item2.GetTitle()) &&
                        (item1.GetUrl() == item2.GetUrl()) &&
                        (item1.GetExternalID() == item2.GetExternalID());

            }
        }
         */

        /// <summary>
        /// Gets an identifier for this item
        /// </summary>
        /// <param name="item">this item</param>
        /// <returns>Some identifier, which may be an internal or external ID or URL</returns>
        internal static string GetIdentifier(this SortedDictionary<string, object> item)
        {
            if (item == null) return "";

            string id = item.GetItemID();
            if (!string.IsNullOrWhiteSpace(id)) return id;
            id = item.GetExternalID();
            if (!string.IsNullOrWhiteSpace(id)) return id;
            id = item.GetUrl();
            return id;
        }

        /// <summary>
        /// Merges a list into this one, in place
        /// </summary>
        /// <param name="list1">this list</param>
        /// <param name="list2">the list to merge in</param>
        //internal static void Merge(this List<SortedDictionary<string, object>> list1, List<SortedDictionary<string, object>> list2, out Dictionary<string, SortedDictionary<string, object>> itemMap)
        //{
        //    if (list1 == null || list2 == null || list2.Count == 0) return;
        //    string itemID;

        //    if (itemMap == null)
        //    {
        //        itemMap = new Dictionary<string, SortedDictionary<string, object>>();
        //        itemMap[""] = null;
        //        foreach (SortedDictionary<string, object> item in list1)
        //        {
        //            itemID = item.GetIdentifier();
        //            if (!string.IsNullOrWhiteSpace(itemID)) 
        //                itemMap[itemID] = item;
        //        }
        //    }

        //    foreach (SortedDictionary<string, object> item in list2)
        //    {
        //        itemID = item.GetIdentifier();
        //        if (!itemMap.ContainsKey(itemID))
        //        {
        //            list1.Add(item);
        //            itemMap[itemID] = item;
        //        }
        //    }
        //}

        /// <summary>
        /// Gets the specified page of items from this list
        /// </summary>
        /// <param name="items">this list of items</param>
        /// <param name="pageSize">The page size with which to perform paging calculations</param>
        /// <param name="pageNumber">The number of the page to retrieve</param>
        /// <returns>A sublist containing items in the desired paging range</returns>
        internal static List<SortedDictionary<string, object>> GetPage(this IList<SortedDictionary<string, object>> items, int pageSize, int pageNumber)
        {
            if (items == null) throw new ArgumentNullException();
            if (pageSize < 1)
                pageSize = int.MaxValue;
            pageNumber = Math.Max(1, pageNumber);

            if (pageNumber == 1) 
                return (pageSize >= items.Count) ? items.ToList() : items.Take(pageSize).ToList();
            else
                return (pageSize >= items.Count) ? new List<SortedDictionary<string, object>>() : items.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
        }

        /// <summary>
        /// Gets the number of pages contained in this list, of the specified size
        /// </summary>
        /// <param name="items">this list of items</param>
        /// <param name="pageSize">The page size with which to perform paging calculations</param>
        /// <returns>The number of pages of the specified size in this list</returns>
        internal static int PageCount(this IList<SortedDictionary<string, object>> items, int pageSize)
        {
            if (items == null) throw new ArgumentNullException();
            
            if (items.Count == 0)
                return 0;
            else if (pageSize < 1 || pageSize == int.MaxValue)
                return 1;

            return (items.Count % pageSize == 0) ? items.Count / pageSize : items.Count / pageSize + 1;
        }

        #endregion System fields for tracking items

        /// <summary>
        /// Strips HTML/XML-style tags, i.e. beginning and ending with carrots, from this string. Well-formed HTML/XML is assumed.
        /// </summary>
        /// <param name="s">this string</param>
        /// <param name="replaceTagsWithSpaces">If true, any tags found will be replaced with spaces</param>
        /// <param name="collapseWhiteSpace">If true, the resulting string will be stripped, and all runs of any whitespace characters will be replaced by a single space</param>
        /// <param name="decodeHtml">If true, the result will be HTML decoded</param>
        /// <returns>This string, with HTML tags stripped</returns>
        public static string StripTags(this string s, bool replaceTagsWithSpaces = true, bool collapseWhiteSpace = true, bool decodeHtml = true)
        {
            if (s == null) return null;

            StringBuilder sb = new StringBuilder(s.Length);
            char c;
            bool inTag = false;
            bool lastWasWhitespace = true;

            for (int x = 0; x < s.Length; x++)
            {
                c = s[x];

                if (c == '<')
                {
                    inTag = true;
                }
                else if (c == '>')
                {
                    if (replaceTagsWithSpaces && (!collapseWhiteSpace || !lastWasWhitespace))
                    {
                        sb.Append(' ');
                        lastWasWhitespace = true;
                    }
                    inTag = false;
                }
                else if (!inTag)
                {
                    if (collapseWhiteSpace)
                    {
                        if (char.IsWhiteSpace(c))
                        {
                            if (!lastWasWhitespace)
                                sb.Append(c);
                            lastWasWhitespace = true;
                        }
                        else
                        {
                            sb.Append(c);
                            lastWasWhitespace = false;
                        }
                    }
                    else
                    {
                        sb.Append(c);
                    }
                }
            }

            string returnValue = sb.ToString();
            if (decodeHtml)
                returnValue = HttpUtility.HtmlDecode(returnValue);
            return returnValue;
        }

        /// <summary>
        /// Decodes a rendering hint from the specified value
        /// </summary>
        /// <param name="name">The name of the rendering hint</param>
        /// <returns>A rendering hint</returns>
        internal static RenderingHint DecodeRenderingHint(string name, RenderingHint defaultValue = RenderingHint.Raw)
        {
            name = (name + "").ToLower();
            switch (name)
            {
                case "plain": return RenderingHint.Plain;
                case "raw": return RenderingHint.Raw;
                case "rendered": return RenderingHint.Rendered;
                case "tag": return RenderingHint.Tag;
                case "target": return RenderingHint.Target;
                case "url": return RenderingHint.Url;
            }

            return defaultValue;
        }

        /// <summary>
        /// Replaces placeholder parameter names in text
        /// </summary>
        /// <param name="s">this string</param>
        /// <param name="parameters">The parameters to replace</param>
        /// <param name="parameterPrefix">The prefix occurring in front of parameter names to be replaced</param>
        /// <param name="parameterSuffix">The suffix occurring after parameter names to be replaced</param>
        /// <returns>A version of this string with parameters replaced</returns>
        public static string ReplaceParameters(this string s, SortedDictionary<string, string> parameters, string parameterPrefix = "{{{", string parameterSuffix = "}}}")
        {
            if (s == null) return "";
            else if (parameters == null) return s;

            foreach (string key in parameters.Keys)
                s = s.Replace(parameterPrefix + key + parameterSuffix, parameters[key]);

            return s;
        }

        /// <summary>
        /// Replaces item field tokens generally of the format {field:[field name]} in this string, using the specified item
        /// </summary>
        /// <param name="s">this string</param>
        /// <param name="item">The item containing the fields to use</param>
        /// <param name="parameterPrefix">The prefix for the token</param>
        /// <param name="parameterSuffix">The suffix for the token</param>
        /// <returns>A versio of this string with any field tokens replaced</returns>
        public static string ReplaceFieldTokens(this string s, SortedDictionary<string, object> item, string parameterPrefix = "{", string parameterSuffix = "}", bool stripUnfoundTokens = true)
        {
            if (string.IsNullOrWhiteSpace(s)) return (s + "");
            else if (item == null) return s;
            else if (string.IsNullOrWhiteSpace(parameterPrefix) || string.IsNullOrWhiteSpace(parameterSuffix)) return s;

            parameterPrefix = parameterPrefix.Trim() + "field:";
            if (s.IndexOf(parameterPrefix) < 0)
                return s;
            parameterSuffix = parameterSuffix.Trim();
            
            string fieldValue, token;            
            foreach (string fieldName in item.Keys)
            {
                token = parameterPrefix + fieldName + parameterSuffix;
                fieldValue = item[fieldName].ToString();
                s = s.Replace(token, fieldValue);
            }

            if (stripUnfoundTokens)
            {
                try {
                    int tokenStart = s.IndexOf(parameterPrefix);
                    int tokenEnd = s.IndexOf(parameterSuffix, tokenStart + 1);

                    while (tokenStart > -1 && tokenEnd > -1)
                    {
                        if (tokenStart == 0)
                        {
                            if (tokenEnd == s.Length - 1)
                                return "";
                            else
                                s = s.Substring(tokenEnd + 1);
                        }
                        else
                        {
                            if (tokenEnd == s.Length - 1)
                                return s.Substring(0, tokenStart);
                            else
                                s = s.Substring(0, tokenStart) + s.Substring(tokenEnd + 1, s.Length - (tokenEnd + 1));
                        }
                    }
                } catch {}
            }

            return s;
        }

        /// <summary>
        /// Replaces dictionary tokens generally of the format {dictionary:[dictionary key name]} in this string
        /// </summary>
        /// <param name="s">this string</param>
        /// <returns>A versio of this string with any dictionary tokens replaced</returns>
        public static string ReplaceDictionarySettingTokens(this string s)
        {
            if (string.IsNullOrWhiteSpace(s)) return (s + "");

            try
            {
                int i = s.IndexOf("{dictionary:");
                if (i < 0)
                    return s;

                int j = -1;
                int settingStart;
                string settingName, settingValue;

                StringBuilder sb = new StringBuilder(s.Length + 100);

                while (i > -1)
                {
                    sb.Append(s.Substring(j + 1, i - (j + 1)));
                    j = s.IndexOf('}', i);

                    if (j > -1)
                    {
                        settingStart = i + 12;
                        settingName = s.Substring(settingStart, j - settingStart);

                        // Get dictionary setting value, or a blank string if not found
                        if (!string.IsNullOrWhiteSpace(settingName))
                        {
                            settingName = settingName.Trim();
                            settingValue = CultureHelper.GetDictionaryTranslation(settingName);
                            if (settingValue != null && settingValue != settingName)
                                sb.Append(settingValue);
                        }

                        i = s.IndexOf("{dictionary:", j);
                    }
                    else
                    {
                        j = i - 1;
                        i = -1;
                    }
                }

                int remainingLength = (s.Length - j) - 1;
                if (remainingLength > 0)
                    sb.Append(s.Substring(j + 1, remainingLength));

                return sb.ToString();
            }
            catch { }

            return s;
        }

        /// <summary>
        /// Converts this NameValueCollection to a SortedDictionary
        /// </summary>
        /// <param name="nvc">this instance</param>
        /// <returns></returns>
        public static SortedDictionary<string, string> ToSortedDictionary(this NameValueCollection nvc)
        {
            SortedDictionary<string, string> sd = new SortedDictionary<string, string>();
            if (nvc == null) return sd;

            foreach (string key in nvc.Keys)
                if (!string.IsNullOrWhiteSpace(key))
                    sd[key.Trim()] = nvc[key] + "";

            return sd;
        }

        /// <summary>
        /// Indicates whether the specified HTTP status code is valid
        /// </summary>
        /// <param name="s">The code to validate</param>
        /// <returns></returns>
        internal static bool IsValidHttpStatusCode(this string s)
        {
            if (string.IsNullOrWhiteSpace(s)) return false;
            int i = 0;

            if (!int.TryParse(s, out i)) 
               return false;

            return (i >= 100 && i < 600);
        }

        /// <summary>
        /// Gets a compact extended single-line message for this exception
        /// </summary>
        /// <param name="e">This exception</param>
        /// <returns>An extended exception message</returns>
        internal static string GetExtendedMessage(this Exception e)
        {
            if (e == null) return "";

            StringBuilder sb = new StringBuilder();
            sb.Append(e.GetType().FullName);
            if (!string.IsNullOrWhiteSpace(e.Message))
                sb.Append(": ").Append(e.Message);

            StackTrace stackTrace = new StackTrace(e, true);
            if (stackTrace.FrameCount > 0)
            {
                try
                {
                    StackFrame stackFrame = stackTrace.GetFrame(0);
                    MethodBase method = stackFrame.GetMethod();
                    string className = method.DeclaringType.FullName;
                    string methodName = method.Name;
                    int lineNumber = stackFrame.GetFileLineNumber();
                    string fileName = stackFrame.GetFileName();

                    sb.Append(" (").Append(className).Append(".").Append(methodName);
                    
                    if (!string.IsNullOrWhiteSpace(fileName))
                        sb.Append(",").Append(fileName);
                    
                    if (lineNumber > 0)
                        sb.Append(":").Append(lineNumber);
                    
                    sb.Append(")");
                }
                catch { }
            }

            return sb.ToString();
        }

        #region Sorting 

        private class FieldComparer : IComparer<SortedDictionary<string, object>>
        {
            /// <summary>
            /// The field name used by this comparer
            /// </summary>
            public string FieldName { get; private set; }

            /// <summary>
            /// Indicates whether sorting supported by this comparer will be ascending or descending
            /// </summary>
            public bool Ascending { get; private set; }

            /// <summary>
            /// Hides the default constructor
            /// </summary>
            private FieldComparer() {}

            /// <summary>
            /// Constructs a new instance
            /// </summary>
            /// <param name="fieldName">The field name to use</param>
            public FieldComparer(string fieldName, bool ascending) 
            {
                if (string.IsNullOrWhiteSpace(fieldName)) throw new ArgumentException("Invalid (null or whitespace) field name");
                FieldName = fieldName.Trim();
                Ascending = ascending;
            }

            /// <summary>
            /// Compares two items by a field
            /// </summary>
            /// <param name="x"></param>
            /// <param name="y"></param>
            /// <returns></returns>
            public int Compare(SortedDictionary<string, object> x, SortedDictionary<string, object> y)
            {
                if (x == null)
                {
                    if (y == null) return 0;
                    else return (Ascending ? 1 : -1);
                }
                else if (y == null) return (Ascending ? -1 : 1);

                object xValue, yValue;
                bool gotXValue = x.TryGetValue(FieldName, out xValue);
                bool gotYValue = y.TryGetValue(FieldName, out yValue);

                if (!gotXValue || xValue == null) 
                {
                    if (!gotYValue || yValue == null) return 0;
                    else return (Ascending ? 1 : -1);
                }
                else if (!gotYValue || yValue == null) return (Ascending ? -1 : 1);

                int comparison;
                if (xValue is string || yValue is string)
                    comparison = xValue.ToString().CompareTo(yValue.ToString());
                else if (xValue is int && yValue is int)
                    comparison = ((int)xValue).CompareTo((int)yValue);
                else if (xValue is DateTime && yValue is DateTime)
                    comparison = ((DateTime)xValue).CompareTo((DateTime)yValue);
                else if (xValue is long && yValue is long)
                    comparison = ((long)xValue).CompareTo((long)yValue);
                else if (xValue is float && yValue is float)
                    comparison = ((float)xValue).CompareTo((float)yValue);
                else if (xValue is double && yValue is double)
                    comparison = ((double)xValue).CompareTo((double)yValue);
                else if (xValue is decimal && yValue is decimal)
                    comparison = ((decimal)xValue).CompareTo((decimal)yValue);
                else
                    comparison = xValue.ToString().CompareTo(yValue.ToString());

                return Ascending ? comparison : comparison * -1;
            }

        }
        
        /// <summary>
        /// Sorts this list by the specified field
        /// </summary>
        /// <param name="list">this list</param>
        /// <param name="fieldName">The name of the field by which to sort</param>
        /// <param name="ascending">Whether to sort ascending (true) or descending (false)</param>
        internal static void SortByField(this List<SortedDictionary<string, object>> list, string fieldName, bool ascending = true)
        {
            if (list == null) return;
            else if (string.IsNullOrWhiteSpace(fieldName)) throw new ArgumentException("Invalid (null or whitespace) field name");

            fieldName = fieldName.Trim();

            list.Sort(new FieldComparer(fieldName, ascending));
        }

        #endregion Sorting

        #endregion Extension methods

        /* Commenting out for now... may no longer be necessary because of discontinued use of JavascriptSerializer
        /// <summary>
        /// Constructs a human-friendly version of JSON, for use in preview mode
        /// </summary>
        /// <param name="text">The JSON source text</param>
        /// <returns>Pretty-formatted JSON</returns>
        internal static string PrettyFormatJson(string text)
        {
            // From http://dl.dropboxusercontent.com/u/52219470/Source/JSonPresentationFormatter.cs and http://chrisghardwick.blogspot.com/2012/01/c-json-pretty-print-simple-c-json.html
            if (string.IsNullOrEmpty(text)) return string.Empty;
            text = text.Replace(System.Environment.NewLine, string.Empty).Replace("\t", string.Empty);

            var offset = 0;
            var output = new System.Text.StringBuilder();
            Action<System.Text.StringBuilder, int> tabs = (sb, pos) => { for (var i = 0; i < pos; i++) { sb.Append("\t"); } };
            Func<string, int, Nullable<Char>> previousNotEmpty = (s, i) =>
            {
                if (string.IsNullOrEmpty(s) || i <= 0) return null;

                Nullable<Char> prev = null;

                while (i > 0 && prev == null)
                {
                    prev = s[i - 1];
                    if (prev.ToString() == " ") prev = null;
                    i--;
                }

                return prev;
            };
            Func<string, int, Nullable<Char>> nextNotEmpty = (s, i) =>
            {
                if (string.IsNullOrEmpty(s) || i >= (s.Length - 1)) return null;

                Nullable<Char> next = null;
                i++;

                while (i < (s.Length - 1) && next == null)
                {
                    next = s[i++];
                    if (next.ToString() == " ") next = null;
                }

                return next;
            };

            bool insideQuotes = false;
            char chr;
            for (var i = 0; i < text.Length; i++)
            {
                chr = text[i];

                if (chr == '"')
                {
                    insideQuotes = !insideQuotes;
                    output.Append(chr);
                }
                else if (chr == '{')
                {
                    offset++;
                    output.Append(chr);
                    output.Append(System.Environment.NewLine);
                    tabs(output, offset);
                }
                else if (chr == '}')
                {
                    offset--;
                    output.Append(System.Environment.NewLine);
                    tabs(output, offset);
                    output.Append(chr);

                }
                else if (chr == ',')
                {
                    output.Append(chr);

                    if (!insideQuotes)
                    {
                        output.Append(System.Environment.NewLine);
                        tabs(output, offset);
                    }
                }
                else if (chr == '[')
                {
                    output.Append(chr);

                    var next = nextNotEmpty(text, i);

                    if (next != null && next != ']')
                    {
                        offset++;
                        output.Append(System.Environment.NewLine);
                        tabs(output, offset);
                    }
                }
                else if (chr == ']')
                {
                    var prev = previousNotEmpty(text, i);

                    if (prev != null && prev != '[')
                    {
                        offset--;
                        output.Append(System.Environment.NewLine);
                        tabs(output, offset);
                    }

                    output.Append(chr);
                }
                else
                    output.Append(chr);
            }

            return output.ToString().Trim();
        }

        /// <summary>
        /// Pretty-formats the specified string for use in Sitecore content editor preview mode
        /// </summary>
        /// <param name="json">The string to pretty-print/pretty-format</param>
        /// <returns>A preview-friendly formatted string</returns>
        internal static string PrettyFormatJsonPreview(string json)
        {
            return PrettyFormatJson(json)
                    .Replace("\n", "<br/>\n")
                    .Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;")
                    .Replace("\":", "\" : ");
        }
         */

        /// <summary>
        /// Multiple field value delimiters
        /// </summary>
        private static char[] MultipleFieldValueDelimiters = new char[] { '|', '\n', '\r' };

        /// <summary>
        /// Removes null-or-white-space entries from a copy of this list, and returns the copy
        /// </summary>
        /// <param name="list">this list</param>
        /// <returns>A copy of this list with empty/null entries removed</returns>
        internal static List<string> RemoveNullAndWhiteSpaceEntries(this List<string> list)
        {
            if (list == null) throw new ArgumentNullException();

            List<string> copy = new List<string>();

            foreach (string s in list)
                if (!string.IsNullOrWhiteSpace(s))
                    copy.Add(s);

            return copy;
        }

        /// <summary>
        /// Gets a list of delimited raw values from a field
        /// </summary>
        /// <param name="item">This item</param>
        /// <param name="fieldName">The name of the field from which to get the values</param>
        /// <returns>A list of delimited values</returns>
        internal static List<string> GetMultipleFieldValues(this Item item, string fieldName)
        {
            if (item == null || string.IsNullOrWhiteSpace(fieldName))
                return new List<string>();
            fieldName = fieldName.Trim();

            string fieldValue = item[fieldName];
            if (string.IsNullOrWhiteSpace(fieldValue))
                return new List<string>();

            return fieldValue.Split(MultipleFieldValueDelimiters, StringSplitOptions.RemoveEmptyEntries).ToList<string>().RemoveNullAndWhiteSpaceEntries();
        }

        /// <summary>
        /// Gets a list of delimited raw values from a field
        /// </summary>
        /// <param name="fieldValue"></param>
        /// <returns></returns>
        internal static List<string> GetMultipleFieldValues(this string fieldValue)
        {
            if (string.IsNullOrWhiteSpace(fieldValue))
                return new List<string>();

            return fieldValue.Split(MultipleFieldValueDelimiters, StringSplitOptions.RemoveEmptyEntries).ToList<string>().RemoveNullAndWhiteSpaceEntries();
        }

        /// <summary>
        /// Joins this list of strings with the specified delimiter, eliminating null/empty/whitespace entries
        /// </summary>
        /// <param name="s">this list of strings</param>
        /// <returns>A joined, delimited string</returns>
        internal static string Join(this IEnumerable<string> sa, string delimiter)
        {
            if (sa == null) 
                return "";
            else if (delimiter == null)
                delimiter = "";

            StringBuilder sb = new StringBuilder();

            foreach (string s in sa)
            {
                if (!string.IsNullOrWhiteSpace(s))
                {
                    if (sb.Length > 0)
                        sb.Append(delimiter);

                    sb.Append(s);
                }
            }

            return sb.ToString();
        }

        /// <summary>
        /// Gets the value of the specified field from this JSON parsed token, returning an empty string if not found
        /// </summary>
        /// <param name="token">this token</param>
        /// <param name="fieldNames">one or more field names to check, in the specified order, for a value</param>
        /// <returns></returns>
        internal static string Get(this JToken token, params string[] fieldNames)
        {
            if (token == null || fieldNames == null || fieldNames.Length == 0) return "";

            foreach (string fieldName in fieldNames)
            {
                if (string.IsNullOrWhiteSpace(fieldName)) continue;

                try {
                    string value = token[fieldName.Trim()].ToString();
                    if (!string.IsNullOrEmpty(value))
                        return value;
                } catch {}
            }

            return "";
        }
    }
}
