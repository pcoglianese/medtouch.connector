﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedTouch.Connector.RestEndpoints
{
    public interface IRestRequestHandler
    {
        /// <summary>
        /// Executes the specified request
        /// </summary>
        /// <param name="request">The request to execute</param>
        void Execute(RestRequest request);
    }
}
