// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITemplateFilter.cs" company="MedTouch">
//   Copyright (c) MedTouch 2015. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//        Developer:    sitecore\admin
// Template Version:    3.0
//             Date:    07/23/2015
// --------------------------------------------------------------------------------------------------------------------
namespace MedTouch.Connector.RestEndpoints.Filters
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    using MedTouch.Common;
    using MedTouch.Common.Versioning;

    using Newtonsoft.Json;

    using Sitecore.Data;
    using Sitecore.Data.Items;

    

    /// <summary>
    /// The 'TemplateFilter' Interface
    /// </summary>
    public interface ITemplateFilter : IMItem, IFilter
    {
        /// <summary>
        /// Gets the ExcludedTemplates
        /// </summary>
        List<string> ExcludedTemplates { get; }

        /// <summary>
        /// Gets the IncludedTemplates
        /// </summary>
        List<string> IncludedTemplates { get; }

    }

    /// <summary>
    /// The 'TemplateFilter' Data Template item
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    [Serializable, GuidAttribute("b89be3c3-8806-4003-b0e8-5cb0167bf79a")]
    [MTemplateVersion("b89be3c3-8806-4003-b0e8-5cb0167bf79a", "Template Filter")]
    public partial class TemplateFilter : Filter, ITemplateFilter
    {
        /// <summary>
        /// Initializes a new instance of the TemplateFilter class.
        /// </summary>
        /// <param name="item">The Sitecore item.</param>
        internal TemplateFilter(Item item)
            : base(item)
        {
            this.ExcludedTemplates = item[FieldNames.ExcludedTemplates].GetMultipleFieldValues();
            this.IncludedTemplates = item[FieldNames.IncludedTemplates].GetMultipleFieldValues();

            foreach (string id in ExcludedTemplates)
                excludedTemplateMap[id] = id;

            foreach (string id in IncludedTemplates)
                includedTemplateMap[id] = id;
        }

        #region Template Properties

        /// <summary>
        /// Gets the ExcludedTemplates
        /// </summary>
        /// <value>The ExcludedTemplates.</value>
        [JsonProperty]
        public List<string> ExcludedTemplates { get; private set; }

        /// <summary>
        /// Gets the IncludedTemplates
        /// </summary>
        /// <value>The IncludedTemplates.</value>
        [JsonProperty]
        public List<string> IncludedTemplates { get; private set; }

        #endregion

        #region ITemplateFilter Properties

        /// <summary>
        /// Gets the ExcludedTemplates
        /// </summary>
        List<string> ITemplateFilter.ExcludedTemplates
        {
            get
            {
                return this.ExcludedTemplates;
            }
        }

        /// <summary>
        /// Gets the IncludedTemplates
        /// </summary>
        List<string> ITemplateFilter.IncludedTemplates
        {
            get
            {
                return this.IncludedTemplates;
            }
        }

        #endregion

        /// <summary>
        /// Performs an implicit conversion from <see cref="Sitecore.Data.Items.Item"/> to <see cref="TemplateFilter"/>.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>
        /// The result of the conversion.
        /// </returns>
        public static implicit operator TemplateFilter(Item item)
        {
            return item.InheritsTemplate(typeof(TemplateFilter)) ? new TemplateFilter(item) : null;
        }

        /// <summary>
        /// The Strongly typed field names for <see cref="TemplateFilter"/>.
        /// </summary>
        public static new class FieldNames
        {
            /// <summary>
            /// The Field Name
            /// </summary>
            public static readonly ID ExcludedTemplates = new ID("9589fa18-c69a-4a01-8f67-54da4b207b5e");

            /// <summary>
            /// The Field Name
            /// </summary>
            public static readonly ID IncludedTemplates = new ID("83e4cad8-8194-46f0-9445-6c2b5d2304ad");

        }


        #region Custom code

        /// <summary>
        /// A mapping of template IDs
        /// </summary>
        private Dictionary<string, string> includedTemplateMap = new Dictionary<string, string>();

        /// <summary>
        /// A mapping of template IDs
        /// </summary>
        private Dictionary<string, string> excludedTemplateMap = new Dictionary<string, string>();


        /// <summary>
        /// Indicates whether this filter excludes the specified item
        /// </summary>
        /// <param name="item">The item to check</param>
        /// <returns>true if the specified item should be filtered out</returns>
        public override bool Excludes(SortedDictionary<string, object> item)
        {
            if (item == null || !item.IsSitecoreItem()) return true;

            string templateID = item.GetTemplateID();

            if (includedTemplateMap.Count > 0)
                return !includedTemplateMap.ContainsKey(templateID);

            return excludedTemplateMap.ContainsKey(templateID);
        }

        /// <summary>
        /// Indicates whether this filter excludes all items from the specified data source
        /// </summary>
        /// <param name="dataSource">The data source to check</param>
        /// <returns>true if all items from the specified data source should be filtered out</returns>
        public override bool Excludes(IDataSource dataSource)
        {
            return false;
        }


        #endregion Custom code


    }
}