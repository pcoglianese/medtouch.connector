﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedTouch.Connector.RestEndpoints.Filters
{
    /// <summary>
    /// Indicates a preferred rendering strategy for content
    /// </summary>
    public enum RenderingHint
    {
        /// <summary>
        /// Indicates that plain text should be used when possible, with any tags stripped
        /// </summary>
        Plain,

        /// <summary>
        /// Indicates that a raw data value should be used
        /// </summary>
        Raw,

        /// <summary>
        /// Indicates that any rendering pipeline code should be invoked
        /// </summary>
        Rendered,

        /// <summary>
        /// Indicates that when appropriate, a tag should be generated (such as for an image or link)
        /// </summary>
        Tag,

        /// <summary>
        /// Indicates that links should be followed to target content
        /// </summary>
        Target,

        /// <summary>
        /// Indicates that when appropriate, a URL should be generated
        /// </summary>
        Url
    }

}
