// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IDataSourceFilter.cs" company="MedTouch">
//   Copyright (c) MedTouch 2015. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//        Developer:    sitecore\admin
// Template Version:    3.0
//             Date:    07/23/2015
// --------------------------------------------------------------------------------------------------------------------
namespace MedTouch.Connector.RestEndpoints.Filters
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    using Newtonsoft.Json;

    using Sitecore.Data;
    using Sitecore.Data.Items;

    using MedTouch.Common;
    using MedTouch.Common.Versioning;
    

    /// <summary>
    /// The 'DataSourceFilter' Interface
    /// </summary>
    public interface IDataSourceFilter : IMItem, IFilter
    {
        /// <summary>
        /// Gets the ExcludedDataSources
        /// </summary>
        IList<IDataSource> ExcludedDataSources { get; }

        /// <summary>
        /// Gets the IncludedDataSources
        /// </summary>
        IList<IDataSource> IncludedDataSources { get; }


    }

    /// <summary>
    /// The 'DataSourceFilter' Data Template item
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    [Serializable, GuidAttribute("e1bb5a40-5ee8-402b-b083-75eab8d356f8")]
    [MTemplateVersion("e1bb5a40-5ee8-402b-b083-75eab8d356f8", "Data Source Filter")]
    public partial class DataSourceFilter : Filter, IDataSourceFilter
    {
        /// <summary>
        /// Initializes a new instance of the DataSourceFilter class.
        /// </summary>
        /// <param name="item">The Sitecore item.</param>
        internal DataSourceFilter(Item item)
            : base(item)
        {
            this.ExcludedDataSources = item.Fields[FieldNames.ExcludedDataSources].ConvertToMItems<IDataSource>();
            this.IncludedDataSources = item.Fields[FieldNames.IncludedDataSources].ConvertToMItems<IDataSource>();

            foreach (IDataSource dataSource in ExcludedDataSources)
                excludedDataSourceNames[dataSource.Name] = dataSource.Name;

            foreach (IDataSource dataSource in IncludedDataSources)
                includedDataSourceNames[dataSource.Name] = dataSource.Name;
        }

        #region Template Properties

        /// <summary>
        /// Gets the ExcludedDataSources
        /// </summary>
        /// <value>The ExcludedDataSources.</value>
        [JsonProperty]
        [MTemplateFieldVersion("2a546a4c-65cc-4fdb-9c2c-09630dd4922e", "Excluded Data Sources", "Treelist")]
        public IList<IDataSource> ExcludedDataSources { get; private set; }

        /// <summary>
        /// Gets the IncludedDataSources
        /// </summary>
        /// <value>The IncludedDataSources.</value>
        [JsonProperty]
        [MTemplateFieldVersion("32e9a5fb-9921-4024-9ad2-5ecd20dfec90", "Included Data Sources", "Treelist")]
        public IList<IDataSource> IncludedDataSources { get; private set; }

        #endregion

        #region IDataSourceFilter Properties

        /// <summary>
        /// Gets the ExcludedDataSources
        /// </summary>
        IList<IDataSource> IDataSourceFilter.ExcludedDataSources
        {
            get
            {
                return this.ExcludedDataSources;
            }
        }

        /// <summary>
        /// Gets the IncludedDataSources
        /// </summary>
        IList<IDataSource> IDataSourceFilter.IncludedDataSources
        {
            get
            {
                return this.IncludedDataSources;
            }
        }

        #endregion

        /// <summary>
        /// Performs an implicit conversion from <see cref="Sitecore.Data.Items.Item"/> to <see cref="DataSourceFilter"/>.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>
        /// The result of the conversion.
        /// </returns>
        public static implicit operator DataSourceFilter(Item item)
        {
            return item.InheritsTemplate(typeof(DataSourceFilter)) ? new DataSourceFilter(item) : null;
        }

        /// <summary>
        /// The Strongly typed field names for <see cref="DataSourceFilter"/>.
        /// </summary>
        public static new class FieldNames
        {
            /// <summary>
            /// The Field Name
            /// </summary>
            public static readonly ID ExcludedDataSources = new ID("2a546a4c-65cc-4fdb-9c2c-09630dd4922e");

            /// <summary>
            /// The Field Name
            /// </summary>
            public static readonly ID IncludedDataSources = new ID("32e9a5fb-9921-4024-9ad2-5ecd20dfec90");

        }


        #region Custom code

        /// <summary>
        /// A mapping of data source names
        /// </summary>
        private Dictionary<string, string> includedDataSourceNames = new Dictionary<string, string>();

        /// <summary>
        /// A mapping of data source names
        /// </summary>
        private Dictionary<string, string> excludedDataSourceNames = new Dictionary<string, string>();


        /// <summary>
        /// Indicates whether this filter excludes the specified item
        /// </summary>
        /// <param name="item">The item to check</param>
        /// <returns>true if the specified item should be filtered out</returns>
        public override bool Excludes(SortedDictionary<string, object> item)
        {
            if (item == null) return true;

            string dataSourceName = item.GetDataSourceName();
            if (string.IsNullOrWhiteSpace(dataSourceName)) 
                return false;

            if (includedDataSourceNames.Count > 0)
                return !includedDataSourceNames.ContainsKey(dataSourceName);

            return excludedDataSourceNames.ContainsKey(dataSourceName);
        }

        /// <summary>
        /// Indicates whether this filter excludes all items from the specified data source
        /// </summary>
        /// <param name="dataSource">The data source to check</param>
        /// <returns>true if all items from the specified data source should be filtered out</returns>
        public override bool Excludes(IDataSource dataSource)
        {
            if (dataSource == null) return true;

            if (includedDataSourceNames.Count > 0)
                return !includedDataSourceNames.ContainsKey(dataSource.Name);

            return excludedDataSourceNames.ContainsKey(dataSource.Name);
        }


        #endregion Custom code


    }
}