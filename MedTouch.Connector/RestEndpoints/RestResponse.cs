﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace MedTouch.Connector.RestEndpoints
{
    /// <summary>
    /// A REST endpoint response
    /// </summary>
    public class RestResponse
    {
        /// <summary>
        /// A default value to which to set the HTTP response's Content-Header value for MIME type
        /// </summary>
        private const string DefaultContentType = "application/json";

        /// <summary>
        /// A default value to which to set the HTTP response's Content-Header value for content encoding 
        /// </summary>
        private static Encoding DefaultContentEncoding = Encoding.UTF8;

        /// <summary>
        /// The HTTP content length header name
        /// </summary>
        private const string ContentLengthHeaderName = "Content-Length";
        
        /// <summary>
        /// The REST context for this response
        /// </summary>
        public RestContext Context
        {
            get;
            private set;
        }

        /// <summary>
        /// The HTTP response for this REST response
        /// </summary>
        public HttpResponse HttpResponse { get; private set; }

        /// <summary>
        /// Hides the default constructor
        /// </summary>
        private RestResponse() { }

        /// <summary>
        /// The encoding used in writing content to output
        /// </summary>
        private Encoding encoding; 

        /// <summary>
        /// Constructs a new instance
        /// </summary>
        /// <param name="httpResponse">The underlying HTTP response for this instance</param>
        /// <param name="context">The REST context for this instance</param>
        public RestResponse(HttpResponse httpResponse, RestContext context)
        {
            if (httpResponse == null || context == null) throw new ArgumentNullException();

            HttpResponse = httpResponse;
            Context = context;

            httpResponse.ContentType = !string.IsNullOrWhiteSpace(context.Endpoint.ContentType) ? context.Endpoint.ContentType.Trim() : DefaultContentType;

            encoding = DefaultContentEncoding;
            if (!string.IsNullOrWhiteSpace(context.Endpoint.ContentEncoding))
            {
                try 
                {
                    encoding = Encoding.GetEncoding(context.Endpoint.ContentEncoding.Trim());
                } catch { }
            }
            httpResponse.ContentEncoding = encoding;
//            httpResponse.CacheControl = "no-cache";
            httpResponse.Headers.Remove("Transer-Encoding");
        }

        /// <summary>
        /// Writes the specified string to the HTTP output stream
        /// </summary>
        /// <param name="s"></param>
        public void Write(string s)
        {
            if (!string.IsNullOrWhiteSpace(s))
            {
                this.HttpResponse.AddHeader(ContentLengthHeaderName, encoding.GetByteCount(s).ToString());
                this.HttpResponse.Output.Write(s);
            }
        }

        /// <summary>
        /// Flushes the underlying output stream
        /// </summary>
        public void Flush()
        {
            this.HttpResponse.Flush();
        }

        /// <summary>
        /// Closes the underlying output stream
        /// </summary>
        public void Close()
        {
            this.HttpResponse.Close();
        }

        /// <summary>
        /// The underlying output stream for this response
        /// </summary>
        public TextWriter Output
        {
            get
            {
                return HttpResponse.Output;
            }
        }

        /// <summary>
        /// The data of this response
        /// </summary>
        private List<SortedDictionary<string, object>> data = new List<SortedDictionary<string, object>>();

        /// <summary>
        /// Adds to the data of this response
        /// </summary>
        /// <param name="o">The data to add</param>
        public void Add(SortedDictionary<string, object> o)
        {
            if (o == null) return;

            lock (this)
            {
                data.Add(o);
            }
        }

        /// <summary>
        /// Adds to the data of this response
        /// </summary>
        /// <param name="o">The data to add</param>
        public void Add(IEnumerable<SortedDictionary<string, object>> data)
        {
            if (data == null) return;
            foreach (SortedDictionary<string, object> o in data)
                Add(o);
        }

        /// <summary>
        /// Gets the data of this response
        /// </summary>
        public IList<SortedDictionary<string, object>> Data
        {
            get
            {
                return data;
            }
        }

        /// <summary>
        /// Indicates whether execution of the request for this response encountered an error
        /// </summary>
        private bool isException = false;

        /// <summary>
        /// Indicates whether execution of the request for this response encountered an error
        /// </summary>
        public bool IsException { get { return isException; } }

        /// <summary>
        /// Exceptions encountered during processing
        /// </summary>
        private IList<Exception> exceptions = new List<Exception>();

        /// <summary>
        /// Gets the list of exceptions encountered during generation of this response
        /// </summary>
        public IEnumerable<Exception> Exceptions
        {
            get
            {
                return exceptions;
            }
        }

        /// <summary>
        /// Gets the last exception encountered by this response
        /// </summary>
        public Exception LastException
        {
            get
            {
                if (!isException)
                    return null;
                else
                    return exceptions[exceptions.Count - 1];
            }
        }

        /// <summary>
        /// Adds the specified exception, and updates the status of this response to an error status
        /// </summary>
        /// <param name="e"></param>
        public void AddException(Exception e)
        {
            if (e == null) return;

            isException = true;
            exceptions.Add(e);

            httpStatusCode = 500;
            HttpResponse.StatusCode = 500;
            httpStatusDescription = "Internal Server Error";
            HttpResponse.StatusDescription = "Internal Server Error";

            Context.Log(e);
        }

        /// <summary>
        /// This response's HTTP status code
        /// </summary>
        private int httpStatusCode = 200;

        /// <summary>
        /// This response's HTTP status code
        /// </summary>
        public int HttpStatusCode
        {
            get
            {
                return httpStatusCode;
            }
        }

        /// <summary>
        /// This response's HTTP status message
        /// </summary>
        private string httpStatusDescription = "OK";

        /// <summary>
        /// This response's HTTP status message
        /// </summary>
        public string HttpStatusDescription
        {
            get
            {
                return httpStatusDescription;
            }
        }

        /// <summary>
        /// The error message, if any, for this response
        /// </summary>
        private string errorMessage = "";

        /// <summary>
        /// The error message, if any, for this response
        /// </summary>
        public string ErrorMessage
        {
            get
            {
                return errorMessage;
            }

            set
            {
                errorMessage = (value + "").Trim();
            }
        }

    }
}
