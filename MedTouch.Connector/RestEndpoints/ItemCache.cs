﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;

namespace MedTouch.Connector.RestEndpoints
{
    /// <summary>
    /// Implements an in-memory cache for REST endpoints
    /// </summary>
    internal static class ItemCache
    {
        /// <summary>
        /// The object cache
        /// </summary>
        private static MemoryCache cache = new MemoryCache((typeof (ItemCache)).FullName);
        
        /// <summary>
        /// Caches a list of items for the specified number of milliseconds
        /// </summary>
        /// <param name="items">The items to cache</param>
        /// <param name="key">The cache key to use</param>
        /// <param name="durationInMilliseconds">The number of milliseconds to keep the items in cache</param>
        public static void Set(List<SortedDictionary<string, object>> items, string key, int durationInMilliseconds) 
        {
            if (items == null || items.Count == 0) return;
            else if (string.IsNullOrWhiteSpace(key)) throw new ArgumentException("Invalid (null or whitespace) key");

            key = key.Trim();

            DateTimeOffset expiration = 
                (durationInMilliseconds > 0 && durationInMilliseconds < int.MaxValue) ?
                DateTimeOffset.UtcNow.AddMilliseconds(durationInMilliseconds) :
                DateTimeOffset.MaxValue;

            cache.Set(key, items, expiration);
        }

        /// <summary>
        /// Gets a cached item list, if available
        /// </summary>
        /// <param name="key">The key to use</param>
        /// <returns>A list of items</returns>
        public static List<SortedDictionary<string, object>> Get(string key)
        {
            if (string.IsNullOrWhiteSpace(key)) throw new ArgumentException("Invalid (null or whitespace) key");

            try
            {
                return (List<SortedDictionary<string, object>>)cache[key];
            } catch {}

            return null;            
        }
    }
}
