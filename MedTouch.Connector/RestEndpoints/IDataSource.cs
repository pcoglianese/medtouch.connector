// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IDataSource.cs" company="MedTouch">
//   Copyright (c) MedTouch 2015. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//        Developer:    sitecore\admin
// Template Version:    3.0
//             Date:    07/23/2015
// --------------------------------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Runtime.InteropServices;

using Newtonsoft.Json;

using Sitecore.Data;
using Sitecore.Data.Items;

using MedTouch.Common;
using MedTouch.Common.Versioning;
using MedTouch.Connector.RestEndpoints.Filters;

namespace MedTouch.Connector.RestEndpoints
{
    

    /// <summary>
    /// The 'DataSource' Interface
    /// </summary>
    public interface IDataSource : IMItem
    {

        /// <summary>
        /// Gets the ExcludedFields
        /// </summary>
        List<string> ExcludedFields { get; }

        /// <summary>
        /// Gets the IncludedFields
        /// </summary>
        List<string> IncludedFields { get; }

        /// <summary>
        /// Gets the FieldTransforms
        /// </summary>
        IEnumerable<IFieldTransform> FieldTransforms { get; }

        /// <summary>
        /// Gets a value indicating whether IsActive
        /// </summary>
        bool IsActive { get; }

        /// <summary>
        /// Gets the Name
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets the Parameters
        /// </summary>
        SortedDictionary<string, string> Parameters { get; }

        /// <summary>
        /// Gets the SortField
        /// </summary>
        string SortField { get; }

        /// <summary>
        /// Gets a value indicating whether SortDescending
        /// </summary>
        bool SortDescending { get; }

        /// <summary>
        /// Gets the Source
        /// </summary>
        List<string> Source { get; }

        bool IsCachingEnabled { get; }

        int CacheDuration { get; }

        RenderingHint ImageFieldRenderingHint { get; }
        RenderingHint LinkFieldRenderingHint { get; }
        RenderingHint TextFieldRenderingHint { get; }

        #region Custom code

        /// <summary>
        /// Gets data from this data soruce
        /// </summary>
        /// <returns>A list of data objects</returns>
        List<SortedDictionary<string, object>> GetData(RestRequest request);

        /// <summary>
        /// Indicates whether this data source is correctly configured
        /// </summary>
        bool IsValid { get; }

        /// <summary>
        /// When this data source is not correctly configured, gets a validation message indicating the problem
        /// </summary>
        string ValidationMessage { get; }

        #endregion Custom code

    }

    /// <summary>
    /// The 'DataSource' Data Template item
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    [Serializable, GuidAttribute("f11b08f8-5679-47c4-ace2-5594832094d0")]
    [MTemplateVersion("f11b08f8-5679-47c4-ace2-5594832094d0", "Data Source")]
    public partial class DataSource : MItem, IDataSource
    {
        /// <summary>
        /// A default cache duration (five minutes, in seconds)
        /// </summary>
        private const int DefaultCacheDuration = 300;

        /// <summary>
        /// Initializes a new instance of the DataSource class.
        /// </summary>
        /// <param name="item">The Sitecore item.</param>
        internal DataSource(Item item)
            : base(item)
        {
            this.ExcludedFields = item[FieldNames.ExcludedFields].GetMultipleFieldValues();
            foreach (string fieldName in ExcludedFields)
                excludedFieldMap[fieldName] = fieldName;

            this.IncludedFields = item[FieldNames.IncludedFields].GetMultipleFieldValues();
            foreach (string fieldName in IncludedFields)
                includedFieldMap[fieldName] = fieldName;

            this.FieldTransforms = item.Fields[FieldNames.FieldTransforms].ConvertToMItems<FieldTransform>();
            this.IsActive = item.Fields[FieldNames.IsActive].ConvertAsBoolean();
            this.Name = item[FieldNames.Name];

            string stuffedParameters = item[FieldNames.Parameters];
            this.Parameters = string.IsNullOrWhiteSpace(stuffedParameters) ? new SortedDictionary<string, string>() : System.Web.HttpUtility.ParseQueryString(stuffedParameters).ToSortedDictionary();

            this.SortDescending = item.Fields[FieldNames.SortDescending].ConvertAsBoolean();
            this.SortField = item[FieldNames.SortField];
            this.Source = item[FieldNames.Source].ReplaceParameters(Parameters).GetMultipleFieldValues();

            this.IsCachingEnabled = item[FieldNames.IsCachingEnabled] == "1";

            int cacheDuration = DefaultCacheDuration;
            int.TryParse(item[FieldNames.CacheDuration], out cacheDuration);
            this.CacheDuration = Math.Max(cacheDuration, 0);
            this.CacheDurationInMilliseconds = this.CacheDuration;
            try { this.CacheDurationInMilliseconds *= 1000; } catch {}

            this.ImageFieldRenderingHint = RestUtility.DecodeRenderingHint(item["Image Field Rendering Hint"]);
            this.LinkFieldRenderingHint = RestUtility.DecodeRenderingHint(item["Link Field Rendering Hint"]);
            this.TextFieldRenderingHint = RestUtility.DecodeRenderingHint(item["Text Field Rendering Hint"]);

            this.itemID = item.ID.ToString();
        }

        /// <summary>
        /// A cache duration in milliseconds
        /// </summary>
        private int CacheDurationInMilliseconds;

        #region Template Properties

        /// <summary>
        /// Gets the ExcludedFields
        /// </summary>
        /// <value>The ExcludedFields.</value>
        [JsonProperty]
        [MTemplateFieldVersion("7653f724-3566-48ee-a175-a7215bb37a63", "Excluded Fields", "Multi-Line Text")]
        public List<string> ExcludedFields { get; private set; }

        /// <summary>
        /// Gets the FieldTransforms
        /// </summary>
        /// <value>The FieldTransforms.</value>
        [JsonProperty]
        [MTemplateFieldVersion("61750b38-1f81-4b7f-9bb6-7f1d4525f2a3", "Field Transforms", "Treelist")]
        public IEnumerable<IFieldTransform> FieldTransforms { get; private set; }

        /// <summary>
        /// Gets the IncludedFields
        /// </summary>
        /// <value>The IncludedFields.</value>
        [JsonProperty]
        [MTemplateFieldVersion("04b84ba9-b101-45e3-8ddd-966e3f9e1fcf", "Included Fields", "Multi-Line Text")]
        public List<string> IncludedFields { get; private set; }

        /// <summary>
        /// Gets a value indicating whether IsActive
        /// </summary>
        /// <value>The IsActive.</value>
        [JsonProperty]
        [MTemplateFieldVersion("d1770be9-d9c0-452b-911c-c15372a0a54f", "Is Active", "Checkbox")]
        public bool IsActive { get; private set; }

        /// <summary>
        /// Gets the Name
        /// </summary>
        /// <value>The Name.</value>
        [JsonProperty]
        [MTemplateFieldVersion("861f845e-090e-4bca-b51e-aee73c59d5a2", "Name", "Single-Line Text")]
        public string Name { get; private set; }

        /// <summary>
        /// Gets the Parameters
        /// </summary>
        /// <value>The Parameters.</value>
        [JsonProperty]
        [MTemplateFieldVersion("492868b2-3f47-4d99-806e-f2a4b00fb796", "Parameters", "Single-Line Text")]
        public SortedDictionary<string, string> Parameters { get; private set; }

        /// <summary>
        /// Gets a value indicating whether SortDescending
        /// </summary>
        /// <value>The SortDescending.</value>
        [JsonProperty]
        [MTemplateFieldVersion("bbd7e8dc-d472-46da-bed1-e1a96f0f9580", "Sort Descending", "Checkbox")]
        public bool SortDescending { get; private set; }

        /// <summary>
        /// Gets the SortField
        /// </summary>
        /// <value>The SortField.</value>
        [JsonProperty]
        [MTemplateFieldVersion("e585a250-eb29-494d-8c0f-25ccff43be87", "Sort Field", "Single-Line Text")]
        public string SortField { get; private set; }

        /// <summary>
        /// Gets the Source
        /// </summary>
        /// <value>The Source.</value>
        [JsonProperty]
        [MTemplateFieldVersion("9afa25bf-d5a7-42a1-a79d-7e4e9ea71d22", "Source", "Multi-Line Text")]
        public List<string> Source { get; private set; }

        /// <summary>
        /// Gets whether IsCachingEnabled
        /// </summary>
        /// <value>The IsCachingEnabled.</value>
        [JsonProperty]
        [MTemplateFieldVersion("d7b9465e-9100-41d7-9306-436bb4073d47", "Is Caching Enabled", "Checkbox")]
        public bool IsCachingEnabled { get; private set; }

        /// <summary>
        /// Gets the CacheDuration
        /// </summary>
        /// <value>The CacheDuration.</value>
        [JsonProperty]
        [MTemplateFieldVersion("d5d75832-146f-4823-bbd8-93684b268dc2", "Cache Duration", "Single-Line Text")]
        public int CacheDuration { get; private set; }

        public RenderingHint ImageFieldRenderingHint { get; set; }
        public RenderingHint LinkFieldRenderingHint { get; set; }
        public RenderingHint TextFieldRenderingHint { get; set; }

        #endregion

        #region IDataSource Properties

        /// <summary>
        /// Gets the ExcludedFields
        /// </summary>
        List<string> IDataSource.ExcludedFields
        {
            get
            {
                return this.ExcludedFields;
            }
        }

        /// <summary>
        /// Gets the FieldTransforms
        /// </summary>
        IEnumerable<IFieldTransform> IDataSource.FieldTransforms
        {
            get
            {
                return this.FieldTransforms;
            }
        }

        /// <summary>
        /// Gets the IncludedFields
        /// </summary>
        List<string> IDataSource.IncludedFields
        {
            get
            {
                return this.IncludedFields;
            }
        }

        /// <summary>
        /// Gets a value indicating whether IsActive
        /// </summary>
        bool IDataSource.IsActive
        {
            get
            {
                return this.IsActive;
            }
        }

        /// <summary>
        /// Gets the Name
        /// </summary>
        string IDataSource.Name
        {
            get
            {
                return this.Name;
            }
        }

        /// <summary>
        /// Gets the Parameters
        /// </summary>
        SortedDictionary<string, string> IDataSource.Parameters
        {
            get
            {
                return this.Parameters;
            }
        }

        /// <summary>
        /// Gets a value indicating whether SortDescending
        /// </summary>
        bool IDataSource.SortDescending
        {
            get
            {
                return this.SortDescending;
            }
        }

        /// <summary>
        /// Gets the SortField
        /// </summary>
        string IDataSource.SortField
        {
            get
            {
                return this.SortField;
            }
        }

        /// <summary>
        /// Gets the Source
        /// </summary>
        List<string> IDataSource.Source
        {
            get
            {
                return this.Source;
            }
        }

        /// <summary>
        /// Gets whether IsCachingEnabled
        /// </summary>
        bool IDataSource.IsCachingEnabled
        {
            get
            {
                return this.IsCachingEnabled;
            }
        }

        /// <summary>
        /// Gets the CacheDuration
        /// </summary>
        int IDataSource.CacheDuration
        {
            get
            {
                return this.CacheDuration;
            }
        }

        RenderingHint IDataSource.ImageFieldRenderingHint { get { return this.ImageFieldRenderingHint; } }
        RenderingHint IDataSource.LinkFieldRenderingHint { get { return this.LinkFieldRenderingHint; } }
        RenderingHint IDataSource.TextFieldRenderingHint { get { return this.TextFieldRenderingHint; } }

        #endregion

        /// <summary>
        /// Performs an implicit conversion from <see cref="Sitecore.Data.Items.Item"/> to <see cref="DataSource"/>.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>
        /// The result of the conversion.
        /// </returns>
        public static implicit operator DataSource(Item item)
        {
            return item.InheritsTemplate(typeof(DataSource)) ? new DataSource(item) : null;
        }

        /// <summary>
        /// The Strongly typed field names for <see cref="DataSource"/>.
        /// </summary>
        public static new class FieldNames
        {
            /// <summary>
            /// The Field Name
            /// </summary>
            public static readonly ID ExcludedFields = new ID("7653f724-3566-48ee-a175-a7215bb37a63");

            /// <summary>
            /// The Field Name
            /// </summary>
            public static readonly ID FieldTransforms = new ID("61750b38-1f81-4b7f-9bb6-7f1d4525f2a3");

            /// <summary>
            /// The Field Name
            /// </summary>
            public static readonly ID IncludedFields = new ID("04b84ba9-b101-45e3-8ddd-966e3f9e1fcf");

            /// <summary>
            /// The Field Name
            /// </summary>
            public static readonly ID IsActive = new ID("d1770be9-d9c0-452b-911c-c15372a0a54f");

            /// <summary>
            /// The Field Name
            /// </summary>
            public static readonly ID Name = new ID("861f845e-090e-4bca-b51e-aee73c59d5a2");

            /// <summary>
            /// The Field Name
            /// </summary>
            public static readonly ID Parameters = new ID("492868b2-3f47-4d99-806e-f2a4b00fb796");

            /// <summary>
            /// The Field Name
            /// </summary>
            public static readonly ID SortDescending = new ID("bbd7e8dc-d472-46da-bed1-e1a96f0f9580");

            /// <summary>
            /// The Field Name
            /// </summary>
            public static readonly ID SortField = new ID("e585a250-eb29-494d-8c0f-25ccff43be87");

            /// <summary>
            /// The Field Name
            /// </summary>
            public static readonly ID Source = new ID("9afa25bf-d5a7-42a1-a79d-7e4e9ea71d22");

            /// <summary>
            /// The Field Name
            /// </summary>
            public static readonly ID IsCachingEnabled = new ID("d7b9465e-9100-41d7-9306-436bb4073d47");

            /// <summary>
            /// The Field Name
            /// </summary>
            public static readonly ID CacheDuration = new ID("d5d75832-146f-4823-bbd8-93684b268dc2");
        }

        #region Custom code

        /// <summary>
        /// An item ID used in constructing a cache key
        /// </summary>
        private string itemID;

        /// <summary>
        /// Generates a cache key for this data source combined with the request
        /// </summary>
        /// <param name="request">The request to use</param>
        /// <returns>A cache key</returns>
        protected string GetCacheKey(RestRequest request)
        {
            if (request == null) throw new ArgumentNullException();

            return itemID + '|' + request.CacheKey;
        }

        /// <summary>
        /// Gets the total number of items to request from a data source that does not directly support paging, to be able to select a desired page
        /// </summary>
        /// <param name="pageSize">The page size of a request</param>
        /// <param name="pageNumber">The page number of a request</param>
        /// <returns>A total number of results to request from a data source which does not support paging</returns>
        protected static int GetNonPagedCount(int pageSize, int pageNumber)
        {
            pageSize = Math.Max(pageSize, 1);
            pageNumber = Math.Max(pageNumber, 1);

            if (pageSize == int.MaxValue)
                return int.MaxValue;

            try
            {
                return pageSize * pageNumber;
            }
            catch { }

            return int.MaxValue;
        }

        /// <summary>
        /// A field-name mapping for tracking field inclusion/exclusion
        /// </summary>
        private Dictionary<string, string> includedFieldMap = new Dictionary<string, string>();

        /// <summary>
        /// A field-name mapping for tracking field inclusion/exclusion
        /// </summary>
        private Dictionary<string, string> excludedFieldMap = new Dictionary<string, string>();

        /// <summary>
        /// Gets data from this data soruce
        /// </summary>
        /// <returns>A list of data objects</returns>
        public virtual List<SortedDictionary<string, object>> GetData(RestRequest request) 
        {
            if (request == null) throw new ArgumentNullException();

            List<SortedDictionary<string, object>> data = new List<SortedDictionary<string, object>>();
            if (!this.IsActive) return data;

            string cacheKey = "";
            if (this.IsCachingEnabled)
            {
                cacheKey = GetCacheKey(request);
                List<SortedDictionary<string, object>> cachedData = ItemCache.Get(cacheKey);
                if (cachedData != null)
                {
                    request.Context.Log("Cached data found");
                    return cachedData;
                }
            }

            List<string> replacedSource = Source.ToList();

            for (int x = 0; x < replacedSource.Count; x++)
                replacedSource[x] = replacedSource[x].ReplaceParameters(request.Parameters);

            this.GetData(request, data, replacedSource);

            string dataSourceName = string.IsNullOrWhiteSpace(this.Name) ? this.GetType().FullName : this.Name;
            foreach (SortedDictionary<string, object> item in data)
                item.SetDataSourceName(dataSourceName);
            
            this.FieldTransforms.Transform(data);
            this.ExcludeFields(data);

            if (!string.IsNullOrWhiteSpace(this.SortField))
                data.SortByField(this.SortField, !this.SortDescending);

            if (this.IsCachingEnabled)
            {
                request.Context.Log("");
                ItemCache.Set(data, cacheKey, CacheDurationInMilliseconds);
            }

            return data;
        }

        /// <summary>
        /// Performs field inclusion/exclusion checks, updating each item's fields as necessary
        /// </summary>
        /// <param name="data">The list of data to process</param>
        private void ExcludeFields(List<SortedDictionary<string, object>> data)
        {
            if (data == null) return;

            SortedDictionary<string, object> item;
            if (includedFieldMap.Count > 0)
            {
                for(int x = data.Count - 1; x >= 0; x--)
                {
                    item = data[x];

                    foreach (string fieldName in item.Keys.ToList())
                    {
                        if (!includedFieldMap.ContainsKey(fieldName))
                        {
                            item.Remove(fieldName);
                            if (item.Count == 0)
                            {
                                data.RemoveAt(x);
                            }
                        }
                    }
                }
            }

            if (excludedFieldMap.Count > 0)
            {
                for (int x = data.Count - 1; x >= 0; x--)
                {
                    item = data[x];

                    foreach (string fieldName in item.Keys.ToList())
                    {
                        if (excludedFieldMap.ContainsKey(fieldName))
                        {
                            item.Remove(fieldName);
                            if (item.Count == 0)
                            {
                                data.RemoveAt(x);
                            }
                        }
                    }
                }
            }

        }

        /// <summary>
        /// Implements logic to get data from this data soruce
        /// </summary>
        /// <param name="request">The request for which to get the data</param>
        /// <param name="data">A list of data objects, to which to append</param>
        /// <param name="sources">The list of sources from which to get the data</param>
        protected virtual void GetData(RestRequest request, List<SortedDictionary<string, object>> data, List<string> sources)
        {
        }

        /// <summary>
        /// Indicates whether the filters for this data source should exclude the specified item
        /// </summary>
        /// <param name="item">The data to test</param>
        /// <returns>true if the item data should be filtered out, otherwise false</returns>
        //protected bool FiltersExclude(SortedDictionary<string, object> item)
        //{
        //    if (item == null) return true;

        //    foreach (IFilter filter in this.Filters)
        //        if (filter.Excludes(this) || filter.Excludes(item))
        //            return true;

        //    return false;
        //}

        /// <summary>
        /// Indicates whether this data source is correctly configured
        /// </summary>
        public virtual bool IsValid { get { return true; } }

        /// <summary>
        /// When this data source is not correctly configured, gets a validation message indicating the problem
        /// </summary>
        public virtual string ValidationMessage { get { return ""; } }

        #endregion Custom code
    }
}