﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Sitecore.SecurityModel;

using MedTouch.Common;


namespace MedTouch.Connector.layouts
{
    using MedTouch.Common;
    using MedTouch.Connector.RestEndpoints;
    using MedTouch.Connector.RestEndpoints.RequestHandlers;
    using MedTouch.Connector.RestEndpoints.ResponseFormatters;

    using Sitecore.SecurityModel;

    public partial class RestEndpointLayout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                using (new SecurityDisabler())
                {
                    RestEndpoint endpoint = Sitecore.Context.Item.AsMItemTypeOf<RestEndpoint>();

                    if (endpoint == null || !endpoint.IsActive)
                    {
                        HttpContext.Current.Response.StatusCode = 403;
                        HttpContext.Current.Response.StatusDescription = "Forbidden";
                        HttpContext.Current.Response.Output.Flush();
                        HttpContext.Current.Response.Output.Close();
                        return;
                    }

                    RestContext context = new RestContext(endpoint, HttpContext.Current);
                    context.Execute();
                }
            }
            catch (Exception ex)
            {
                try
                {
                    HttpContext.Current.Response.Write(ex.ToString());
                    HttpContext.Current.Response.Flush();
                    HttpContext.Current.Response.Close();
                }
                catch { }
            }
        }
    }
}